
i0 = 0
i1 = 100
j0 = 69
j1 = 130

;; Get lat and lon
fili = 'APHRO_MA_TAVE_050deg_V1204R1.1979.nc'
lat = NCDF_READV( fili, VARNAME='latitude' )
lon = NCDF_READV( fili, VARNAME='longitude' )

print, lat, lon

RESTORE, 'aphrodite_mean_daily_tair_1979to2007.sav'

;; Write to netcdf
filo = 'aphrodite_mean_daily_tair_1979to2007.nc'

;; -  define global attributes in structure
global_att = {history: 'Created by APHRODITE project team 28- 3月-2013', $
              coments: 'APHRO_MA V1204R1 daily precipitation with 0.50deg grids' }
WRITE_LT_DAILY_MEAN_TO_NETCDF, result, lat[j0:j1], lon[i0:i1], filo, GLOBAL=global_att


END
