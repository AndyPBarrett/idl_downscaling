;;----------------------------------------------------------------------
;; Uses ERA-Interim T2m to downscale temperatures to MODIS grid cells.
;;
;; 2013-10-07 A.P.Barrett <apbarret@nsidc.org>
;;----------------------------------------------------------------------

PRO WRITE_TO_NETCDF, data, lat2d, lon2d, time, filo

  dims = SIZE( data, /DIMENSION )
  ncol = dims[0]
  nrow = dims[1]
  ntim = dims[2]

  cdfid = NCDF_CREATE( filo, /CLOBBER )

  xdimid = NCDF_DIMDEF( cdfid, 'col', ncol )
  ydimid = NCDF_DIMDEF( cdfid, 'row', nrow )
  tdimid = NCDF_DIMDEF( cdfid, 'time', ntim )

  xid   = NCDF_VARDEF( cdfid, 'col', [xdimid], /FLOAT )
  yid   = NCDF_VARDEF( cdfid, 'row', [ydimid], /FLOAT )
  tid   = NCDF_VARDEF( cdfid, 'time', [tdimid], /FLOAT )
  datid = NCDF_VARDEF( cdfid, 'tsurf', [xdimid,ydimid,tdimid], /FLOAT )
  latid = NCDF_VARDEF( cdfid, 'lat2d', [xdimid,ydimid], /FLOAT )
  lonid = NCDF_VARDEF( cdfid, 'lon2d', [xdimid,ydimid], /FLOAT )

  NCDF_ATTPUT, cdfid, xid, 'long_name', 'row'
  NCDF_ATTPUT, cdfid, xid, 'units', 'none'
  NCDF_ATTPUT, cdfid, yid, 'long_name', 'col'
  NCDF_ATTPUT, cdfid, yid, 'units', 'none'
  NCDF_ATTPUT, cdfid, tid, 'long_name', 'time'
  NCDF_ATTPUT, cdfid, tid, 'units', 'days since 1900-01-01 00:00:00'

  NCDF_ATTPUT, cdfid, datid, 'long_name', '2m temperature'
  NCDF_ATTPUT, cdfid, datid, 'units', 'degrees Kelvin'
  NCDF_ATTPUT, cdfid, latid, 'long_name', 'latitude'
  NCDF_ATTPUT, cdfid, latid, 'units', 'degrees-north'
  NCDF_ATTPUT, cdfid, lonid, 'long_name', 'longitude'
  NCDF_ATTPUT, cdfid, lonid, 'units', 'degrees-east'

  NCDF_ATTPUT, cdfid, 'creation_date', SYSTIME(), /GLOBAL
  NCDF_ATTPUT, cdfid, 'created_by', 'A.P.Barrett apbarret@nsidc.org', /GLOBAL
  NCDF_ATTPUT, cdfid, 'description', '2m temperature downscaled from ERA-Interim reanalysis T2m', /GLOBAL

  NCDF_CONTROL, cdfid, /ENDEF

  NCDF_VARPUT, cdfid, xid, FINDGEN(ncol)
  NCDF_VARPUT, cdfid, yid, FINDGEN(nrow)
  NCDF_VARPUT, cdfid, tid, FINDGEN(ntim)
  NCDF_VARPUT, cdfid, latid, lat2d
  NCDF_VARPUT, cdfid, lonid, lon2d
  NCDF_VARPUT, cdfid, datid, data

  NCDF_CLOSE, cdfid

  RETURN

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------
PRO DOWNSCALE_ERAI_T2M_BY_LAPSE, tile, year, LAPSE=lapse

  IF ( N_PARAMS() NE 2 ) THEN BEGIN
     PRINT, 'USAGE: DOWNSCALE_ERAI_T2M_BY_LAPSE, tile, year, LAPSE=lapse'
     PRINT, '      tile: modis tile id (h??v??)'
     PRINT, '      year: year to downscale'
     PRINT, ''
     PRINT, '      LAPSE: temperature lapse rate (deg C/km)'
     RETURN
  ENDIF

  nrow = 2400
  ncol = 2400

  ;; Set default lapse rate
  lapse = N_ELEMENTS( lapse ) EQ 0. ? 7. : lapse

  ;; Convert lapse rate to deg C/m
  gamma = (-1.) * lapse / 1000.

  ;; Read cell address for tile
  add_diri = '/raid/ERA_Interim/t2m'
  add_fili = 'modis_era_gridcells_'+tile+'.long.bin'
  cell_address = OPEN_IMAGE( add_diri + '/' + add_fili, ncol, nrow, 3 )

  ;; Get MODIS DEM
  dem_diri = '/projects/CHARIS/elevation_data/SRTM_esri'
  dem_fili = 'sin_'+tile+'.tif'
  zmod = READ_TIFF( dem_diri + '/' + dem_fili )

  ;; Get ERA-I elevations
  geo_diri = '/raid/ERA_Interim/t2m/'
  geo_fili = 'era_interim.zs.hkh.invariant.nc'
  zera = NCDF_READV( geo_diri + '/' + geo_fili, VARNAME='zs' )
  ;; - convert geopotential to geopotential height
  g = 9.81
  zera = zera / g

  ;; Assign ERA-Interim geopotential height to MODIS grid
  zera_grid = zera[ cell_address ]
  dz = zmod - zera_grid
  tadj = dz * gamma

;;----------------------------------------------------------------------
;; Get MODIS tile geolocation files
;;----------------------------------------------------------------------

  lat2d = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
  lon2d = GET_MODIS_GEOLOCATION_PT( tile, /LON )

;;----------------------------------------------------------------------
;; Process ERA-Interim T2m data
;;----------------------------------------------------------------------

  diri = '/raid/ERA_Interim/t2m' + '/' + STRING(year,FORMAT='(i4)')
  diro = '/raid/ERA_Interim/t2m' + '/' + STRING(year,FORMAT='(i4)')
  
  FOR im=0, 11 DO BEGIN
     
     month = im+1
     
     PRINT, '% Processing temperature data for month ' + STRING(year,month,FORMAT='(i4,"-",i02)')
     
     fili = 'era_interim.t2m.hkh.'+STRING(year,month,FORMAT='(i4,i02)')+'.nc'
     PRINT, '    getting data from ' + fili + '...'
     t2m = NCDF_READV( diri + '/' + fili, VARNAME='t2m' )
     
     dims = SIZE( t2m, /DIMENSIONS )
     ntim = dims[2]
     
     result = MAKE_ARRAY( nrow, ncol, ntim )
     
     PRINT, '    disaggregating ERA-Interim temperatures...'
     FOR it=0, ntim-1 DO BEGIN
        
        tmp = t2m[ *, *, it ]
        
        mtmp = tmp[ cell_address ]
        
        result[*,*,it] = mtmp + tadj
        
     ENDFOR
     
     ;; Calculate daily mean from 6h data
     PRINT, '    calculating daily means...'
     nday = ntim / 4
     id = 1
     FOR it=0, ntim-1, 4 DO BEGIN
        
        filo = diro + '/' + 't2m_' + tile + '_day_by_lapse_' + STRING(year,month,id,FORMAT='(i4,i02,i02)') + '.bin'
        
        tday = MEAN( result[*,*,it:it+3], DIM=3 )
        
        PRINT, '     writing daily T2m to ' + filo + '...'
        OPENW, U, filo, /GET_LUN
        WRITEU, U, tday
        CLOSE, U
        FREE_LUN, U
        
        id++
        
     ENDFOR
     
     DELVAR, result, tmp, mtmp, tday, t2m
     
  ENDFOR

END

