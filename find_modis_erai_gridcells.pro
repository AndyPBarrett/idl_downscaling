;; Tile
tile = 'h24v05'
srcfile = 'era_interim.zs.hkh.invariant.nc'

latvar = 'lat'
lonvar = 'lon'

VERBOSE=1

;; Get MODIS geolocation
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting MODIS tile coordinates...'
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Get ERA-Interim coordinates
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting source grid coordinates...'
elat = NCDF_READV( srcfile, VARNAME=latvar )
elon = NCDF_READV( srcfile, VARNAME=lonvar )
data = NCDF_READV( srcfile, VARNAME='zs' )
nlon = N_ELEMENTS( elon )
nlat = N_ELEMENTS( elat )

;; Make 2D lat lon grids for ERA-I
elat2D = REBIN( TRANSPOSE(elat), nlon, nlat, /SAMPLE )
elon2D = REBIN( elon, nlon, nlat, /SAMPLE )
ncell = N_ELEMENTS( elat2D )

dlat = 0.375
dlon = 0.375

cell_address = MAKE_ARRAY( ncol, nrow, /LONG )

FOR ic = 0, ncell-1 DO BEGIN

   idx = WHERE( ( mlat GE (elat2D[ic]-dlat) AND mlat LT (elat2D[ic]+dlat) ) AND $
                ( mlon GE (elon2D[ic]-dlon) AND mlon LT (elon2D[ic]+dlon) ), num )

   IF ( num GT 0 ) THEN cell_address[idx] = ic

ENDFOR

;;----------------------------------------------------------------------
;; Write results to binary
;;----------------------------------------------------------------------

filo = 'modis_era_gridcells_'+tile+'.long.bin'
PRINT, '% Writing cell indices to ' + filo
OPENW, U, filo, /GET_LUN
WRITEU, U, cell_address
CLOSE, U
FREE_LUN, U

;;----------------------------------------------------------------------
;; Plot results
;;----------------------------------------------------------------------

minlat = 28.
minlon = 67.
maxlat = 42.
maxlon = 94.

cntlat = MEAN( [minlat,maxlat] )
cntlon = MEAN( [minlon,maxlon] )

MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, LIMIT=[minlat,minlon,maxlat,maxlon]
CONTOUR, BYTSCL(cell_address), mlon, mlat, /OVERPLOT, /CELL_FILL, LEVEL=LINDGEN(256), C_COLOR=LINDGEN(256)
MAP_CONTINENTS, /COUNTRIES, /HIRES
MAP_GRID, /box

OPLOT, elon2D, elat2D, PSYM=1, SYMSIZE=1, THICK=2 


END
