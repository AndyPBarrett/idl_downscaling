;;----------------------------------------------------------------------
;; Extracts the lapse rates for WAPDA climate stations in the Hunza basin.
;;
;; 2014-03-07 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION GET_DAYAVG, x

   dims = SIZE( x, /DIMENSION )
   nlon = dims[0]
   nlat = dims[1]
   ntim = dims[2]

   nday = ntim / 4

   notvalid = WHERE( x LE -9998., num_notvalid )
   IF ( num_notvalid GT 0 ) THEN x[ notvalid ] = !VALUES.F_NAN

   xday = MAKE_ARRAY( nlon, nlat, nday, /FLOAT, VALUE=-9999.99 )

   id = 0
   FOR it=0, ntim-1, 4 DO BEGIN
      xday[*,*,id] = MEAN( x[*,*,it:it+3], DIM=3, /NAN )
      id = id + 1
   ENDFOR

   notvalid = WHERE( FINITE( xday ) EQ 0, num_notvalid )
   IF ( num_notvalid GT 0 ) THEN xday[ notvalid ] = -9999.99

   RETURN, xday

END

PRO FIND_STATION_GRID_CELL, lat, lon, mlat, mlon, ii, jj

  nlat = N_ELEMENTS( lat )
  nlon = N_ELEMENTS( lon )

;; Check if lat and lon are monotonic
  reverse_lat = 0
  reverse_lon = 0
  IF ( lat[0] GT lat[nlat-1] ) THEN reverse_lat = 1
  IF ( lon[0] GT lon[nlon-1] ) THEN reverse_lon = 1
  
;; Find grid cell spacing - assumes constant for lat and lon
  dlat = ABS( lat[1] - lat[0] )
  dlon = ABS( lon[1] - lon[0] )
  
  ii = ROUND( (mlon - MIN(lon))/dlon )
  jj = ROUND( (mlat - MIN(lat))/dlat )
  
  IF ( reverse_lat EQ 1 ) THEN jj = nlat - 1 - jj
  IF ( reverse_lon EQ 1 ) THEN ii = nlon - 1 - ii

  RETURN

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------
PRO GET_ERAI_TEMPERATURE_FOR_STATIONS, year

;year = 2001

;; Define ERA-Interim directory
erai_diri = RAID_PATH('arctic6') + '/ERA_Interim/t2m/'+STRTRIM(year,2)

;; Define station locations
station  = [ 'Khunjrab',  'Naltar',  'Ziarat', 'Gilgit', 'Islamabad' ]
mlat     = [  36.850000, 36.160300, 36.798000,  35.9219,     33.7167 ] 
mlon     = [  75.400000, 74.179883, 74.482000,  72.2892,     73.0667 ]
nstat = N_ELEMENTS( station )

;;----------------------------------------------------------------------
;; Find nearest ERA-I grid cell to stations
;;----------------------------------------------------------------------
erai_fili = 'era_interim.t2m.hkh.'+STRTRIM(year,2)+'01.nc'
lat = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lat' )
lon = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lon' )

FIND_STATION_GRID_CELL, lat, lon, mlat, mlon, ii, jj

;;----------------------------------------------------------------------
;; extract lapse rates for cells containing stations
;;----------------------------------------------------------------------
doy_arr   = INDGEN(366)+1
year_arr  = MAKE_ARRAY( 366, /FLOAT, VALUE=year )
month_arr = MAKE_ARRAY( 366, /FLOAT, VALUE=-9999.99 )
day_arr   = MAKE_ARRAY( 366, /FLOAT, VALUE=-9999.99 )
t2m_arr   = MAKE_ARRAY( nstat, 366, /FLOAT, VALUE=-9999.99 )

k = 0
FOR im=0, 11 DO BEGIN

   month = im+1
   erai_fili = 'era_interim.t2m.hkh.'+STRING(year,month,FORMAT='(i4,i02)')+'.nc'

   t2m = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='t2m' )

   ;; Get daily average temperature
   t2m_dy = GET_DAYAVG( t2m )

   dims = SIZE( t2m_dy, /DIMENSIONS )
   ntim = dims[2]

   month_arr[k:k+ntim-1]         = month
   day_arr[k:k+ntim-1]           = INDGEN(ntim)+1

   FOR istat=0, nstat-1 DO BEGIN

      t2m_arr[istat,k:k+ntim-1]  = t2m_dy[ii[istat],jj[istat],0:ntim-1]

   ENDFOR

   k = k + ntim

ENDFOR

nrec = k

t2m_arr  = t2m_arr[*,0:nrec-1]

;;----------------------------------------------------------------------
;; Write average temperature to file
;;----------------------------------------------------------------------
filo = 'era_interim.t2m.hkh.station_locations.'+STRING(year,FORMAT='(i4)')+'.txt'
OPENW, U, erai_diri + '/' + filo, /GET_LUN
PRINTF, U, '# year'
PRINTF, U, '# month'
PRINTF, U, '# day'
FOR istat=0, nstat-1 DO PRINTF, U, '# '+station[istat]+' T2m', FORMAT='(a)'
FOR ir=0, nrec-1 DO BEGIN
   PRINTF, U, year_arr[ir], month_arr[ir], day_arr[ir], doy_arr[ir], $
          t2m_arr[*,ir], FORMAT='(i4,1x,i2,1x,i2,1x,i3,'+STRTRIM(nstat,2)+'(1x,f8.2))'
ENDFOR
CLOSE, U
FREE_LUN, U

END
