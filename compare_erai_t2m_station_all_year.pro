;;----------------------------------------------------------------------
;; Compares T2m from ERA-Interim with station observations of T2m.
;;
;; 2014-03-25 A.P.Barrett
;;----------------------------------------------------------------------

PRO PLOT_XY, x, y, TITLE=title

  HELP, x, y

;  idx = WHERE( x GT -4000 AND y GT -4000, num )
  idx = WHERE( FINITE(x) EQ 1  AND FINITE(y) EQ 1, num )
  IF ( num GT 0 ) THEN BEGIN
     x = x[idx]
     y = y[idx]
  ENDIF
  ;print, num
  ;print, MIN(x,/NAN), MAX(x,/NAN)
  ;print, MIN(y,/NAN), MAX(y,/NAN)

  reso = 2.
  xmin = FLOOR( MIN( [x,y], /NAN )/reso ) * reso
  xmax = CEIL( MAX( [x,y], /NAN )/reso ) * reso
  
  A = FINDGEN(31) * (!PI*2/30.)
  USERSYM, COS(A), SIN(A), /FILL

  PLOT, x, y, PSYM=8, SYMSIZE=0.25, $
        XRANGE=[xmin,xmax], XSTYLE=1, XTITLE='Observed (K)', $
        YRANGE=[xmin,xmax], YSTYLE=1, YTITLE='Model (K)', $
        /ISOTROPIC, TITLE=title, FONT=0
  PLOTS, !X.CRANGE, !Y.CRANGE

  diff = x - y
  davg = MEAN( diff )
  rmsd = SQRT( MEAN( diff^2 ) )
  xavg = MEAN( x )
  yavg = MEAN( y )
  rho = CORRELATE( x, y )
 
  b = REGRESS( REFORM(x), REFORM(y), CONST=a, YFIT=yhat )
  x0 = MIN( x )
  x1 = MAX( x )
  y0 = a + ( b[0]*x0 )
  y1 = a + ( b[0]*x1 )
  error = y - yhat
  sse = TOTAL( error^2 )
  ssy = TOTAL( (y-MEAN(y))^2 )
  r2  = 1 - (SSE/SSY)

;  dt = a + (b[0]*xmin)

  PLOTS, [x0,x1], [y0,y1], THICK=2, COLOR=FSC_COLOR('red')

  charsize=0.8
  dx = xmax - xmin
  xl = xmin + ( dx * 0.05 )
  yl = xmin + ( dx * 0.9  )
  ;XYOUTS, xl, yl, STRING( rmsd, FORMAT='("RMSD:  ",f6.2," K")' ), CHARSIZE=charsize
  XYOUTS, xl, yl, STRING( davg, FORMAT='("BIAS:  ",f6.2," K")' ), CHARSIZE=charsize, FONT=0
  ;XYOUTS, xl, yl-(2*dx*0.05), STRING( xavg, FORMAT='("X-avg: ",f6.2," K")' ), CHARSIZE=charsize
  ;XYOUTS, xl, yl-(3*dx*0.05), STRING( yavg, FORMAT='("Y-avg: ",f6.2," K")' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(1*dx*0.07), STRING( rho, FORMAT='("rho:   ",f7.4)' ), CHARSIZE=charsize, FONT=0
;  XYOUTS, xl, yl-(2*dx*0.05), STRING( a, FORMAT='("dT =   ",f8.3)' ), CHARSIZE=charsize
  ;XYOUTS, xl, yl-(6*dx*0.05), STRING( b[0], FORMAT='("b = ",f7.4)' ), CHARSIZE=charsize
  ;XYOUTS, xl, yl-(7*dx*0.05), STRING( r2, FORMAT='("R!U2!N: ",f9.4)' ), CHARSIZE=charsize

  RETURN

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------

time = TIMEGEN( START=JULDAY(1,1,1995), FINAL=JULDAY(12,31,2009), UNITS='Days' )
ntime = N_ELEMENTS( time )
CALDAT, time, mm, dd, yy
doy = FIX(time - JULDAY(1,1,yy) + 1)

;;----------------------------------------------------------------------
;; Get data for all years
;;----------------------------------------------------------------------

xobs = MAKE_ARRAY( 3, ntime, /FLOAT, VALUE=-9999.99 )
yera = MAKE_ARRAY( 3, ntime, /FLOAT, VALUE=-9999.99  )
   
FOR year = 1995, 2009 DO BEGIN

   ;; index to start variable copy at
   istrt = WHERE( yy EQ year AND doy EQ 1, num )

   ;; Get ERA-Interim
   erai_fili = RAID_PATH('arctic6') + '/ERA_Interim/t2m/'  + STRTRIM(year,2) + $
               '/era_interim.t2m.hkh.station_locations.' + $
               STRTRIM(year,2) + '.txt'
   IF ( FILE_TEST( erai_fili ) EQ 1 ) THEN BEGIN
      erai = READ_ERAI_STATION( erai_fili )
      idy = istrt[0] + erai.doy - 1
      yera[0,idy] = erai.khunjerab
      yera[1,idy] = erai.naltar
      yera[2,idy] = erai.ziarat
   ENDIF
   
   ;; Get observations
   obs_diri = '/projects/CHARIS/streamflow/Pakistan/Data_from_DHashmi/'+$
              'from_danial/Data_for_Andrew_Barnett/climate' 
   
   khun_fili = obs_diri + '/khunjerab/khunjerab_' + STRTRIM(year,2) + '.qc1.txt'
   IF ( FILE_TEST( khun_fili ) EQ 1 ) THEN BEGIN
      khun = READ_WAPDA_CLIMATE( khun_fili )
      idy = istrt[0] + khun.doy - 1
      xobs[0,idy] = khun.tavg
   ENDIF

   nalt_fili = obs_diri + '/naltar/naltar_' + STRTRIM(year,2) + '.qc1.txt'
   IF ( FILE_TEST( nalt_fili ) EQ 1 ) THEN BEGIN
      nalt = READ_WAPDA_CLIMATE( nalt_fili )
      idy = istrt[0] + nalt.doy - 1
      xobs[1,idy] = nalt.tavg
   ENDIF
   
   ziar_fili = obs_diri + '/ziarat/ziarat_' + STRTRIM(year,2) + '.qc1.txt'
   IF ( FILE_TEST( ziar_fili ) EQ 1 ) THEN BEGIN
      ziar = READ_WAPDA_CLIMATE( ziar_fili )
      idy = istrt[0] + ziar.doy - 1
      xobs[2,idy] = ziar.tavg
   ENDIF
   
ENDFOR

notvalid = WHERE( xobs LE -9999., num_notvalid )
IF ( num_notvalid GT 0 ) THEN xobs[ notvalid ] = !VALUES.F_NAN
xobs = xobs+273.15

;STOP
   
;;----------------------------------------------------------------------
;; Make plot
;;----------------------------------------------------------------------

IF ( !D.NAME EQ 'X' ) THEN WINDOW, XSIZE=968, YSIZE=1120
!P.MULTI=[0,3,4]

;PLOT_XY, xobs[0,*], yera[0,*], TITLE='Khunjerab'
;PLOT_XY, xobs[1,*], yera[1,*], TITLE='Naltar'
;PLOT_XY, xobs[2,*], yera[2,*], TITLE='Ziarat'

thismonth = [1,4,7,10]
FOR im=0, 3 DO BEGIN

   imx = WHERE( mm EQ thismonth[im], nummon)

   IF ( im EQ 0 ) THEN title='Khunjerab' ELSE title=''
   PLOT_XY, xobs[0,imx], yera[0,imx], TITLE=title
   IF ( im EQ 0 ) THEN title='Naltar' ELSE title=''
   PLOT_XY, xobs[1,imx], yera[1,imx], TITLE=title
   IF ( im EQ 0 ) THEN title='Ziarat' ELSE title=''
   PLOT_XY, xobs[2,imx], yera[2,imx], TITLE=title

ENDFOR

END
