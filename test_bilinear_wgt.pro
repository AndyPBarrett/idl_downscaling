;; Define tile
tile = 'h23v05'

;; Get MODIS coordinates
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Get ERA-Interim coordinates
diri = '/disks/arctic6_raid/ERA_Interim/daily'
fili = 'era_interim.T2m.200806.nc'
elat = NCDF_READV( diri + '/' + fili, VARNAME='latitude' )
elon = NCDF_READV( diri + '/' + fili, VARNAME='longitude' )
t2m  = NCDF_READV( diri + '/' + fili, VARNAME='t2m' )
t2m = ( t2m * 0.00192523717167162 ) + 260.442670099742

nlat = N_ELEMENTS( elat )
nlon = N_ELEMENTS( elon )
elat2D = REBIN( TRANSPOSE( elat ), nlon, nlat )
elon2D = REBIN( elon, nlon, nlat )

tmp = t2m[*,*,2]

test = ( tmp[q11]*w11 ) + ( tmp[q12] * w12 ) + $
       ( tmp[q21]*w21 ) + ( tmp[q22] * w22 )

PRINT, MIN( tmp ), MAX( tmp )
PRINT, MIN( test ), MAX(test )

levels = ( FINDGEN(13)*10 ) + 200.

;; For testing plot MODIS and ERA-Interim points in Hunza
minlat = FLOOR( MIN( mlat ) )
maxlat = CEIL( MAX( mlat ) )
minlon = FLOOR( MIN( mlon ) )
maxlon = CEIL( MAX( mlon ) )
cntlat = MEAN( [ minlat, maxlat ] )
cntlon = MEAN( [ minlon, maxlon ] )

WINDOW, XS=900, YS=1200
!P.MULTI = [0,1,2]

idx = WHERE( ( elon2d GE minlon AND elon2d LE maxlon ) AND $
             ( elat2d GE minlat AND elat2d LE maxlat ), num )

HIST_PLOT, tmp[ idx ]
HIST_PLOT, test
STOP


MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, $
         LIMIT=[minlat, minlon, maxlat, maxlon]
CONTOUR, t2m[*,*,2], elon, elat, LEVELS=levels, /FILL, /OVERPLOT
MAP_CONTINENTS, /COUNTRIES, /HIRES
MAP_GRID, /box, latdel=1., londel=1.

MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, $
         LIMIT=[minlat, minlon, maxlat, maxlon], /NOERASE
CONTOUR, test, mlon, mlat, /CELL_FILL, LEVELS=levels, /OVERPLOT
MAP_CONTINENTS, /COUNTRIES, /HIRES
MAP_GRID, /box, latdel=1., londel=1.

END


