;;----------------------------------------------------------------------
;; Performs bilinear interpolation using a weights file
;;
;; 2013-06-19 A.P.Barrett
;;----------------------------------------------------------------------

;; Needs to be modified to deal with others files - scale and offset are
;;                                                  hard-coded
PRO GET_ERAI, fili, lat, lon, data

  lat  = NCDF_READV( fili, VARNAME='latitude' )
  lon  = NCDF_READV( fili, VARNAME='longitude' )
  data = NCDF_READV( fili, VARNAME='t2m' )
  data = ( data * 0.00192523717167162 ) + 260.442670099742

  ;; Reverse elat to make it monotonic increasing
  lat = REVERSE( lat )
  ;; ... and also tmp
  data = REVERSE( data, 2 )

  RETURN

END

;; Define tile
tile = 'h23v05'

;; Get ERA-Interim coordinates
diri = '/disks/arctic6_raid/ERA_Interim/daily'
fili = 'era_interim.T2m.200806.nc'
GET_ERAI, diri + '/' + fili, elat, elon, t2m

;; Restore wieghts and indices
wgts_file = tile + '_binlinear_wgt.sav'
RESTORE, wgts_file

tmp = t2m[*,*,20]

tnew = ( tmp[ i0 ] * w0 ) + ( tmp[ i1 ] * w1 ) + $
       ( tmp[ i2 ] * w2 ) + ( tmp[ i3 ] * w3 )

END


