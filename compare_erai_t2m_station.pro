;;----------------------------------------------------------------------
;; Compares T2m from ERA-Interim with station observations of T2m.
;;
;; 2014-03-25 A.P.Barrett
;;----------------------------------------------------------------------

PRO PLOT_XY, x, y, TITLE=title

  idx = WHERE( x GT -9998 AND y GT -9998, num )
  IF ( num GT 0 ) THEN BEGIN
     x = x[idx]
     y = y[idx]
  ENDIF
  print, num

  reso = 2.
  xmin = FLOOR( MIN( [x,y] )/reso ) * reso
  xmax = CEIL( MAX( [x,y] )/reso ) * reso
  
  print, xmin, xmax

  A = FINDGEN(31) * (!PI*2/30.)
  USERSYM, COS(A), SIN(A), /FILL

  PLOT, x, y, PSYM=8, SYMSIZE=0.5, $
        XRANGE=[xmin,xmax], XSTYLE=1, XTITLE='Observed (K)', $
        YRANGE=[xmin,xmax], YSTYLE=1, YTITLE='Model (K)', $
        /ISOTROPIC, TITLE=title
  PLOTS, !X.CRANGE, !Y.CRANGE

  diff = x - y
  davg = MEAN( diff )
  rmsd = SQRT( MEAN( diff^2 ) )
  xavg = MEAN( x )
  yavg = MEAN( y )
  rho = CORRELATE( x, y )
  
  b = REGRESS( x, y, CONST=a, YFIT=yhat )
  x0 = MIN( x )
  x1 = MAX( x )
  y0 = a + ( b[0]*x0 )
  y1 = a + ( b[0]*x1 )
  error = y - yhat
  sse = TOTAL( error^2 )
  ssy = TOTAL( (y-MEAN(y))^2 )
  r2  = 1 - (SSE/SSY)

  PLOTS, [x0,x1], [y0,y1], THICK=2, COLOR=FSC_COLOR('red')

  charsize=0.5
  dx = xmax - xmin
  xl = xmin + ( dx * 0.05 )
  yl = xmin + ( dx * 0.9  )
  XYOUTS, xl, yl, STRING( rmsd, FORMAT='("RMSD:  ",f6.2," K")' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(dx*0.05), STRING( davg, FORMAT='("BIAS:  ",f6.2," K")' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(2*dx*0.05), STRING( xavg, FORMAT='("X-avg: ",f6.2," K")' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(3*dx*0.05), STRING( yavg, FORMAT='("Y-avg: ",f6.2," K")' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(4*dx*0.05), STRING( rho, FORMAT='("rho:   ",f7.4)' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(5*dx*0.05), STRING( a, FORMAT='("a =   ",f8.3)' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(6*dx*0.05), STRING( b[0], FORMAT='("b = ",f7.4)' ), CHARSIZE=charsize
  XYOUTS, xl, yl-(7*dx*0.05), STRING( r2, FORMAT='("R!U2!N: ",f9.4)' ), CHARSIZE=charsize

  RETURN

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------

year = 1995

erai = READ_ERAI_STATION( RAID_PATH('arctic6') + $
                          '/ERA_Interim/t2m/'  + STRTRIM(year,2) + '/era_interim.t2m.hkh.station_locations.' + $
                          STRTRIM(year,2) + '.txt')

obs_diri = '/projects/CHARIS/streamflow/Pakistan/Data_from_DHashmi/'+$
           'from_danial/Data_for_Andrew_Barnett/climate' 

khun_fili = obs_diri + '/khunjerab/khunjerab_' + STRTRIM(year,2) + '.txt'
khun = READ_WAPDA_CLIMATE( khun_fili )

nalt_fili = obs_diri + '/naltar/naltar_' + STRTRIM(year,2) + '.txt'
nalt = READ_WAPDA_CLIMATE( nalt_fili )

ziar_fili = obs_diri + '/ziarat/ziarat_' + STRTRIM(year,2) + '.txt'
ziar = READ_WAPDA_CLIMATE( ziar_fili )

!P.MULTI=[0,3,5]

PLOT_XY, khun.tavg+273.15, erai.khunjerab, TITLE='Khunjerab'
PLOT_XY, nalt.tavg+273.15, erai.naltar, TITLE='Naltar'
PLOT_XY, ziar.tavg+273.15, erai.ziarat, TITLE='Ziarat'

thismonth = [1,4,7,10]
FOR im=0, 3 DO BEGIN

   imx = WHERE( erai.month EQ thismonth[im], nummon)

   PLOT_XY, khun[imx].tavg+273.15, erai[imx].khunjerab
   PLOT_XY, nalt[imx].tavg+273.15, erai[imx].naltar
   PLOT_XY, ziar[imx].tavg+273.15, erai[imx].ziarat

ENDFOR

END
