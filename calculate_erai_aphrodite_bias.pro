
;; Get ERA-Interim temperatures
erai_diri = RAID_PATH('arctic6')+'/'+'ERA_Interim/t2m'
erai_fili = 'erai_mean_daily_tair_1979to2007.nc'
erai = NCDF_READV(erai_diri + '/' + erai_fili, VARNAME='t2m')
ismissing = WHERE( erai LE -999., numissing )
IF ( numissing GT 0 ) THEN erai[ ismissing ] = !VALUES.F_NAN

lat  = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lat' )
lon  = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lon' )

;; Get Aphrodite temperatures
aphr_diri = RAID_PATH('arctic6')+'/'+'APHRODITE/AphroTemp/V1204R1/APHRO_MA/050deg'
aphr_fili = 'aphrodite_mean_daily_tair_1979to2007.erai_0.5deg.nc'
aphr = NCDF_READV(aphr_diri + '/' + aphr_fili, VARNAME='t2m')
;; Convert to Kelvin
ismissing = WHERE( aphr LE -999., numissing )
IF ( numissing GT 0 ) THEN aphr[ ismissing ] = !VALUES.F_NAN
aphr = aphr + 273.15

;;----------------------------------------------------------------------
;; Calculate bias
;;----------------------------------------------------------------------
bias = erai - aphr

;; Calculate seasonal mean biases for display
djf_bias = MEAN( [ [[erai[*,*,0:58]-aphr[*,*,0:58]]], $
                   [[erai[*,*,334:364]-aphr[*,*,334:364]]] ], DIM=3, /NAN )
mam_bias = MEAN( erai[*,*,59:150]-aphr[*,*,59:150], DIM=3, /NAN )
jja_bias = MEAN( erai[*,*,151:242]-aphr[*,*,151:242], DIM=3, /NAN )
son_bias = MEAN( erai[*,*,243:333]-aphr[*,*,243:333], DIM=3, /NAN )

;;----------------------------------------------------------------------
;; Write bias to NetCDF
;;----------------------------------------------------------------------
global = {description: 'bias is aphrodite minus erai'}
WRITE_LT_DAILY_MEAN_TO_NETCDF, bias*(-1.), lat, lon, 'daily_aphrodite_minus_cfsr_erai.nc'

ssn_bias = [ [ [ djf_bias ] ], [ [ mam_bias ] ], [ [ jja_bias ] ], [ [ son_bias ] ] ]
global = {description: 'bias is aphrodite minus erai'}
WRITE_LT_DAILY_MEAN_TO_NETCDF, ssn_bias*(-1.), lat, lon, 'seasonal_aphrodite_minus_erai_bias.nc'

;;----------------------------------------------------------------------
;; Plot bias
;;----------------------------------------------------------------------
WINDOW, XSIZE=1000, YSIZE=900
!P.MULTI=[0,2,2]

PLOT_APHROTEMP, djf_bias, lat, lon, TMIN=-5., TMAX=5., TINT=0.5, $
                   TITLE='DJF'
PLOT_APHROTEMP, mam_bias, lat, lon, TMIN=-5., TMAX=5., TINT=0.5, $
                   TITLE='MAM', /ADVANCE
PLOT_APHROTEMP, jja_bias, lat, lon, TMIN=-5., TMAX=5., TINT=0.5, $
                   TITLE='JJA', /ADVANCE
PLOT_APHROTEMP, son_bias, lat, lon, TMIN=-5., TMAX=5., TINT=0.5, $
                   TITLE='SON', /ADVANCE

END
