;;----------------------------------------------------------------------
;; Batch file to generate downscaled T2m profiles for a given basin
;;
;; 2014-05-02 A.P.Barrett
;;----------------------------------------------------------------------

basin = 'hunza'
year = [ 1995, 1996, 1997, 1998, 1999, 2000, 2003, 2004, 2005, $
         2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]
nyear = N_ELEMENTS( year )

FOR iy=0, nyear-1 DO BEGIN

   PRINT, 'Processing T2m for ' + basin + ' ' + STRTRIM( year[iy], 2 )

   MAKE_BASIN_T2M_DSCALE_PROFILE, basin, year[iy]

   PRINT, ''

ENDFOR

END
