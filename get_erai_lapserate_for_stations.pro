;;----------------------------------------------------------------------
;; Extracts the lapse rates for WAPDA climate stations in the Hunza basin.
;;
;; 2014-03-07 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION GET_DAYAVG, x

   dims = SIZE( x, /DIMENSION )
   nlon = dims[0]
   nlat = dims[1]
   ntim = dims[2]

   nday = ntim / 4

   notvalid = WHERE( x LE -9998., num_notvalid )
   IF ( num_notvalid GT 0 ) THEN x[ notvalid ] = !VALUES.F_NAN

   xday = MAKE_ARRAY( nlon, nlat, nday, /FLOAT, VALUE=-9999.99 )

   id = 0
   FOR it=0, ntim-1, 4 DO BEGIN
      xday[*,*,id] = MEAN( x[*,*,it:it+3], DIM=3, /NAN )
      id = id + 1
   ENDFOR

   notvalid = WHERE( FINITE( xday ) EQ 0, num_notvalid )
   IF ( num_notvalid GT 0 ) THEN xday[ notvalid ] = -9999.99

   RETURN, xday

END
  
station  = [ 'Khunjrab',  'Naltar',  'Ziarat', 'Gilgit', 'Islamabad' ]
mlat     = [  36.850000, 36.160300, 36.798000,  35.9219,     33.7167 ] 
mlon     = [  75.400000, 74.179883, 74.482000,  72.2892,     73.0667 ]
nstat = N_ELEMENTS( station )

erai_diri = RAID_PATH('arctic6') + '/ERA_Interim/daily'
erai_fili = 'era_interim.lapserate.hkh.200101.nc'
lat = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lat' )
lon = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lon' )

lat2d = REBIN( TRANSPOSE(lat), N_ELEMENTS(lon), N_ELEMENTS(lat), /SAMPLE )
lon2d = REBIN( lon, N_ELEMENTS(lon), N_ELEMENTS(lat), /SAMPLE )
latg = (lat + SHIFT(lat,1))*0.5
long = (lon + SHIFT(lon,1))*0.5

idx = WHERE( ( lat2d GE lat0 AND lat2d LE lat1 ) AND $
             ( lon2d GE lon0 AND lon2d LE lon1 ), numlat )
lat2d_sub = lat2d[idx]
lon2d_sub = lon2d[idx]

;;----------------------------------------------------------------------
;; Find nearest ERA-I grid cell to stations
;;----------------------------------------------------------------------
grid_id = MAKE_ARRAY( nstat, /LONG )
grid_ij = MAKE_ARRAY( 2, nstat, /LONG )

FOR im=0, nstat-1 DO BEGIN
   idx = WHERE( lat2d GE FLOOR(mlat[im]) AND lat2d LE CEIL(mlat[im]) AND $
                lon2d GE FLOOR(mlon[im]) AND lon2d LE CEIL(mlon[im]), num )
   dist = HAVERSINE( mlon[im], mlat[im], lon2d[idx], lat2d[idx] )
   nearest = idx[ SORT( dist ) ]
   grid_id[im] = nearest[0]
   grid_ij[*,im] = IDX2IJK( nearest[0], [N_ELEMENTS(lon), N_ELEMENTS(lat)] )
ENDFOR

;;----------------------------------------------------------------------
;; extract lapse rates for cells containing stations
;;----------------------------------------------------------------------
year = 2001
year_arr        = MAKE_ARRAY( 366, /FLOAT, VALUE=year )
month_arr       = MAKE_ARRAY( 366, /FLOAT, VALUE=-9999.99 )
day_arr         = MAKE_ARRAY( 366, /FLOAT, VALUE=-9999.99 )
lapse_rates_up  = MAKE_ARRAY( 3, 366, /FLOAT, VALUE=-9999.99 )
lapse_rates_inv = MAKE_ARRAY( 3, 366, /FLOAT, VALUE=-9999.99 )

k = 0
FOR im=0, 11 DO BEGIN
   month = im+1
   erai_fili = 'era_interim.lapserate.hkh.'+STRING(year,month,FORMAT='(i4,i02)')+'.nc'

   gamma0 = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='gamma0' )
   gamma1 = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='gamma1' )

   ;; Get daily mean lapse rate
   gamma0_dy = GET_DAYAVG( gamma0 )
   gamma1_dy = GET_DAYAVG( gamma1 )

   dims = SIZE( gamma0_dy, /DIMENSIONS )
   ntim = dims[2]

   ist = grid_ij[0,0:2]
   jst = grid_ij[0,0:2]
 
   month_arr[k:k+ntim-1]         = month
   day_arr[k:k+ntim-1]           = INDGEN(ntim)+1
   lapse_rates_up[*,k:k+ntim-1]  = [ [ gamma0_dy[ist[0],jst[0],0:ntim-1] ], $
                                     [ gamma0_dy[ist[1],jst[1],0:ntim-1] ], $
                                     [ gamma0_dy[ist[2],jst[2],0:ntim-1] ] ]
   lapse_rates_inv[*,k:k+ntim-1] = [ [ gamma1_dy[ist[0],jst[0],0:ntim-1] ], $
                                     [ gamma1_dy[ist[1],jst[1],0:ntim-1] ], $
                                     [ gamma1_dy[ist[2],jst[2],0:ntim-1] ] ]

   k = k + ntim

ENDFOR

nrec = k

lapse_rates_up  = lapse_rates_up[*,0:nrec-1]
lapse_rates_inv = lapse_rates_inv[*,0:nrec-1]

;; Convert to standard lapse rate; e.g. +ve cool with elevation, units=C/km
isvalid = WHERE( lapse_rates_up GT -9998., num_valid )
IF ( num_valid GT 0 ) THEN lapse_rates_up[ isvalid ] = (-1000.)*lapse_rates_up[ isvalid ]
isvalid = WHERE( lapse_rates_inv GT -9998., num_valid )
IF ( num_valid GT 0 ) THEN lapse_rates_inv[ isvalid ] = (-1000.)*lapse_rates_inv[ isvalid ]

;;----------------------------------------------------------------------
;; Write lapse rates to file
;;----------------------------------------------------------------------
OPENW, U, 'era_interim.lapse_rate.hkh.station_locations.'+STRING(year,FORMAT='(i4)')+'.txt', $
       /GET_LUN
PRINTF, U, 'year'
PRINTF, U, 'month'
PRINTF, U, 'day'
PRINTF, U, station[0:2]+' upper lapse rate', FORMAT='(a)'
PRINTF, U, station[0:2]+' inversion lapse rate', FORMAT='(a)'
FOR ir=0, nrec-1 DO BEGIN
   PRINTF, U, year_arr[ir], month_arr[ir], day_arr[ir], $
          lapse_rates_up[*,ir], lapse_rates_inv[*,ir], $
          FORMAT='(i4,1x,i02,1x,i02,6(1x,f8.2))'
ENDFOR
CLOSE, U
FREE_LUN, U


;;----------------------------------------------------------------------
;; Plot points
;;----------------------------------------------------------------------

;; For checking plot points
latx = 36.
lonx = 74.
lat0 = 33.
lon0 = 70.
lat1 = 38.
lon1 = 77.

idx = WHERE( ( lat2d GE lat0 AND lat2d LE lat1 ) AND $
             ( lon2d GE lon0 AND lon2d LE lon1 ), numlat )

MAP_SET, 36., 74., /LAMBERT, /ISOTROPIC, LIMIT=[33.,70.,38.,77.]
MAP_CONTINENTS, /COUNTRIES, /HIRES
;MAP_GRID
PLOTS, mlon, mlat, PSYM=2, THICK=2, COLOR=FSC_COLOR('red')
PLOTS, lon2d[idx], lat2d[idx], PSYM=1, THICK=1, COLOR=FSC_COLOR('green')

idx = WHERE( latg GE lat0 AND latg LE lat1, num )
FOR il=0, num-1 DO PLOTS, MAP_2POINTS( lon0, latg[idx[il]], lon1, latg[idx[il]], DPATH=0.1 ), $
                          COLOR=FSC_COLOR('green')
idx = WHERE( long GE lon0 AND long LE lon1, num )
FOR il=0, num-1 DO PLOTS, MAP_2POINTS( long[idx[il]], lat0, long[idx[il]], lat1, DPATH=0.1 ), $
                          COLOR=FSC_COLOR('green')

XYOUTS, mlon+0.1, mlat+0.1, station

FOR im=0, nstat-1 DO $
   PLOTS, lon2d[ grid_ij[0,im], grid_ij[1,im] ], lat2d[ grid_ij[0,im], grid_ij[1,im] ], $
          PSYM=2, COLOR=FSC_COLOR('yellow')


END
