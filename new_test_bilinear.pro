;; Define tile
tile = 'h23v05'

;; Get MODIS coordinates
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Get ERA-Interim coordinates
diri = '/disks/arctic6_raid/ERA_Interim/daily'
fili = 'era_interim.T2m.200806.nc'
elat = NCDF_READV( diri + '/' + fili, VARNAME='latitude' )
elon = NCDF_READV( diri + '/' + fili, VARNAME='longitude' )
t2m  = NCDF_READV( diri + '/' + fili, VARNAME='t2m' )
t2m = ( t2m * 0.00192523717167162 ) + 260.442670099742
tmp = t2m[*,*,2]

;; Reverse elat to make it monotonic increasing
elat = REVERSE( elat )
;; ... and also tmp
tmp = REVERSE( tmp, 2 )

;; Find closest row and column of ERA-I data to top row of MODIS tile
mlat0 = mlat[0,0]
mlon0 = mlon[0,0]

ilat = VALUE_LOCATE( elat, mlat0 )
ilon = VALUE_LOCATE( elon, mlon0 )

ispt = WHERE( ( mlon GE elon[ilon] AND mlon LE elon[ilon+1] ) AND $
              ( mlat GE elat[ilat] AND mlat LE elat[ilat+1] ), $
              numpt )
tnew = MAKE_ARRAY( numpt, /FLOAT )

FOR ii=0, numpt-1 DO BEGIN

   w = BILINT_WGT( mlon[ispt[ii]], mlat[ispt[ii]], elon[ilon], elon[ilon+1], $
                   elat[ilat], elat[ilat+1] )
   tnew[ii] = ( tmp[ilon,ilat]*w[0] ) + ( tmp[ilon+1,ilat]*w[1] ) + $
          ( tmp[ilon,ilat+1]*w[2] ) + ( tmp[ilon+1,ilat+1]*w[3] )
   
   print, ii
   print, tmp[ilon:ilon+1,ilat:ilat+1]
   print, w
   print, tnew[ii]

;   STOP

ENDFOR

h = HISTOGRAM( tnew, MIN=305., NBINS=7, BINSIZE=0.5, REVERSE_INDICES=ri )
nh = N_ELEMENTS( h )
color = BYTSCL( FINDGEN(nh), TOP=240 ) + 16

minlat = FLOOR(elat[ilat])
maxlat = CEIL(elat[ilat+1])
minlon = FLOOR(elon[ilon])
maxlon = CEIL(elon[ilon+1])

WINDOW, XS=900, YS=900
PLOT, elon, elat, PSYM=2, $
      XRANGE=[minlon,maxlon+0.2], XSTYLE=1, $
      YRANGE=[minlat,maxlat], YSTYLE=1, $
      /NODATA
PLOTS, elon[ilon], elat[ilat], PSYM=2
PLOTS, elon[ilon+1], elat[ilat], PSYM=2
PLOTS, elon[ilon], elat[ilat+1], PSYM=2
PLOTS, elon[ilon+1], elat[ilat+1], PSYM=2

FOR ih=0, nh-1 DO BEGIN
   
   IF ( ri[ih] NE ri[ih+1] ) THEN BEGIN
      
      isit = ri[ ri[ih]:ri[ih+1]-1 ]
      PLOTS, mlon[ispt[isit]], mlat[ispt[isit]], PSYM=3, COLOR=color[ih]

   ENDIF
   
ENDFOR

END


