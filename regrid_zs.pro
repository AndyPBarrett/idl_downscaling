
rearth = 6.37e6 ; radius of Earth
g = 9.80665

DEBUG = 0

z0 = NCDF_READV( 'ERA_Interim.Z.sigma1.nc', VARNAME='z' )
z0 = 25877.7788042423 + ( z0 * 0.821293413904657 )

latin = NCDF_READV( 'ERA_Interim.ZS.invariant.nc', VARNAME='latitude' )
lonin = NCDF_READV( 'ERA_Interim.ZS.invariant.nc', VARNAME='longitude' )
;latin = REVERSE( latin )

latout = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.1979.nc', VARNAME='lat' )
lonout = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.1979.nc', VARNAME='lon' )

nlato = N_ELEMENTS( latout )
nlono = N_ELEMENTS( lonout )
nlati = N_ELEMENTS( latin )
nloni = N_ELEMENTS( lonin )

weight = MAKE_ARRAY( nlono, nlato, 4, /FLOAT )
lat_index  = MAKE_ARRAY( nlono, nlato, 4, /LONG )
lon_index  = MAKE_ARRAY( nlono, nlato, 4, /LONG )

zout = MAKE_ARRAY( nlono, nlato, /FLOAT )

FOR ilon=0, nlono-1 DO BEGIN

   FOR ilat=0, nlato-1 DO BEGIN

      ;; Find nearest longitude points
      ii = VALUE_LOCATE( lonin, lonout[ilon] )

      ;; Find nearest latitude points on either side of location
      jj = VALUE_LOCATE( latin, latout[ilat] )

;      PRINT, lonout[ ilon ], latout[ ilat ]
;      PRINT, lonin[ ii ], lonin[ ii+1 ]
;      PRINT, latin[ jj ], latin[ jj+1 ]
;      PRINT, z0[ ii, jj ], z0[ ii+1, jj ], z0[ ii, jj+1 ], z0[ ii+1, jj+1 ]

      IF ( ii LT nloni-1 ) THEN BEGIN

         x1 = lonin[ ii ]
         x2 = lonin[ ii+1 ]
         y1 = latin[ jj ]
         y2 = latin[ jj+1 ]
         zin = [z0[ ii, jj ], z0[ ii+1, jj ], z0[ ii, jj+1 ], z0[ ii+1, jj+1 ]]

      ENDIF ELSE BEGIN

         x1 = lonin[ ii ]
         x2 = 360.
         y1 = latin[ jj ]
         y2 = latin[ jj+1 ]
         zin = [z0[ ii, jj ], z0[ 0, jj ], z0[ ii, jj+1 ], z0[ 0, jj+1 ]]

      ENDELSE

      zout[ilon,ilat] = BILINT( lonout[ ilon ], latout[ ilat ], zin, x1, x2, y1, y2 )

;      PRINT, zout[ilon,ilat]
;      STOP

   ENDFOR

ENDFOR

IMSIZE, zout, x0, y0, xs, ys
TVSCL, CONGRID( zout, xs, ys ), x0, y0
PLOT, RANDOMU(seed,10), RANDOMU(seed,10), $
      XRANGE=[0,nloni], XSTYLE=1, $
      YRANGE=[0,nlati], YSTYLE=1, $
      POSITION=[x0,y0,x0+xs,y0+ys], $
      /NOERASE, /DEVICE, /NODATA

;; Write to netcdf
filo = 'ERA_Interim.ZS.invariant.hires.nc'
cdfid = NCDF_CREATE( filo, /CLOBBER )

lat_dimid = NCDF_DIMDEF( cdfid, 'lat', nlato )
lon_dimid = NCDF_DIMDEF( cdfid, 'lon', nlono )

latid = NCDF_VARDEF( cdfid, 'lat', lat_dimid, /FLOAT )
lonid = NCDF_VARDEF( cdfid, 'lon', lon_dimid, /FLOAT )
varid = NCDF_VARDEF( cdfid, 'zs', [ lon_dimid, lat_dimid ], /FLOAT )

NCDF_ATTPUT, cdfid, latid, 'long_name', 'latitude'
NCDF_ATTPUT, cdfid, latid, 'units', 'degree_north'
NCDF_ATTPUT, cdfid, lonid, 'long_name', 'longitude'
NCDF_ATTPUT, cdfid, lonid, 'units', 'degree_east'
NCDF_ATTPUT, cdfid, varid, 'long_name', 'surface geopotential'
NCDF_ATTPUT, cdfid, varid, 'units', 'm**2 s**-2'

NCDF_ATTPUT, cdfid, 'description', 'regrided surface geopotential', /GLOBAL
NCDF_ATTPUT, cdfid, 'created_by', 'Andrew P. Barrett <apbarret@nsidc.org>', /GLOBAL
NCDF_ATTPUT, cdfid, 'created', SYSTIME(), /GLOBAL

NCDF_CONTROL, cdfid, /ENDEF

NCDF_VARPUT, cdfid, latid, latout
NCDF_VARPUT, cdfid, lonid, lonout
NCDF_VARPUT, cdfid, varid, zout

END

