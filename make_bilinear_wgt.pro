;;----------------------------------------------------------------------
;; Finds z(x,y) using bilinear interpolation.
;;
;; x - x-coord to be estimated
;; y - y-coord to be estimated
;; z - 4-element array [ z(x1,y1), z(x2,y1), z(x1,y2), z(x2,y2) ]
;; x1,x2 - x-coords
;; y1,y2 - y-coords
;;----------------------------------------------------------------------
DEBUG=0

;; Define tile
tile = 'h25v06'

;; Get MODIS coordinates
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Get ERA-Interim coordinates
diri = '/disks/arctic6_raid/ERA_Interim/daily'
fili = 'era_interim.lapserate.hkh.200801.nc'
elat = NCDF_READV( diri + '/' + fili, VARNAME='lat' )
elat = REVERSE( elat ) ; to monotonic increasing
elon = NCDF_READV( diri + '/' + fili, VARNAME='lon' )
nlat = N_ELEMENTS( elat )
nlon = N_ELEMENTS( elon )
elat2D = REBIN( TRANSPOSE( elat ), nlon, nlat )
elon2D = REBIN( elon, nlon, nlat )

;; For testing plot MODIS and ERA-Interim points in Hunza
minlat = FLOOR( MIN( mlat ) )
maxlat = CEIL( MAX( mlat ) )
minlon = FLOOR( MIN( mlon ) )
maxlon = CEIL( MAX( mlon ) )
cntlat = MEAN( [ minlat, maxlat ] )
cntlon = MEAN( [ minlon, maxlon ] )


;; Define result arrays
q11 = MAKE_ARRAY( ncol, nrow, /LONG )
q21 = MAKE_ARRAY( ncol, nrow, /LONG )
q12 = MAKE_ARRAY( ncol, nrow, /LONG )
q22 = MAKE_ARRAY( ncol, nrow, /LONG )
w11 = MAKE_ARRAY( ncol, nrow, /FLOAT )
w21 = MAKE_ARRAY( ncol, nrow, /FLOAT )
w12 = MAKE_ARRAY( ncol, nrow, /FLOAT )
w22 = MAKE_ARRAY( ncol, nrow, /FLOAT )

;nmod = 100
FOR imod=0, nmod-1 DO BEGIN

;; Find indices of 4 corner cells in ERA-Interim grid
   ilat = VALUE_LOCATE( elat, mlat[imod] )
   ilon = VALUE_LOCATE( elon, mlon[imod] )
   ipt  = ( (ilat+[0,1,0,1]) * nlon ) + ilon+[0,0,1,1]

   weight = BILINT_WGT( mlon[imod], mlat[imod], $
                        elon[ilon], elon[ilon+1], $
                        elat[ilat], elat[ilat+1] )

   PRINT, imod
   PRINT, ipt
   print, weight

   q11[ imod ] = ipt[0]
   q21[ imod ] = ipt[1]
   q12[ imod ] = ipt[2]
   q22[ imod ] = ipt[3]

   w11[ imod ] = weight[0]
   w21[ imod ] = weight[1]
   w12[ imod ] = weight[2]
   w22[ imod ] = weight[3]

   IF ( DEBUG EQ 1 ) THEN BEGIN

      PRINT, ii
      PRINT, mlon[idx[ii]], mlat[idx[ii]]
      PRINT, elon[ilon], elat[ilat]
      PRINT, weight
   
      MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, $
               LIMIT=[minlat, minlon, maxlat, maxlon]
      MAP_CONTINENTS, /COUNTRIES, /HIRES
      MAP_GRID, /box, latdel=1., londel=1.
      OPLOT, elon2D, elat2D, PSYM=2, COLOR=FSC_COLOR('red')
      PLOTS, mlon[idx[ii]], mlat[idx[ii]], PSYM=2, COLOR=FSC_COLOR('green')
      PLOTS, elon2D[ ipt ], elat2D[ ipt ], PSYM=2, COLOR=FSC_COLOR('blue')

      STOP

   ENDIF

ENDFOR

SAVE, q11, q21, q12, q22, w11, w21, w12, w22, $
      FILE='bilinear_wgt_hkh_'+tile+'_'+$
      STRING(nlat,FORMAT='(i03)')+'latx'+$
      STRING(nlon,FORMAT='(i03)')+'lon.sav'
 
;; Calculate weights


;; Write to file


END
