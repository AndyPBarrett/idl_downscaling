;;----------------------------------------------------------------------
;; A test routine/recipe to generate OI weights for MODIS Tiles.
;;
;; 2015-06-12 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION GET_WEIGHTS, ilat, ilon, olat, olon, $
                      RMAX=rmax, LSCALE=lscale, EPS2=EPS2

  ;; define parameters if they have not been set
  rmax   = N_ELEMENTS( rmax ) EQ 0 ? 300. : rmax
  lscale = N_ELEMENTS( lscale ) EQ 0 ? 500. : lscale
  eps2   = N_ELEMENTS( eps2 ) EQ 0 ? 0.36 : eps2

  ;; Find number of stations
  nstat = N_ELEMENTS( olat )

  ;; Get distance of stations from analysis point
  dist = HAVERSINE( ilat, ilon, olat, olon )

  ;; Initialize weights array
  weight = MAKE_ARRAY( nstat, /DOUBLE, VALUE=0. )

  ;; Find stations within search radius
  include = WHERE( dist LT rmax, num_include )
  IF ( num_include GT 0 ) THEN BEGIN

     ;; Subset coordinates to only include station within search radius
     tmp_lat = olat[include]
     tmp_lon = olon[include]

     ;; Make station-station distance matrix
     rk = MAKE_ARRAY( num_include, num_include, /DOUBLE, VALUE=0. )  
     ;; Populate distance matrix
     FOR i=0, num_include-1 DO BEGIN
        rk[*,i] = HAVERSINE( tmp_lat[i], tmp_lon[i], tmp_lat, tmp_lon )
     ENDFOR
     ;; Calculate normalized covariance matrix (correlation) 
     rho_k = (1+(rk/lscale))*EXP((-1.)*(rk/lscale))
     
     ;; Define observation error matrix
     eps2_mtx = DIAG_MATRIX( REPLICATE( eps2, num_include ) )
     
     ;; Total error covariance
     error = rho_k + eps2_mtx
     
     ;; Calculate analysis to observation distance
     rb = HAVERSINE( ilat, ilon, tmp_lat, tmp_lon )
     ;; Calculate analysis to obs covariance
     rho_b = (1+(rb/lscale))*EXP((-1.)*(rb/lscale))
     
     ;; calculate weights
     IF ( num_include GT 1 ) THEN BEGIN
        tmp_wgt = INVERT( error, /DOUBLE ) ## REFORM(rho_b,1,num_include)
     ENDIF ELSE BEGIN
        tmp_wgt = rho_b / rho_k
     ENDELSE
     
     weight[ include ] = tmp_wgt

  ENDIF
 
  RETURN, weight

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------

rmax   = 300. ;; Maximum distance to include stations
lscale = 500. ;; length scale for temperature
eps2   = 0.36 ;; Observations error (instrument + representativeness)

;; Get modis tile coordinates
;; Define tile
tile = 'h23v05'

;; Get MODIS coordinates
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Analysis point
;ilat = 26.9
;ilon = 74.3

;; Observation stations
olat = [ 36.85, 36.1603, 36.85 ] 
olon = [ 75.4, 74.179883, 74.482 ]

;; This will only work for a set number of stations
wgt0 = MAKE_ARRAY( nrow, ncol, /DOUBLE, VALUE=0. )
wgt1 = MAKE_ARRAY( nrow, ncol, /DOUBLE, VALUE=0. )
wgt2 = MAKE_ARRAY( nrow, ncol, /DOUBLE, VALUE=0. )

FOR imod = 0, nmod-1 DO BEGIN

   PRINT, 'Processing point ' + STRTRIM(imod,2)

   weight = GET_WEIGHTS( mlat[imod], mlon[imod], olat, olon )

   PRINT, weight

   wgt0[ imod ] = weight[0]
   wgt1[ imod ] = weight[1]
   wgt2[ imod ] = weight[2]
   
ENDFOR

SAVE, wgt0, wgt1, wgt2, FILENAME='hunza_'+tile+'_oi_weights.sav'

END
