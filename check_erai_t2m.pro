
nrow = 2400
ncol = 2400

;; Get ERA-I cell addresses
cellid = GET_BASIN_DATA_APB( 'cell_address', 'hunza' )

;; Get Hunza basin mask
mask = GET_BASIN_DATA_APB( 'mask', 'hunza' )
isbasin = WHERE( mask EQ 1, num_basin )

;; Loop through ERA-I

year = 2005
diri = '/raid/ERA_Interim/t2m' + '/' + STRING(year,FORMAT='(i4)')
diro = '/raid/ERA_Interim/t2m' + '/' + STRING(year,FORMAT='(i4)')
  
IMSIZE, cellid, x0, y0, xs, ys

FOR im=0, 11 DO BEGIN
     
   month = im+1
     
   PRINT, '% Processing temperature data for month ' + STRING(year,month,FORMAT='(i4,"-",i02)')
     
   fili = 'era_interim.t2m.hkh.'+STRING(year,month,FORMAT='(i4,i02)')+'.nc'
   PRINT, '    getting data from ' + fili + '...'
   t2m = NCDF_READV( diri + '/' + fili, VARNAME='t2m' )
   
   dims = SIZE( t2m, /DIMENSIONS )
   ntim = dims[2]
     
   FOR it=0, ntim-1 DO BEGIN
      
      tmp = t2m[ *, *, it ]
      
      mtmp = tmp[ cellid ]
      
      TVSCL, CONGRID( mtmp, xs, ys ), x0, y0
      STOP
      
   ENDFOR
   
ENDFOR

END

