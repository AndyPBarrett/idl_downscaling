
imon = 0
level = ( FINDGEN( 15 ) * 5. ) + 235.

g = 9.81

z0 = NCDF_READV( 'ERA_Interim.Z.sigma1.nc', VARNAME='z' )
z0 = 25877.7788042423 + ( z0 * 0.821293413904657 )
z0 = z0 / g

lat = NCDF_READV( 'ERA_Interim.ZS.invariant.nc', VARNAME='latitude' )
lon = NCDF_READV( 'ERA_Interim.ZS.invariant.nc', VARNAME='longitude' )
latid = WHERE( lat GT 55 AND lat LT 85, num_latid )
lonid = WHERE( lon EQ 325., num_lonid )

zf = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.1979.nc', VARNAME='zf' )
ta = NCDF_READV( 'ERA_Interim.TA.sigma.48to60.monthly_mean.1979.synth.nc', VARNAME='ta' )

dims = SIZE( zf, /DIMENSION )
nlon = dims[0]
nlat = dims[1]
nlev = dims[2]
ntim = dims[3]

zf = zf / g
z = zf + z0[*,*,0]

lat2d = REBIN( lat, nlat, nlev, /SAMPLE )
lev2d = z[ 434, *, *, imon ]

PLOT, lat, z0[ 434, *, 0 ], THICK=2, $
      XRANGE=[-90.,90.], XSTYLE=1, XTITLE='Lat', $
      YRANGE=[0,5000.], YSTYLE=1, YTITLE='m', $
      /NODATA
CONTOUR, ta[ 434, *, *, imon ], lat2d, lev2d, LEVEL=level, /FILL, /IRREGULAR 

OPLOT, lat, z0[ 434, *, 0 ], THICK=2

END
