
;; Get MODIS coordinates
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )

h = HISTOGRAM( tnew, MIN=276., NBINS=13, BINSIZE=4, $
               REVERSE_INDICES=ri, LOCATION=location )
nh = N_ELEMENTS( h )
color = BYTSCL( FINDGEN(nh), TOP=240 ) + 16

minlat = FLOOR( MIN( mlat ) )-1
maxlat = CEIL( MAX( mlat ) )+1
minlon = FLOOR( MIN( mlon ) )-1
maxlon = CEIL( MAX( mlon ) )+1

ilat = WHERE( elat GE minlat AND elat LE maxlat, numlat )
ilon = WHERE( elon GE minlon AND elon LE maxlon, numlon )
tmp_sub = tmp[ ilon[0]:ilon[numlon-1], ilat[0]:ilat[numlat-1] ]

WINDOW, XS=900, YS=900
!P.MULTI = [0,1,2]

HIST_PLOT, tmp_sub, BINSIZE=2.5, MIN=270, TITLE='Original'
HIST_PLOT, tnew, BINSIZE=2.5, MIN=270, TITLE='Regridded'

PRINT, MEAN( tmp_sub ), STDDEV( tmp_sub )
PRINT, MEAN( tnew ), STDDEV( tnew )

STOP 

PLOT, elon, elat, PSYM=2, $
      XRANGE=[minlon,maxlon+0.2], XSTYLE=1, $
      YRANGE=[minlat,maxlat], YSTYLE=1, $
      /NODATA

FOR ih=0, nh-1 DO BEGIN
   IF ( ri[ih] NE ri[ih+1] ) THEN BEGIN
      isit = ri[ ri[ih]:ri[ih+1]-1 ]
      PLOTS, mlon[isit], mlat[isit], PSYM=3, COLOR=color[ih]
   ENDIF
ENDFOR

PLOT, elon, elat, PSYM=2, $
      XRANGE=[minlon,maxlon+0.2], XSTYLE=1, $
      YRANGE=[minlat,maxlat], YSTYLE=1, $
      /NODATA
CONTOUR, tmp, elon, elat, /FILL, LEVELS=location, C_COLOR=color, /OVERPLOT

END
