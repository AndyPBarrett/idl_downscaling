;;----------------------------------------------------------------------
;; Makes a comparison time series plot of observed 2m air temperatures and
;; temperatures downscaled from ERA-Interim using the Jarosch method.
;;
;; 2015-06-10 A.P.Barrett
;;----------------------------------------------------------------------

PRO PLOT_PANEL, obs, dscale1, dscale2, title

  isvalid = WHERE( obs.tavg GT -9999., num_isvalid )
  IF ( num_isvalid GT 0 ) THEN BEGIN
     ymin = MIN( obs[isvalid].tavg )
     ymax = MAX( obs[isvalid].tavg )
  ENDIF

  isvalid = WHERE( dscale1 GT -9999., num_isvalid )
  IF ( num_isvalid GT 0 ) THEN BEGIN
     ymin = MIN( [ ymin, dscale1[isvalid] ] )
     ymax = MAX( [ ymax, dscale1[isvalid] ] )
  ENDIF
  
  isvalid = WHERE( dscale2 GT -9999., num_isvalid )
  IF ( num_isvalid GT 0 ) THEN BEGIN
     ymin = FLOOR( MIN( [ ymin, dscale2[isvalid] ] ) / 5. ) * 5.
     ymax = CEIL( MAX( [ ymax, dscale2[isvalid] ] ) / 5. ) * 5. 
  ENDIF
  
  PLOT, obs.tavg, dscale1, $
      XRANGE=[ymin,ymax], XSTYLE=1, XTITLE='Observed (!Uo!NC)', $
      YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Downscaled (!Uo!NC)', $
      TITLE=title, THICK=2, /NODATA, /ISOTROPIC
  PLOTS, !X.CRANGE, !Y.CRANGE

  isvalid = WHERE( obs.tavg GT -9999. AND dscale1 GT -9999., num_valid )
  IF ( num_valid GT 0 ) THEN BEGIN

     rho = CORRELATE( obs[isvalid].tavg, dscale1[isvalid] )
     bias = MEAN( obs[isvalid].tavg - dscale1[isvalid], /NAN )
     rmse = SQRT( MEAN( (obs[isvalid].tavg - dscale1[isvalid])^2, /NAN ) )

     OPLOT, obs[isvalid].tavg, dscale1[isvalid], PSYM=1, COLOR=FSC_COLOR('red'), THICK=2
     x0 = ymin+( (ymax-ymin)*0.1 )
     y0 = ymin+( (ymax-ymin)*0.9 )
     dy = (ymax-ymin)*0.05
     XYOUTS, x0, y0,        STRING( rho,  FORMAT='("rho  = ",f9.4)' ), /DATA, COLOR=FSC_COLOR('red')
     XYOUTS, x0, y0-dy,     STRING( bias, FORMAT='("bias = ",f9.4)' ), /DATA, COLOR=FSC_COLOR('red')
     XYOUTS, x0, y0-(2*dy), STRING( rmse, FORMAT='("RMSE = ",f9.4)' ), /DATA, COLOR=FSC_COLOR('red')

  ENDIF

  isvalid = WHERE( obs.tavg GT -9999. AND dscale2 GT -9999., num_valid )
  IF ( num_valid GT 0 ) THEN BEGIN

     rho = CORRELATE( obs[isvalid].tavg, dscale2[isvalid] )
     bias = MEAN( obs[isvalid].tavg - dscale2[isvalid], /NAN )
     rmse = SQRT( MEAN( (obs[isvalid].tavg - dscale2[isvalid])^2, /NAN ) )

     OPLOT, obs[isvalid].tavg, dscale2[isvalid], PSYM=1, COLOR=FSC_COLOR('cyan'), THICK=2
     x0 = ymin+( (ymax-ymin)*0.1 )
     y0 = ymin+( (ymax-ymin)*0.9 )
     dy = (ymax-ymin)*0.05
     XYOUTS, x0, y0-(3*dy),        STRING( rho,  FORMAT='("rho  = ",f9.4)' ), /DATA, COLOR=FSC_COLOR('cyan')
     XYOUTS, x0, y0-(4*dy),     STRING( bias, FORMAT='("bias = ",f9.4)' ), /DATA, COLOR=FSC_COLOR('cyan')
     XYOUTS, x0, y0-(5*dy), STRING( rmse, FORMAT='("RMSE = ",f9.4)' ), /DATA, COLOR=FSC_COLOR('cyan')

  ENDIF

END


;;----------------------------------------------------------------------
;; Main routine
;;----------------------------------------------------------------------

year = 2001

;;----------------------------------------------------------------------
;; Get data
;;----------------------------------------------------------------------

;; Observations
obs_diri = '/projects/CHARIS/surface_met_data/WAPDA'
fili = STRING( obs_diri, 'khunjerab', 'khunjerab', year, FORMAT='(a,"/",a,"/",a,"_",i4,".qc1.txt")' )
khun = READ_CHARIS_CLIMATE( fili, NREC=n_khun )

fili = STRING( obs_diri, 'naltar', 'naltar', year, FORMAT='(a,"/",a,"/",a,"_",i4,".qc1.txt")' )
nalt = READ_CHARIS_CLIMATE( fili, NREC=n_nalt )

fili = STRING( obs_diri, 'ziarat', 'ziarat', year, FORMAT='(a,"/",a,"/",a,"_",i4,".qc1.txt")' )
ziar = READ_CHARIS_CLIMATE( fili, NREC=n_ziar )

;; Downscaled - with inversion
dscale_inv = READ_DOWNSCALE_VALIDATION( 'downscaled_tair_hunza_validation.txt', station, NREC=ninv )

;; Downscaled - no inversion
dscale_noinv = READ_DOWNSCALE_VALIDATION( 'downscaled_tair_hunza_validation_noinversion.txt', station, NREC=nnoinv )

;;----------------------------------------------------------------------
;; Make time series plot
;;----------------------------------------------------------------------

!P.MULTI=[0,3,1]

PLOT_PANEL, khun, dscale_inv.tair[0]-273.15, dscale_noinv.tair[0]-273.15, 'Khunjerab'
PLOT_PANEL, nalt, dscale_inv.tair[1]-273.15, dscale_noinv.tair[1]-273.15, 'Naltar'
PLOT_PANEL, ziar, dscale_inv.tair[2]-273.15, dscale_noinv.tair[2]-273.15, 'Ziarat'

!P.MULTI=0

END
 
