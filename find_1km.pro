
g = 9.80665 ; gravity m/s**2

thislevel = 1000.

z = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.2001.nc', VARNAME='zf' )
ta = NCDF_READV( 'ERA_Interim.TA.sigma.48to60.monthly_mean.2001.synth.nc', VARNAME='ta' )
lat = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.2001.nc', VARNAME='lat' )
lon = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.2001.nc', VARNAME='lon' )
lev = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.2001.nc', VARNAME='lev' )
time = NCDF_READV( 'ERA_Interim.zfull.sigma.48to60.monthly_mean.2001.nc', VARNAME='time' )

nlat = N_ELEMENTS( lat )
nlon = N_ELEMENTS( lon )
nlev = N_ELEMENTS( lev )
ntim = N_ELEMENTS( time )

isub = 200
jsub = 200
z = z[ isub:isub+2, jsub:jsub+2, *, 0:1 ]
ta = ta[ isub:isub+2, jsub:jsub+2, *, 0:1 ]
nlon = 3
nlat = 3
ntim = 2

nshift = nlon * nlat

t1000 = MAKE_ARRAY( nlon, nlat, ntim, /FLOAT, VALUE=-9999.99 ) 

FOR it = 0, ntim-1 DO BEGIN

   ztmp = REFORM( z[ *, *, *, it ] )
   ttmp = REFORM( ta[ *, *, *, it ] )

   levidx = MAKE_ARRAY( nlon, nlat, /LONG, VALUE=-1 )

   FOR il = 0, nlev -1 DO BEGIN

      idx = WHERE( ztmp[*, *, il ] GT thislevel, count )
      IF ( count GT 0 ) THEN levidx[idx] = idx + (nshift*il)

   ENDFOR

   dta = ttmp[ levidx ] - ttmp[ levidx+nshift ]
   dz  = ztmp[ levidx ] - ztmp[ levidx+nshift ]
   deltaz = thislevel - ztmp[ levidx+nshift ]
   dtadz = dta / dz

   t1000[*,*,it] = ttmp[ levidx+nshift ] + ( deltaz * dtadz )

   FOR ilon=0, nlon-1 DO BEGIN
      FOR ilat=0, nlat-1 DO BEGIN;

         PLOT, ttmp[ilon,ilat,*], ztmp[ilon,ilat,*], PSYM=-2, $
               TITLE=STRING( it, ilon, ilat, FORMAT='("t=",i2," lat=",i2," lon=",i2)' )
         PLOTS, !X.CRANGE, 1000
         PLOTS, ttmp[levidx[ilat,ilon]], ztmp[levidx[ilat,ilon]], PSYM=2, COLOR=90
         PLOTS, ttmp[levidx[ilat,ilon]+nshift], ztmp[levidx[ilat,ilon]+nshift], PSYM=2, COLOR=90
         PLOTS, t1000[ilon,ilat], 1000., PSYM=2, COLOR=240

         STOP

      ENDFOR
   ENDFOR

ENDFOR

help, t1000

FOR it=0, ntim-1 DO BEGIN
   IMSIZE, t1000[*,*,it], x0, y0, xs, ys
   TVSCL, CONGRID( t1000[*,*,it], xs, ys ), x0, y0
   STOP
ENDFOR

END


