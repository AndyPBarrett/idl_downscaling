FUNCTION READ_6H_TAIR, fili

  xdim=2400
  ydim=2400
  tdim=4

  result = MAKE_ARRAY( xdim, ydim, tdim, /FLOAT )

  OPENR, U, fili, /GET_LUN
  READU, U, result
  CLOSE, U
  FREE_LUN, U

  RETURN, result

END

;;----------------------------------------------------------------------
;; Main routine
;;----------------------------------------------------------------------
PRO GET_DAILY_MEAN_TAIR, year, tile, DIRI=diri

diri = N_ELEMENTS( diri ) EQ 0 ? '.' : diri

;tile = 'h24v05'
;year = 2001

xdim = 2400
ydim = 2400

file = FINDFILE( diri + '/' + tile + '/' + STRTRIM(year,2) + '/' + $
                 'tsurf_'+tile+'_'+STRING(year,FORMAT='(i4)')+'????.bin', COUNT=nfile )

;;----------------------------------------------------------------------
;; Calc daily mean
;;----------------------------------------------------------------------
npt = 1
xidx = 185
yidx = 1507
y = MAKE_ARRAY( npt, nfile, /FLOAT )

FOR ifile=0, nfile-1 DO BEGIN

   PRINT, '% Processing temperatures from ' + file[ifile]
   data = READ_6H_TAIR( file[ifile] )

   dataAvg = MEAN( data, DIM=3 )

   filo = STR_REPLACE( file[ifile], 'tsurf_'+tile, 'tsurf_'+tile+'_day' )
   
   PRINT, '% Writing daily average temperature to ' + filo
   OPENW, U, filo, /GET_LUN
   WRITEU, U, dataAvg
   CLOSE, U
   FREE_LUN, U

ENDFOR

END
