
;fili = 'cfsr_mean_daily_tair_1979to2007.nc'
;fili = 'merra_mean_daily_tair_1979to2007.nc'
;fili = 'aphrodite_mean_daily_tair_1979to2007.merra_0.5deg.nc'
;fili = 'aphrodite_mean_daily_tair_1979to2007.erai_0.5deg.nc'
;fili = 'aphrodite_mean_daily_tair_1979to2007.nc'
fili = 'aphrodite_mean_daily_tair_1979to2007.cfsr_0.5deg.nc'

data = NCDF_READV( fili, VARNAME='t2m' )
lat  = NCDF_READV( fili, VARNAME='lat' )
lon  = NCDF_READV( fili, VARNAME='lon' )

FOR id=0, 365 DO BEGIN

   PLOT_APHROTEMP, data[*,*,id], lat, lon, TITLE=STRING(id+1,FORMAT='("Day = ",i3)')
   WAIT, 1
   ;STOP

ENDFOR

END
