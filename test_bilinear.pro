;; A routine to test bilinear interpolation

z = [[1.0,0.5],[0.0,1.0]]
xin = [[0.0,1.0],[0.0,1.0]]
yin = [[1.0,1.0],[0.0,0.0]]

xout = REBIN( FINDGEN( 11 ) / 10., 11, 11, /SAMPLE )
yout = REBIN( TRANSPOSE( FINDGEN( 11 ) / 10. ), 11, 11, /SAMPLE )

dx21 = xin[1,0] - xout
dx11 = xout - xin[0,0]
dx22 = xin[1,1] - xout
dx12 = xout - xin[0,1]

dy10 = yout - yin[1,1]
dy20 = yin[0,0] - yout

dx = xin[1,0]-xin[0,0]
dy = yin[1,0]-yin[0,0]

w11 = dx21 * dy20
w21 = dx11 * dy20
w12 = dx22 * dy10
w22 = dx12 * dy10

zout = ( z[0,1]*w11 ) + ( z[1,1]*w21 ) + ( z[0,0]*w22 ) + ( z[1,0]*w12 )
 
PLOT, RANDOMU(seed,10), RANDOMU(seed,10), $
      XRANGE=[-0.1,1.1], XSTYLE=1, XTITLE='X', $
      YRANGE=[-0.1,1.1], YSTYLE=1, YTITLE='Y', $
      POSITION=[0.1,0.1,0.9,0.9], $
      /NODATA



END
