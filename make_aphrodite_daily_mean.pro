;;----------------------------------------------------------------------
;; Calculates long-term daily mean temperature fields from Aphrodite AphroTemp
;; data
;;
;; 2014-11-14 A.P.Barrett <apbarret@nsidc.org>
;;----------------------------------------------------------------------

VERBOSE = 1

diri = '/raid/APHRODITE/AphroTemp/V1204R1/APHRO_MA/050deg'
ffmt = '("APHRO_MA_TAVE_050deg_V1204R1.",i4,".nc")'

nday = 365  ;; Number of days in a year (ignores 366)

ybeg = 1979
yend = 2007
nyear = yend - ybeg + 1

window = 31

;; Define limits to subset input array
i0 = 0
i1 = 100
j0 = 69
j1 = 130

;; Define array to hold results
di = i1 - i0 + 1
dj = j1 - j0 + 1
result = MAKE_ARRAY( di, dj, nday+1, /FLOAT )

;; Define calculation array - this is the same for each loop of id and gets
;;                            set to zero each time
ntim = nyear * window
carray = MAKE_ARRAY( di, dj, ntim, /FLOAT, VALUE=!VALUES.F_NAN )

;;----------------------------------------------------------------------
;; Loop through each day in the year
;;----------------------------------------------------------------------
FOR id = 0, nday-1 DO BEGIN

   PRINT, '% Processing day ' + STRTRIM(id,2) + '...'

   carray[ *, *, * ] = 0.0

   FOR iy = ybeg, yend DO BEGIN
      
      fili = diri + '/' + STRING( iy, FORMAT=ffmt )
      CASE 1 OF
         FILE_TEST( fili ) EQ 1: BEGIN
            isgz = 0
         END
         FILE_TEST( fili+'.gz' ) EQ 1: BEGIN
            isgz = 1
            IF ( VERBOSE EQ 1 ) THEN PRINT, '% Unzipping ' + fili+'.gz'
            SPAWN, 'gunzip ' + fili+'.gz'
         END
         ELSE: BEGIN
            PRINT, '% File not found'
            STOP
         END
      ENDCASE
      
      ;; Get the data
      data = NCDF_READV( fili, VARNAME='tave' )
      ;; Subset it
      tmp = data[ i0:i1, j0:j1, * ]

      ;; Set missing < -99.99 to NAN
      ismissing = WHERE( tmp LT -99., numissing )
      IF ( numissing GT 0 ) THEN tmp[ ismissing ] = !VALUES.F_NAN

      ;; Get data for time window
      tmp = TEMPORARY( (SHIFT( tmp, 0, 0, 15-id ))[*,*,0:30] )

      t0 = window*(iy-ybeg)
      t1 = t0 + 30
      carray[ *, *, t0:t1 ] = tmp

      IF ( isgz EQ 1 ) THEN BEGIN
         IF ( VERBOSE EQ 1 ) THEN PRINT, '% Zipping ' + fili
         SPAWN, 'gzip ' + fili
      ENDIF

   ENDFOR

   result[ *, *, id ] = MEAN( carray, DIM=3, /NAN )

ENDFOR

result[*,*,365] = result[*,*,364]

;; Set NANs to missing values -999.99
isnan = WHERE( FINITE( result ) EQ 0, numnan )
IF ( numnan GT 0 ) THEN result[ isnan ] = -999.99

;; Get lat and lon
fili = diri + '/' + STRING( 1979, FORMAT=ffmt )
lat = NCDF_READV( fili, VARNAME='latitude' )
lon = NCDF_READV( fili, VARNAME='longitude' )

;; Write to netcdf
filo = 'aphrodite_mean_daily_tair_1979to2007.nc'

;; -  define global attributes in structure
global_att = {history: 'Created by APHRODITE project team 28- 3月-2013', $
              coments: 'APHRO_MA V1204R1 daily precipitation with 0.50deg grids' }
WRITE_LT_DAILY_MEAN_TO_NETCDF, result, lat[j0:j1], lon[i0:i1], filo, GLOBAL=global_att, $
                               MISSING=-999.99


END
