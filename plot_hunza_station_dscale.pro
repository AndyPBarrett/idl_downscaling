
FUNCTION READ_DOWNSCALED_STATION, fili

  struct = {year:  0, $
            month: 0, $
            day:   0, $
            doy:   0, $
            khunjerab: -9999.99, $
            naltar:    -9999.99, $
            ziarat:    -9999.99, $
            gilgit:    -9999.99, $
            islamabad: -9999.99 }
  data = REPLICATE( struct, 366 )

  OPENR, U, fili, /GET_LUN

  line = ''
  ir = 0
  WHILE ( NOT EOF(U) ) DO BEGIN

     READF, U, line

     IF ( STREGEX( line, '^#', /BOOLEAN ) EQ 0 ) THEN BEGIN

        field = STRSPLIT( line, /EXTRACT )

        data[ir].year      = FIX( field[0] )
        data[ir].month     = FIX( field[1] )
        data[ir].day       = FIX( field[2] )
        data[ir].doy       = FIX( field[3] )
        data[ir].khunjerab = FLOAT( field[4] )
        data[ir].naltar    = FLOAT( field[5] )
        data[ir].ziarat    = FLOAT( field[6] )
        data[ir].gilgit    = FLOAT( field[7] )
        data[ir].islamabad = FLOAT( field[8] )

        ir = ir + 1
        
     ENDIF

  ENDWHILE

  CLOSE, U
  FREE_LUN, U

  nrec = ir

  data = data[0:nrec-1]

  RETURN, data

END


year = 2001
wapda_diri = '/projects/CHARIS/streamflow/Pakistan/' + $
             'Data_from_DHashmi/from_danial/Data_for_Andrew_Barnett/climate/'
khun = READ_WAPDA_CLIMATE( wapda_diri + 'khunjerab/khunjerab_'+STRTRIM(year,2)+'.txt' )
nalt = READ_WAPDA_CLIMATE( wapda_diri + 'naltar/naltar_'+STRTRIM(year,2)+'.txt' )
ziar = READ_WAPDA_CLIMATE( wapda_diri + 'ziarat/ziarat_'+STRTRIM(year,2)+'.txt' )

dscale = READ_DOWNSCALED_STATION( 'downscaled_t2m_hunza_validation.txt' )

khun_diff = ( dscale.khunjerab - 273.15 ) - khun.tavg
nalt_diff = ( dscale.naltar - 273.15 ) - nalt.tavg
ziar_diff = ( dscale.ziarat - 273.15 ) - ziar.tavg

PRINT, 'Khunjerab RMSE: ' + STRTRIM( SQRT( MEAN( khun_diff^2 ) ), 2 )
PRINT, 'Naltar RMSE:    ' + STRTRIM( SQRT( MEAN( nalt_diff^2 ) ), 2 )
PRINT, 'Ziarat RMSE:    ' + STRTRIM( SQRT( MEAN( ziar_diff^2 ) ), 2 )

!P.MULTI = [0,1,3]

ymin = FLOOR( MIN( [khun.tavg,dscale.khunjerab-273.15] )/5. ) * 5.
ymax = CEIL( MAX( [khun.tavg,dscale.khunjerab-273.15] )/5. ) * 5.
PLOT, khun.doy, khun.tavg, XRANGE=[0,367], XSTYLE=1, $
      YRANGE=[ymin,ymax], YSTYLE=1, THICK=2, TITLE='Khunjerab (4730 m)'
OPLOT, dscale.doy, dscale.khunjerab-273.15, COLOR=FSC_COLOR('red')
PLOTS, !X.CRANGE, 0.

ymin = FLOOR( MIN( [nalt.tavg,dscale.naltar-273.15] )/5. ) * 5.
ymax = CEIL( MAX( [nalt.tavg,dscale.naltar-273.15] )/5. ) * 5.
PLOT, nalt.doy, nalt.tavg, XRANGE=[0,367], XSTYLE=1, $
      YRANGE=[ymin,ymax], YSTYLE=1, THICK=2, TITLE='Naltar (2810 m)'
OPLOT, dscale.doy, dscale.naltar-273.15, COLOR=FSC_COLOR('red')
PLOTS, !X.CRANGE, 0.

ymin = FLOOR( MIN( [ziar.tavg,dscale.ziarat-273.15] )/5. ) * 5.
ymax = CEIL( MAX( [ziar.tavg,dscale.ziarat-273.15] )/5. ) * 5.
PLOT, ziar.doy, ziar.tavg, XRANGE=[0,367], XSTYLE=1, $
      YRANGE=[ymin,ymax], YSTYLE=1, THICK=2, TITLE='Ziarat (3669 m)'
OPLOT, dscale.doy, dscale.ziarat-273.15, COLOR=FSC_COLOR('red')
PLOTS, !X.CRANGE, 0.

END

