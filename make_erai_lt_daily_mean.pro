;;----------------------------------------------------------------------
;; Calculates long-term daily mean temperature fields from Aphrodite AphroTemp
;; data
;;
;; 2014-11-14 A.P.Barrett <apbarret@nsidc.org>
;;----------------------------------------------------------------------

VERBOSE = 1

diri = '/raid/ERA_Interim/t2m'
ffmt = '("era_interim.t2m.day_mean.charis.",i4,".nc")'

nday = 365  ;; Number of days in a year (ignores 366)

ybeg = 1979
yend = 2007
nyear = yend - ybeg + 1

window = 31

;; Define array to hold results
di = 103
dj = 65
result = MAKE_ARRAY( di, dj, nday+1, /FLOAT )

;; Define calculation array - this is the same for each loop of id and gets
;;                            set to zero each time
ntim = nyear * window
carray = MAKE_ARRAY( di, dj, ntim, /FLOAT )

;;----------------------------------------------------------------------
;; Loop through each day in the year
;;----------------------------------------------------------------------
FOR id = 0, nday-1 DO BEGIN

   PRINT, '% Getting mean for day ' + STRTRIM(id+1,2)

   carray[ *, *, * ] = 0.0

   FOR iy = ybeg, yend DO BEGIN
      
      fili = diri + '/' + STRTRIM(iy,2) + '/' + STRING( iy, FORMAT=ffmt )
      CASE 1 OF
         FILE_TEST( fili ) EQ 1: BEGIN
            isgz = 0
         END
         FILE_TEST( fili+'.gz' ) EQ 1: BEGIN
            isgz = 1
            IF ( VERBOSE EQ 1 ) THEN PRINT, '% Unzipping ' + fili+'.gz'
            SPAWN, 'gunzip ' + fili+'.gz'
         END
         ELSE: BEGIN
            PRINT, '% File not found'
            STOP
         END
      ENDCASE
      
      ;; Get the data
      data = NCDF_READV( fili, VARNAME='t2m' )
      ;; Subset it
      tmp = data
      tmp = TEMPORARY( (SHIFT( tmp, 0, 0, 15-id ))[*,*,0:30] )

      t0 = window*(iy-ybeg)
      t1 = t0 + 30
      carray[ *, *, t0:t1 ] = tmp

      IF ( isgz EQ 1 ) THEN BEGIN
         IF ( VERBOSE EQ 1 ) THEN PRINT, '% Zipping ' + fili
         SPAWN, 'gzip ' + fili
      ENDIF

   ENDFOR

   result[ *, *, id ] = MEAN( carray, DIM=3 )

ENDFOR

;; Set day 366 to same as 365
result[*,*,365] = result[*,*,364]

;LOADCT, 39
;IMSIZE, result[*,*,0], x0, y0, xs, ys
;TVSCL, CONGRID( result[*,*,0], xs, ys ), x0, y0

;; Get lat and lon
fili = diri + '/' + STRTRIM(1979,2) + '/' + STRING( 1979, FORMAT=ffmt )
lat = NCDF_READV( fili, VARNAME='lat' )
lon = NCDF_READV( fili, VARNAME='lon' )

;; Write to netcdf
filo = 'erai_mean_daily_tair_1979to2007.nc'

;; -  define global attributes in structure
global_att = {source: 'European Center for Medium-Range Weather Forecasts (RSMC)', $
              model:  'ERA-Interim' }
WRITE_LT_DAILY_MEAN_TO_NETCDF, result, lat, lon, filo, GLOBAL=global_att

END
