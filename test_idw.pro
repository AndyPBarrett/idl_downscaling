FUNCTION SELECT_WITHOUT_REPLACEMENT, n, m

  randomNumbers = RANDOMU( seed, m )
  sortedRandomNumbers = SORT( randomNumbers )
  indices = sortedRandomNumbers[ 0:n-1 ]

  RETURN, indices

END

FUNCTION IDW, x0, y0, xi, yi, ui, p

  d = SQRT( (x0-xi)^2 + (y0-yi)^2 )

  ispoint = WHERE( d EQ 0, numpoint )
  
  IF ( numpoint GT 0 ) THEN BEGIN
     u0 = ui[ispoint[0]]
  ENDIF ELSE BEGIN
     wi = 1 / (d^p)
     u0 = TOTAL( wi * ui ) / TOTAL( wi )
  ENDELSE

  RETURN, u0

END

nx = 50
ny = 50
ni = 10
p = 1

u = MAKE_ARRAY( nx, ny, /FLOAT )

idx = SELECT_WITHOUT_REPLACEMENT( ni, N_ELEMENTS(u) )
ij = IDX2IJK( idx, [nx,ny] )
uh = DIST(nx)
ui = uh[idx]

FOR ix=0, nx-1 DO BEGIN
   FOR jy=0, ny-1 DO BEGIN
      u[ ix, jy ] = IDW( ix, jy, ij[0,*], ij[1,*], ui, p )
   ENDFOR
ENDFOR

ERASE
IMSIZE, u, x0, y0, xs, ys
TVSCL, CONGRID( u, xs, ys ), x0, y0

PLOT, ij[0,*]+0.5, ij[1,*]+0.5, $
      XRANGE=[0,nx], XSTYLE=1, XTITLE='X', $
      YRANGE=[0,ny], YSTYLE=1, YTITLE='Y', $
      TITLE='P='+STRTRIM(p,2), PSYM=2, THICK=3, $
      POSITION=[x0,y0,x0+xs,y0+ys], /NOERASE, /DEVICE

END
