pro test_idl, other_args
  compile_opt strictarr

  args = command_line_args(count=nargs)

  help, nargs
  if (nargs gt 0L) then print, args

  help, other_args
  if (n_elements(other_args) gt 0L) then print, other_args
end
