;;----------------------------------------------------------------------
;; Makes a plot of years with ice-free (< 1e6 km^2) Oceans.
;;
;; 2014-12-02 A.P.Barrett
;;----------------------------------------------------------------------0

A = FINDGEN(31) * (!PI*2/31.)
USERSYM, COS(A), SIN(A), /FILL

;; Get data
data = READ_CMIP5_TABLE('cmip5_extent_historical_and_rcp85_model_ensemble_means.csv')

;; Sort the model into alphabetic order regardless of case
sidx = SORT( STRUPCASE( data.model ) )
model    = data.model[sidx]
ensemble = data.ensemble[sidx]
sic      = data.table[sidx,*]
;FOR ie=0, data.nens-1 DO PRINT, model[ie], ensemble[ie], FORMAT='(a20,1x,a8)'
;STOP 

;; Discard CSIRO-Mk3-6-0
thismodel = WHERE( model NE 'CSIRO-Mk3-6-0' AND $
                   model NE 'Mean-all' AND $
                   model NE 'Mean-select', nummodel )
IF ( nummodel GT 0 ) THEN BEGIN
   model    = model[ thismodel ]
   ensemble = ensemble[ thismodel ]
   sic      = sic[ thismodel, * ]
   nens = nummodel
ENDIF

;; Extract just September data
nyear = data.ntime / 12
yidx = (LINDGEN(nyear)*12)+8
year = data.year(yidx)
sic = sic[*, yidx]

;; Get just 2000 to 2010
year = FIX( year / 100. )
thisyear = WHERE( year GT 2000, numyear )

;; Subset by year
year = year[ thisyear ]
sic  = sic[*,thisyear]

;; Make plot skeleton
PLOT, RANDOMU(seed,10), RANDOMU(seed,10), $
      XRANGE=[2000,2101], XSTYLE=5, XTITLE='Year', $
      YRANGE=[-1,nens+0.5], YSTYLE=5, /NODATA, $
      XMARGIN=[20,4], FONT=0
AXIS, XAXIS=0, XTICKS=6, XMINOR=0, XRANGE=[2000,2101], XSTYLE=1, $
      XTICKV=[2000,2020,2040,2060,2080,2100], FONT=0

;; Add boxes to delineate model ensembles
FOR im=0, nens-1 DO XYOUTS, 1995, !Y.CRANGE[1]-im-0.75, model[im], $
                             /DATA, CHARSIZE=0.6, FONT=0, ALIGNMENT=1

;; Add some blue boxes to aid interpretation
width = 3
FOR im=0, numod-1, width DO BEGIN
   y0 = MAX( [(!Y.CRANGE[1] - im - width), 0.5] )
   y1 = !Y.CRANGE[1] - im
   IF ( ( im MOD 2 ) EQ 0 ) THEN BEGIN

         x = !X.CRANGE[[0,0,1,1,0]]
         y = [y0,y1,y1,y0,y0]
         POLYFILL, x, y, COLOR=FSC_COLOR('Cyan')
      ENDIF
ENDFOR

FOR ie=0, nens-1 DO BEGIN

   tmp = sic[ie,*]

   isfree = WHERE( tmp LT 1.0, numfree )

   IF ( numfree GT 0 ) THEN PLOTS, year[ isfree ], nens-ie, $
                                   PSYM=8, SYMSIZE=0.5

ENDFOR

END


