
;;----------------------------------------------------------------------
;; Finds z(x,y) using bilinear interpolation.
;;
;; x - x-coord to be estimated
;; y - y-coord to be estimated
;; z - 4-element array [ z(x1,y1), z(x2,y1), z(x1,y2), z(x2,y2) ]
;; x1,x2 - x-coords
;; y1,y2 - y-coords
;;----------------------------------------------------------------------
FUNCTION BILINT, x, y, z, x1, x2, y1, y2, WEIGHT=WEIGHT

  w11 = (x2 - x) * (y2 - y) / ( (x2-x1) * (y2-y1) )
  w21 = (x - x1) * (y2 - y) / ( (x2-x1) * (y2-y1) )
  w12 = (x2 - x) * (y - y1) / ( (x2-x1) * (y2-y1) )
  w22 = (x - x1) * (y - y1) / ( (x2-x1) * (y2-y1) )

  w = [ w11, w21, w12, w22 ]
  znew = TRANSPOSE(z) # w       ; dot product of z and weight vectors

  weight = w

  RETURN, znew

END

rearth = 6.37e6 ; radius of Earth
g = 9.80665

x = 0.7
y = -88.77

x1 = 0.0 ;0.
x2 = 0.75 ;1.
y1 = 89.25 ;0.
y2 = 90.0 ;1.
;zin = [ 0., 1., 1., 0.5 ]
zin = [ 829.15,    827.51,    805.33,    803.69 ]

n = 10
xo = ( FINDGEN( n+1 ) * (x2-x1)/FLOAT(n) ) + x1
yo = ( FINDGEN( n+1 ) * (y2-y1)/FLOAT(n) ) + y1

x2d = REBIN( xo, n+1, n+1, /SAMPLE )
y2d = REBIN( TRANSPOSE(yo), n+1, n+1, /SAMPLE )
z2d = MAKE_ARRAY( n+1, n+1, /FLOAT )

FOR ii=0, n DO BEGIN
   FOR jj=0, n DO BEGIN

      z2d[ii,jj] = BILINT( x2d[ii,jj], y2d[ii,jj], zin, x1, x2, y1, y2 )

   ENDFOR
ENDFOR

IMSIZE, z2d, x0, y0, xs, ys
TVSCL, CONGRID( z2d, xs, ys ), x0, y0

END
