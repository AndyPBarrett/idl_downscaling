;;----------------------------------------------------------------------
;; Calculates distance grids for a set of stations
;;
;; 2015-07-13 A.P.Barrett
;;----------------------------------------------------------------------

;; Function: Get station locations
FUNCTION GET_STATION_INFO, NSTATION=nstation

  station = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0, $
                              'LATITUDE', 36.1667, 'LONGITUDE', 74.1833 ), $
              CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0, $
                             'LATITUDE', 36.7980, 'LONGITUDE', 74.4820  ), $
              CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0, $
                             'LATITUDE', 36.8411, 'LONGITUDE', 75.4192 ) ]

  nstation = N_ELEMENTS( station )

  RETURN, station

END

FUNCTION MAKE_WEIGHT, r, Rmax, CRESSMAN=CRESSMAN, MEAN_REGION=MEAN_REGION

  CASE 1 OF
     ( KEYWORD_SET( CRESSMAN ) EQ 1 ): BEGIN
        ;; uses Cressman (1959) formulation
        w = ( Rmax^2 - r^2 ) / ( Rmax^2 + r^2 )
        ;; If outside maximum radius set to zero
        outside = WHERE( r GT Rmax, numoutside )
        IF ( numoutside GT 0 ) THEN w[ outside ] = 0.0
     END
     ( KEYWORD_SET( MEAN_REGION ) EQ 1 ): BEGIN
        w = EXP( (-1.) * ((r-150)^2) / (2 * Rmax^2) )
        ;; Set points inside 150 km to 1.
        inside = WHERE( r LE 150, num_inside )
        IF ( num_inside GT 0 ) THEN w[ inside ] = 1.
     END
     ELSE: BEGIN
        ;; Use exponential weight function
        w = EXP( (-1.) * (r^2) / (2 * Rmax^2) )
     END
  ENDCASE

  ;; IF cell is within max cell radius then give a weight of 1
  iscell = WHERE( r LT 0.353, numcell )
  IF ( numcell GT 0 ) THEN w[ iscell ] = 1.

  RETURN, w

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------
DO_PLOT=0
NO_WRITE = 0

VERBOSE = 1
tile = 'h23v05'

base_dir = 'bias_correction_grids'

kappa = 0.015
eps2  = 0.11

Rmax = 50.

IF ( NO_WRITE EQ 0 ) THEN BEGIN

   IF ( FILE_TEST( base_dir + '/' + tile, /DIRECTORY ) EQ 0 ) THEN BEGIN
      FILE_MKDIR, base_dir + '/' + tile
   ENDIF

ENDIF

;; Get station info
;;station = GET_STATION_INFO( NSTATION=nstat )

;; Get bias data and station info
diri = '/disks/arctic6_raid/ERA_Interim/daily'
fili = 'bias_erai_hunza_tair.txt'
READ_BIAS_CORRECTION, diri + '/' + fili, bias, station

;; Get tile geolocation
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting MODIS tile coordinates...'
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; calculate distances
d0 = HAVERSINE( station[0].longitude, station[0].latitude, mlon, mlat )
d1 = HAVERSINE( station[1].longitude, station[1].latitude, mlon, mlat )
d2 = HAVERSINE( station[2].longitude, station[2].latitude, mlon, mlat )

;w0 = EXP( (-1.) * (d0^2) / (2 * Rmax^2) )
;w1 = EXP( (-1.) * (d1^2) / (2 * Rmax^2) )
;w2 = EXP( (-1.) * (d2^2) / (2 * Rmax^2) )

w0 = MAKE_WEIGHT( d0, 100, /MEAN_REGION )
w1 = MAKE_WEIGHT( d1, 100, /MEAN_REGION )
w2 = MAKE_WEIGHT( d2, 100, /MEAN_REGION )

wbar = MAX( [ [[w0]], [[w1]], [[w2]] ], DIM=3 )

;IMSIZE, w0, x0, y0, xs, ys
;TVSCL, CONGRID( wbar, xs, ys ), x0, y0
;STOP

bias_avg = MEAN( bias, DIM=1 )

;correction = MAKE_ARRAY( nrow, ncol, 365, /FLOAT, VALUE=0. )

FOR id=0, 364 DO BEGIN

   PRINT, '... generating bias correction grid for day ' + STRING(id+1,FORMAT='(i03)')

   correction = bias_avg[id] * wbar

   IF ( NO_WRITE EQ 0 ) THEN BEGIN
      filo = base_dir + '/' + tile + '/' + $
             'erai_tsurf_bias_correction_'+tile+'_'+STRING(id+1,FORMAT='(i03)')+'.v0.bin'
      PRINT, '... writing grid to ' + filo
      
      OPENW, U, filo, /GET_LUN
      WRITEU, U, correction
      CLOSE, U
      FREE_LUN, U
   ENDIF

   ;numer = ( w0 * bias[0,id] ) + ( w1 * bias[1,id] ) + ( w2 * bias[2,id] )

   ;notzero = WHERE( denom GT 0, num_notzero )
   ;correction[notzero] = numer[notzero] / denom[notzero]
   ;correction = numer / denom

   ;; For plotting
   IF ( DO_PLOT EQ 1 ) THEN BEGIN

      h = HISTOGRAM( correction, MIN=-5., MAX=5., BINSIZE=0.1, REVERSE_INDICES=ri )
      nh = N_ELEMENTS(h)
      colors = BYTSCL( INDGEN(nh), TOP=240 ) + 15
      image = MAKE_ARRAY( nrow, ncol, /BYTE )
      FOR ih=0, nh-1 DO BEGIN
         IF ( ri[ih] NE ri[ih+1] ) THEN BEGIN
            image[ ri[ ri[ih]:ri[ih+1]-1 ] ] = colors[ih]
         ENDIF
      ENDFOR

      ERASE
      title = STRING(MIN(correction), MAX(correction), FORMAT='("Min=",f7.4," Max=",f7.4)' )
      TV, CONGRID( image, xs, ys ), x0, y0
      PLOT, RANDOMU(seed,10), RANDOMU(seed,10), $
            XRANGE=[0,2401], XSTYLE=1, YRANGE=[0,2401], YSTYLE=1, $
            TITLE=title, POSITION=[x0,y0,x0+xs,y0+ys], /DEVICE, /NOERASE
      STOP
      
   ENDIF

ENDFOR

END
