FUNCTION GET_TILE, type

  udiri = '/projects/CHARIS'
  xdim = 2400
  ydim = 2400

  tile = 'h25v06'

  CASE type OF
     'lat': BEGIN
        file = udiri + '/ancillary/modis_geolocation/' + tile + '.lat.2400x2400x1.float.dat'
     END
     'lon': BEGIN
        file = udiri + '/ancillary/modis_geolocation/' + tile + '.lon.2400x2400x1.float.dat'
     END
     'mask': BEGIN
        file = udiri + '/basins/basin_MODIS_tiles/' + tile + '/IN_UpperIndus_at_Mingora_'+tile+'.tif'
     END
     'elev': BEGIN
        file = udiri + 'elevation_data/' + 'sin_' + tile + '.tif'
     END
     ELSE: BEGIN
        PRINT, '% GET_2TILES: Unknown TYPE'
        RETURN, -1
     END
  ENDCASE

  IF ( type EQ 'lat' OR type EQ 'lon' ) THEN BEGIN
     grid = OPEN_IMAGE( file, xdim, ydim, 4 )
  ENDIF ELSE BEGIN
     grid = READ_TIFF( file )
  ENDELSE

  RETURN, grid

END
     
FUNCTION GET_TAIR_GRID, date, NOINVERSION=NOINVERSION, VERBOSE=VERBOSE

  diri = RAID_PATH('arctic6')+'/ERA_Interim/daily'
  xdim = 2400
  ydim = 2400

  tile = 'h25v06'

  IF ( KEYWORD_SET( noinversion ) ) THEN BEGIN
     file = diri + '/tsurf_'+tile+'_'+date+'_noinversion.bin'
  ENDIF ELSE BEGIN
     file = diri + '/tsurf_'+tile+'_day_'+date+'.bin'
  ENDELSE

  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting data from ' + file
  grid = OPEN_IMAGE( file, xdim, ydim, 4 )

  RETURN, grid

END

FUNCTION DATE_SERIES, year

  jday = TIMEGEN( START=JULDAY(1,1,year), FINAL=JULDAY(12,31,year), UNITS='Day' )
  result = STRING( jday, FORMAT='(C(CYI04,CMOI02,CDI02))' )

  RETURN, result

END

PRO PLOT_SERIES, temp, SITE_NAME=site_name

  gilgit_hi = [  9.3, 12.0, 17.9, 23.9, 28.4, 34.3, 36.1, 35.3, 31.6, 25.3, 17.8, 11.0 ]
  gilgit_lo = [ -2.7,  2.5,  5.4,  9.4, 11.7, 15.2, 18.8, 18.1, 12.7,  6.6,  0.6, -2.4 ]
  gilgit_av = ( gilgit_hi + gilgit_lo ) * 0.5

  islam_hi = [ 17.1, 19.1, 23.9, 30.1, 35.3, 38.7, 35.0, 33.4, 33.5, 30.9, 25.4, 19.7 ]
  islam_lo = [  2.6,  5.1,  9.9, 15.0, 19.7, 23.7, 24.3, 23.5, 20.6, 13.9,  7.5,  3.4 ]
  islam_av = ( islam_hi + islam_lo ) * 0.5

  !P.MULTI=[0,1,5]

  mtime = JULDAY( INDGEN(12)+1,15.,2008)-JULDAY(1,1,2008)+1
  time = INDGEN(365)+1
  ymin = FLOOR( MIN(temp[0,*]-273.15) / 5. ) * 5.
  ymax = CEIL( MAX(temp[0,*]-273.15) / 5. ) * 5.
  PLOT, time, temp[0,*]-273.15, $
        XRANGE=[0,366], XSTYLE=1, $
        YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='!Uo!NC', $
        TITLE=site_name[0]
  PLOTS, !X.CRANGE, 0

  ymin = FLOOR( MIN(temp[1,*]-273.15 ) / 5. ) * 5. 
  ymax = CEIL( MAX(temp[1,*]-273.15 ) / 5. ) * 5.
  PLOT, time, temp[1,*]-273.15, $
        XRANGE=[0,366], XSTYLE=1, $
        YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='!Uo!NC', $
        TITLE=site_name[1]
  PLOTS, !X.CRANGE, 0
  
  ymin = FLOOR( MIN(temp[2,*]-273.15) / 5. ) * 5.
  ymax = CEIL( MAX(temp[2,*]-273.15) / 5. ) * 5.
  PLOT, time, temp[2,*]-273.15, $
        XRANGE=[0,366], XSTYLE=1, $
        YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='!Uo!NC', $
        TITLE=site_name[2]
  PLOTS, !X.CRANGE, 0

  mtime = JULDAY( INDGEN(12)+1,15.,2008)-JULDAY(1,1,2008)+1
  time = INDGEN(365)+1
  ymin = FLOOR( MIN( [ MIN(temp[3,*]-273.15), MIN(gilgit_lo ) ] ) / 5. ) * 5.
  ymax = CEIL( MAX( [ MAX(temp[3,*]-273.15), MAX(gilgit_hi ) ] ) / 5. ) * 5.
  PLOT, time, temp[3,*]-273.15, $
        XRANGE=[0,366], XSTYLE=1, $
        YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='!Uo!NC', $
        TITLE=site_name[3]
  PLOTS, !X.CRANGE, 0
  OPLOT, mtime, gilgit_hi, COLOR=FSC_COLOR('red')
  OPLOT, mtime, gilgit_lo, COLOR=FSC_COLOR('blue')
  OPLOT, mtime, gilgit_av, COLOR=FSC_COLOR('green')

  ymin = FLOOR( MIN( [ MIN(temp[4,*]-273.15), MIN(islam_lo ) ] ) / 5. ) * 5.
  ymax = CEIL( MAX( [ MAX(temp[4,*]-273.15), MAX(islam_hi ) ] ) / 5. ) * 5.
  PLOT, time, temp[4,*]-273.15, $
        XRANGE=[0,366], XSTYLE=1, $
        YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='!Uo!NC', $
        TITLE=site_name[4]
  PLOTS, !X.CRANGE, 0
  OPLOT, mtime, islam_hi, COLOR=FSC_COLOR('red')
  OPLOT, mtime, islam_lo, COLOR=FSC_COLOR('blue')
  OPLOT, mtime, islam_av, COLOR=FSC_COLOR('green')

  !P.MULTI=0

END

PRO WRITE_TO_FILE, date, temp, filo, station

  nstation = N_ELEMENTS( station )

  dims = SIZE( temp, /DIMENSIONS )
  ntime = dims[1]

  ;; Parse date
  day = LONG(date) MOD 100
  rem = LONG( LONG(date) / 100 )
  mon = rem MOD 100
  year = LONG( rem / 100 )

  OPENW, U, filo, /GET_LUN

  FOR is = 0, nstation-1 DO PRINTF, U, '# ' + station[is]

  FOR it=0, ntime-1 DO BEGIN
     PRINTF, U, year[it], mon[it], day[it], it+1, temp[*,it], $
            FORMAT='(i4,2(1x,i02),1x,i3,'+STRTRIM(nstation,2)+'(1x,f5.1))'
  ENDFOR

  CLOSE, U
  FREE_LUN, U

  RETURN

END



;;----------------------------------------------------------------------
;; MAIN ROUTINE
;;----------------------------------------------------------------------
PRO GET_NEPAL_SITES_TAIR_DSCALE, year, NOINVERSION=NOINVERSION, VERBOSE=VERBOSE

;;----------------------------------------------------------------------
;; Get geolocation, elevation and mask grids
;;----------------------------------------------------------------------

;; Get geolocation tiles and stitch together
lat = GET_TILE( 'lat' )
lon = GET_TILE( 'lon' )

;;----------------------------------------------------------------------
;; Find indices for selected locations
;;----------------------------------------------------------------------
;; locations are Gilgit, Khunjerab Pass and Islamabad (in that order)
diri = '/projects/CHARIS/surface_met_data/Nepal/Nepal_Met_Data'
fili = 'ku_dhm_met_station_list.txt'
station = READ_DHM_STATION_LIST( diri + '/' + fili )
n=N_ELEMENTS(station)

grid_id = MAKE_ARRAY( n, /LONG )
FOR im=0, n-1 DO BEGIN
   idx = WHERE( lat GE FLOOR(station[im].lat) AND lat LE CEIL(station[im].lat) AND $
                lon GE FLOOR(station[im].lon) AND lon LE CEIL(station[im].lon), num )
   dist = HAVERSINE( station[im].lon, station[im].lat, lon[idx], lat[idx] )
   nearest = idx[ SORT( dist ) ]
   grid_id[im] = nearest[0]
   PRINT, station[im].name, station[im].lat, station[im].lon, grid_id[im], FORMAT='(a30,2(1x,f9.4),1x,i10)'
ENDFOR

;STOP

;;----------------------------------------------------------------------
;; Loop through dates to get temperature series for grid points
;;----------------------------------------------------------------------
date = DATE_SERIES(year)
ndate = N_ELEMENTS( date )

temp = MAKE_ARRAY( n, ndate, /FLOAT )
FOR id = 0, ndate-1 DO BEGIN

   tair = GET_TAIR_GRID( date[id], NOINVERSION=noinversion, VERBOSE=VERBOSE )
   temp[*,id] = tair[ grid_id ]

ENDFOR

;;----------------------------------------------------------------------
;; Write results to file
;;----------------------------------------------------------------------
IF ( KEYWORD_SET(noinversion) EQ 0 ) THEN BEGIN
   filo = 'downscaled_tair_nepal_validation_'+STRTRIM(year,2)+'.txt'
ENDIF ELSE BEGIN
   filo = 'downscaled_tair_nepal_validation_noinversion_'+STRTRIM(year,2)+'.txt'
ENDELSE   
;PRINT, filo
WRITE_TO_FILE, date, temp, filo, STRTRIM(station.name,2)

;;----------------------------------------------------------------------
;; Plot results
;;----------------------------------------------------------------------

PLOT_SERIES, temp, SITE_NAME=station.name

END

