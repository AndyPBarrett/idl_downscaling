
PRO PARSE_DATE, date, month, day

  monstr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', $
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]

  field = STRSPLIT( date, '-', /EXTRACT )

  ;; Convert day to Int
  day = FIX( field[0] )
  
  ;; Get month number
  idx = WHERE( field[1] EQ monstr, count )
  IF ( count LE 0 ) THEN BEGIN
     PRINT, '% PARSE_DATE: Error: Month name ' + field[1] + ' is not recognized'
     STOP
  ENDIF
  month = idx + 1
  
  RETURN

END


fili = 'naltar_05.csv'
year = 2005

MAXREC = 400
struct = { year: 0, $
           month: 0, $
           day: 0, $
           doy: 0, $
           tmax: -9999.99, $
           tmin: -9999.99, $
           rhmin: -9999.99, $
           rhmax: -9999.99, $
           precip: -9999.99, $
           rad: -9999.99, $
           tavg: -9999.99, $
           pdd: -9999.99 }
result = REPLICATE( struct, MAXREC )

OPENR, U, fili, /GET_LUN

line = ''
k = 0
WHILE ( NOT EOF(U) ) DO BEGIN

   READF, U, line

   IF ( STREGEX( line, '[0-9]+-[a-zA-Z]{3}', /BOOLEAN ) EQ 1 ) THEN BEGIN

      field = STRSPLIT( line, ',', /EXTRACT )

      PARSE_DATE, field[0], month, day

      ;; Calculate day of year
      doy = JULDAY( month, day, year ) - JULDAY( 1, 1, year ) + 1

      result[k].year   = year
      result[k].month  = month
      result[k].day    = day
      result[k].doy    = doy
      result[k].tmax   = FLOAT( field[1] )
      result[k].tmin   = FLOAT( field[2] )
      result[k].rhmin  = FLOAT( field[3] )
      result[k].rhmax  = FLOAT( field[4] )
      result[k].precip = FLOAT( field[5] )
      result[k].rad    = FLOAT( field[6] )
      result[k].tavg   = FLOAT( field[7] )
      result[k].pdd    = FLOAT( field[8] )
     
      k = k + 1

   ENDIF

ENDWHILE

CLOSE, U
FREE_LUN, U

nrec = k
result = result[ 0:nrec-1 ]

fmt = '(i4,2(1x,i2),1x,i3,8(1x,f8.2))'
FOR ir=0, nrec-1 DO PRINT, result[ir], FORMAT=fmt

END

