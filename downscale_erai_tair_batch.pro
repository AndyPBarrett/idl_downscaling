
ybeg = 2011
yend = 2014

tile = [ 'h24v05', 'h23v05', 'h25v06' ]
;tile = [ 'h23v05', 'h25v06' ]
nTiles = N_ELEMENTS( tile )

tstart = SYSTIME(/SECONDS)

rdiro = '/projects/CHARIS/forcing_data/Downscaled/ERA-Interim/day/uncorrected/v0/'

FOR it=0, nTiles-1 DO BEGIN

   FOR iy=ybeg, yend DO BEGIN

      diro = rdiro + tile[it] + '/' + STRTRIM( iy, 2 )
      IF ( FILE_TEST( diro, /DIRECTORY ) EQ 0 ) THEN BEGIN
         FILE_MKDIR, diro
      ENDIF

      FOR im=1,12 DO BEGIN
         date = STRING( iy, im, FORMAT='(i4,i02)' )
         downscale_erai_tair, tile[it], date, NO_INVERSION=0, DIRO=diro
      ENDFOR

      tint = SYSTIME(/SECONDS) - tstart
      PRINT, tint/60., FORMAT='("Time Elapsed for one year: ",f10.2)'

   ENDFOR

   tint = SYSTIME(/SECONDS) - tstart
   PRINT, tint/60., FORMAT='("Time Elapsed for one tile: ",f10.2)'

ENDFOR

END
