;;----------------------------------------------------------------------
;; Calculates and plots a map of temperature lapse rate adjustments
;;
;; 2014-05-13 A.P.Barrett
;;----------------------------------------------------------------------

year = 2001
month = 7
lapse = 7.
nrow = 2400
ncol = 2400

;; Convert lapse rate to deg C/m
gamma = (-1.) * lapse / 1000.

;; Read cell address for tile
cell_address = GET_BASIN_DATA_APB( 'cell_address', 'hunza' )

;; Get MODIS DEM
zmod = GET_BASIN_DATA_APB( 'elev', 'hunza' )
ismissing = WHERE( zmod LT -1000., num_missing )
IF ( num_missing GT 0 ) THEN zmod[ ismissing ] = !VALUES.F_NAN

;; Get ERA-I elevations
geo_diri = '/raid/ERA_Interim/t2m/'
geo_fili = 'era_interim.zs.hkh.invariant.nc'
zera = NCDF_READV( geo_diri + '/' + geo_fili, VARNAME='zs' )
;; - convert geopotential to geopotential height
g = 9.81
zera = zera / g

;; Assign ERA-Interim geopotential height to MODIS grid
zera_grid = zera[ cell_address ]
dz = zmod - zera_grid
tadj = dz * gamma

;; Get ERA-Interim data
diri = '/raid/ERA_Interim/t2m/' + STRING(year,FORMAT='(i4)')
fili = 'era_interim.t2m.hkh.'+STRING(year,month,FORMAT='(i4,i02)')+'.nc'
PRINT, '    getting data from ' + fili + '...'
t2m = NCDF_READV( diri + '/' + fili, VARNAME='t2m' )
edims = SIZE( t2m, /DIMENSIONS )
nlat = edims[0]
nlon = edims[1]
ntim = edims[2]

IMSIZE, zera_grid, x0, y0, xs, ys

;TV, CONGRID( BYTSCL( zera_grid, /NAN ), xs, ys ), x0, y0
;STOP

;TV, CONGRID( BYTSCL( zmod, /NAN ), xs, ys ), x0, y0
;STOP

;TV, CONGRID( BYTSCL( dz, /NAN ), xs, ys ), x0, y0
;STOP

;TV, CONGRID( BYTSCL( tadj, /NAN ), xs, ys ), x0, y0
;STOP

result = MAKE_ARRAY( 2*ncol, nrow, ntim, /FLOAT, VALUE=!VALUES.F_NAN )

;; Calculate temperature
FOR it = 0, ntim-1 DO BEGIN

   PRINT, 'PROCESSING ' + STRTRIM(it,2)

   tmp = t2m[*,*,it]
   mtmp = tmp[ cell_address ]

   result[*,*,it] = mtmp + tadj

ENDFOR

;; calculate daily means
nday = ntim / 4
t2m_day = MAKE_ARRAY( 2*ncol, nrow, nday, /FLOAT, VALUE=!VALUES.F_NAN )
id = 0 
FOR it=0, ntim-1, 4 DO BEGIN
   
   t2m_day[ *, *, id ] = MEAN( result[*, *, it:it+3 ], DIM=3, /NAN )

   id++

ENDFOR

mint = FLOOR( MIN( t2m_day, /NAN )/10. ) * 10.
maxt = CEIL( MAX( t2m_day, /NAN )/10. ) * 10.

FOR id = 0, nday-1 DO BEGIN

   PRINT, id
   h = HISTOGRAM( t2m_day[*,*,id], MIN=mint, MAX=maxt, BINSIZE=5., REVERSE_INDICES=ri, LOCATION=location )
   nh = N_ELEMENTS(h)
   color = BYTSCL( INDGEN(nh), TOP=240 ) + 16
   
   image = t2m_day[*,*,id]

   FOR ih=0, nh-1 DO BEGIN
      IF ( ri[ih] NE ri[ih+1] ) THEN BEGIN
         idx = ri[ ri[ih]:ri[ih+1]-1 ]
         image[ idx ] = color[ih ]
      ENDIF
   ENDFOR

   TV, CONGRID( image, xs, ys ), x0, y0
   STOP

ENDFOR

END
