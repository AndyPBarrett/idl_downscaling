;;----------------------------------------------------------------------
;; Makes a plot of years with ice-free (< 1e6 km^2) Oceans.
;;
;; 2014-12-02 A.P.Barrett
;;----------------------------------------------------------------------0

A = FINDGEN(31) * (!PI*2/31.)
USERSYM, COS(A), SIN(A), /FILL

;; Get data
data = READ_CMIP5_TABLE('cmip5_extent_historical_and_rcp85_sept.csv')

;; Sort the model into alphabetic order regardless of case
sidx = SORT( STRUPCASE( data.model ) )
model    = data.model[sidx]
ensemble = data.ensemble[sidx]
sic      = data.table[sidx,*]
;FOR ie=0, data.nens-1 DO PRINT, model[ie], ensemble[ie], FORMAT='(a20,1x,a8)'
;STOP 

;; Discard CSIRO-Mk3-6-0
thismodel = WHERE( model NE 'CSIRO-Mk3-6-0', nummodel )
IF ( nummodel GT 0 ) THEN BEGIN
   model    = model[ thismodel ]
   ensemble = ensemble[ thismodel ]
   sic      = sic[ thismodel, * ]
   nens = nummodel
ENDIF

;; Get just 2000 to 2010
year = FIX( data.year / 100. )
thisyear = WHERE( year GT 2000, numyear )

;; Subset by year
year = year[ thisyear ]
sic  = sic[*,thisyear]

;; Make plot skeleton
PLOT, RANDOMU(seed,10), RANDOMU(seed,10), $
      XRANGE=[2000,2101], XSTYLE=5, XTITLE='Year', $
      YRANGE=[-2,nens+0.5], YSTYLE=5, /NODATA, $
      XMARGIN=[20,4], FONT=0
AXIS, XAXIS=0, XTICKS=6, XMINOR=0, XRANGE=[2000,2101], XSTYLE=1, $
      XTICKV=[2000,2020,2040,2060,2080,2100], FONT=0

;; Add boxes to delineate model ensembles
umodel = model[ UNIQ( model, SORT( STRUPCASE(model) ) ) ]
numod = N_ELEMENTS( umodel )
;FOR im=0, numod-1 DO BEGIN
;   idx = WHERE( model EQ umodel[im], num )
;   y0 = !Y.CRANGE[1] - idx[num-1] - 1
;   y1 = !Y.CRANGE[1] - idx[0]
;   yt = MEAN( [y0,y1] )
;   XYOUTS, 1995, yt-0.25, umodel[im], /DATA, CHARSIZE=0.6, FONT=0, ALIGNMENT=1
    ;print, umodel[im], num, idx[0], idx[num-1], !Y.CRANGE[1] - idx[num-1] - 1, !Y.CRANGE[1] - idx[0], $
;          FORMAT='(a20,1x,i2,4(1x,f4.1))'
;   IF ( ( im MOD 2 ) EQ 0 ) THEN BEGIN
;      IF ( num GT 0 ) THEN BEGIN
;         x = !X.CRANGE[[0,0,1,1,0]]
;         y = [y0,y1,y1,y0,y0]
;         POLYFILL, x, y, COLOR=FSC_COLOR('Cyan')
;      ENDIF

;   ENDIF
;ENDFOR

FOR ie=0, nens-1 DO BEGIN

   tmp = sic[ie,*]

   isfree = WHERE( tmp LT 1.0, numfree )

   IF ( numfree GT 0 ) THEN PLOTS, year[ isfree ], nens-ie, $
                                   PSYM=8, SYMSIZE=0.5

ENDFOR

END


