
year = 2000

fili = 'hunza_era_interim_t2m_profile_100m.'+STRTRIM(year,2)+'.dscale.asc'

status = READ_HYPSOMETRY( fili, DATA=data, ELEVATION=elev )

FOR i=0, 62 DO BEGIN

   PLOT, data.doy, data.area[i], $
         XRANGE=[0,N_ELEMENTS(data)], XSTYLE=1, XTITLE='Day', $
         YRANGE=[220,310], YSTYLE=1, YTITLE='Air temperature (K)', $
         TITLE=STRTRIM(elev[i],2)+ ' m'
   PLOTS, !X.CRANGE, 273.

   STOP

ENDFOR

END
