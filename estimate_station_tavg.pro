
year = 2001
wapda_diri = '/projects/CHARIS/streamflow/Pakistan/' + $
             'Data_from_DHashmi/from_danial/Data_for_Andrew_Barnett/climate/'
khun = READ_WAPDA_CLIMATE( wapda_diri + 'khunjerab/khunjerab_'+STRTRIM(year,2)+'.txt' )
nalt = READ_WAPDA_CLIMATE( wapda_diri + 'naltar/naltar_'+STRTRIM(year,2)+'.txt' )
ziar = READ_WAPDA_CLIMATE( wapda_diri + 'ziarat/ziarat_'+STRTRIM(year,2)+'.txt' )
nday = N_ELEMENTS( khun )

kelev = 4730.0
zelev = 3669.0
nelev = 2810.0

;; Estimate Khunjerab temperature from Naltar and Ziarat
tlapse = ( ziar.tavg - nalt.tavg ) / ( zelev - nelev )
khun_est0 = nalt.tavg + ( tlapse * (kelev - nelev ) )
khun_est1 = nalt.tavg + ( (-0.0065) * (kelev - nelev ) )

nalt_est1 = ziar.tavg + ( (-0.0065) * (nelev - zelev) )

!P.MULTI=[0,1,2]
 
ymin = FLOOR( MIN( [khun.tavg,khun_est1] )/5. ) * 5.
ymax = CEIL( MAX( [khun.tavg,khun_est1] )/5. ) * 5.
PLOT, khun.tavg, khun_est1, $
      XRANGE=[ymin,ymax], XSTYLE=1, XTITLE='Observed', $
      YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Estimated', $
      TITLE='Khunjerab', PSYM=2, /ISOTROPIC
PLOTS, !X.CRANGE, !Y.CRANGE

ymin = FLOOR( MIN( [nalt.tavg,nalt_est1] )/5. ) * 5.
ymax = CEIL( MAX( [nalt.tavg,nalt_est1] )/5. ) * 5.
PLOT, nalt.tavg, nalt_est1, $
      XRANGE=[ymin,ymax], XSTYLE=1, XTITLE='Observed', $
      YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Estimated', $
      TITLE='Naltar', PSYM=2, /ISOTROPIC
PLOTS, !X.CRANGE, !Y.CRANGE

END
