
FUNCTION INTERP_TAIR, z, ta, LEVEL=LEVEL

  level = N_ELEMENTS( level ) GT 0 ? level : 1000.

  dims = SIZE( z, /DIMENSIONS )
  
  nlon = dims[0]
  nlat = dims[1]
  nlev = dims[2]
  ntim = dims[3]

  result = MAKE_ARRAY( nlon, nlat, ntim, /FLOAT, VALUE=-9999.99 ) 

  FOR it = 0, ntim-1 DO BEGIN
     
     FOR ilat = 0, nlat-1 DO BEGIN
        FOR ilon = 0, nlon-1 DO BEGIN
           
           ztmp = REFORM( z[ ilon, ilat, *, it ] )
           ttmp = REFORM( ta[ ilon, ilat, *, it ] )

           idx = VALUE_LOCATE( ztmp, level )
           dta = ttmp[ idx ] - ttmp[ idx+1 ]
           dz  = ztmp[ idx ] - ztmp[ idx+1 ]
           deltaz = level - ztmp[ idx+1 ]
           dtadz = dta / dz
           
           result[ ilon, ilat, it] = ttmp[ idx+1 ] + ( deltaz * dtadz )

        ENDFOR
     ENDFOR

  ENDFOR

  RETURN, result

END

PRO PLOT_XSECTION, z, ta, tlev, lat, lon

   ;; - find lat and lon indices
  ilat0 = VALUE_LOCATE( lat, 65. )
  ilat1 = VALUE_LOCATE( lat, 85. ) + 1
  ilon  = VALUE_LOCATE( lon, 45. )
  itim  = 7
  
  tslc = REFORM( ta[ ilon, ilat0:ilat1, *, itim ] )
  zslc = REFORM(  z[ ilon, ilat0:ilat1, *, itim ] )

  dims = SIZE( tslc, /DIMENSIONS )
  nx   = dims[0]
  ny   = dims[1]

  ll   = REBIN( lat[ ilat0:ilat1], nx, ny, /SAMPLE )
 
  HELP, tslc, zslc, ll

  zmin = FLOOR( MIN(tslc)/5. ) * 5.
  zmax = CEIL( MAX( tslc )/5. ) * 5.
  zrng = zmax - zmin
  nlev = 20
  zint = zrng/FLOAT(nlev)
  lev  = ( FINDGEN( nlev+1 ) * zint ) + zmin

  CONTOUR, tslc, ll, zslc, /FILL, /IRREGULAR, LEVEL=lev

  RETURN

END

PRO WRITE_TO_FILE, ta, lat, lon, time, filo

  dims = SIZE( ta, /DIMENSIONS )
  nlon = dims[0]
  nlat = dims[1]
  ntim = dims[2]

  cdfid = NCDF_CREATE( filo, /CLOBBER )

  latdimid = NCDF_DIMDEF( cdfid, 'lat', nlat )
  londimid = NCDF_DIMDEF( cdfid, 'lon', nlon )
  timdimid = NCDF_DIMDEF( cdfid, 'time', ntim )
  
  latid = NCDF_VARDEF( cdfid, 'lat', latdimid, /FLOAT )
  lonid = NCDF_VARDEF( cdfid, 'lon', londimid, /FLOAT )
  timid = NCDF_VARDEF( cdfid, 'time', timdimid, /FLOAT )
  varid = NCDF_VARDEF( cdfid, 'T1000', [londimid,latdimid,timdimid], /FLOAT )

  NCDF_ATTPUT, cdfid, latid, 'long_name', 'latitude'
  NCDF_ATTPUT, cdfid, latid, 'units', 'degrees_north'
  NCDF_ATTPUT, cdfid, lonid, 'long_name', 'longitude'
  NCDF_ATTPUT, cdfid, lonid, 'units', 'degrees_east'
  NCDF_ATTPUT, cdfid, timid, 'long_name', 'time'
  NCDF_ATTPUT, cdfid, timid, 'units', 'hours since 1800-01-01 00:00'
  NCDF_ATTPUT, cdfid, varid, 'long_name', 'air temperature at 1000 m above surface'
  NCDF_ATTPUT, cdfid, varid, 'units', 'K'

  NCDF_ATTPUT, cdfid, 'description', 'air temperature at 1000m above surface interpolated from sigma levels', $
               /GLOBAL
  NCDF_ATTPUT, cdfid, 'created', SYSTIME(), /GLOBAL
  NCDF_ATTPUT, cdfid, 'created_by', 'Andrew P. Barrett <apbarret@nsidc.org>', /GLOBAL

  NCDF_CONTROL, cdfid, /ENDEF

  NCDF_VARPUT, cdfid, latid, lat
  NCDF_VARPUT, cdfid, lonid, lon
  NCDF_VARPUT, cdfid, timid, time
  NCDF_VARPUT, cdfid, varid, ta

  NCDF_CLOSE, cdfid

  RETURN

END
;;----------------------------------------------------------------------
;; MAIN ROUTINE
;;----------------------------------------------------------------------

thislevel = 1000.

year = 2001

FOR year=2001, 2012 DO BEGIN

   PRINT, '% Getting ta and z data for ' + STRING(year,FORMAT='(i4)')
;;----------------------------------------------------------------------
;; Define files
;;----------------------------------------------------------------------
  zfile = 'ERA_Interim.zfull.sigma.48to60.monthly_mean.' + STRING(year,FORMAT='(i4)') + '.nc'
  tfile = 'ERA_Interim.TA.sigma.48to60.monthly_mean.' + STRING(year,FORMAT='(i4)') + '.synth.nc'

  z    = NCDF_READV( zfile, VARNAME='zf' )
  ta   = NCDF_READV( tfile, VARNAME='ta' )
  lat  = NCDF_READV( zfile, VARNAME='lat' )
  lon  = NCDF_READV( zfile, VARNAME='lon' )
  lev  = NCDF_READV( zfile, VARNAME='lev' )
  time = NCDF_READV( zfile, VARNAME='time' )

;;----------------------------------------------------------------------
;; Interpolate air temperature to 1000m
;;----------------------------------------------------------------------
  tlev = INTERP_TAIR( z, ta, LEVEL=thislevel )

;;----------------------------------------------------------------------
;; Write data to NetCDF
;;----------------------------------------------------------------------
  filo = 'ERA_Interim.T.1000m.monthly_mean.' + STRING(year,FORMAT='(i4)') + '.nc'
  PRINT, '% Writing interpolated T to ' + filo
  WRITE_TO_FILE, tlev, lat, lon, time, filo

;;----------------------------------------------------------------------
;; For debugging: plot cross section over Greenland
;;----------------------------------------------------------------------
;ll   = 
;FOR it=0, ntim-1 DO BEGIN
;   IMSIZE, tlev[*,*,it], x0, y0, xs, ys
;   TVSCL, CONGRID( tlev[*,*,it], xs, ys ), x0, y0
;   STOP
;ENDFOR

ENDFOR

END


