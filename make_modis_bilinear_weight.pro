;;----------------------------------------------------------------------
;; Generates bilinear weights files for MODIS tiles.  It expects a netCDF file
;; containing latitude and longitude points for the source grid.  Only the
;; tile name needs to be specified for the MODIS grid.
;;
;; Variable names for latitude and longitude in the source grid can be
;; specified if these variables have different names from the default;
;; lat and lon.
;;
;; The procedure generates eight arrays that are written to a netCDF file.
;; Four of these arrays contain the wieghts for the four neighbouring grid
;; points og the source grid and four arrays contains the indices of these
;; four points.
;;
;; 2013-06-26 A.P.Barrett
;;----------------------------------------------------------------------
PRO WRITE_TO_NCDF, i0, i1, i2, i3, w0, w1, w2, w3, filo, $
                   SRCGRID=SRCGRID, TILE=TILE

;; !!!!!!!!!!!! NOT WORKING !!!!!!!!!!!!!! (NC_ERROR=-31 - Sys error too many
;;                                                         netcdf files open)
  dims = SIZE( i0, /DIMENSIONS )
  ncol = dims[0]
  nrow = dims[1]

  cdfid = NCDF_CREATE( filo, /CLOBBER )
  
  coldimid = NCDF_DIMDEF( cdfid, 'col', ncol )
  rowdimid = NCDF_DIMDEF( cdfid, 'row', nrow )
  
  colid = NCDF_VARDEF( cdfid, 'col', coldimid, /FLOAT )
  rowid = NCDF_VARDEF( cdfid, 'row', rowdimid, /FLOAT )
  
  i0id = NCDF_VARDEF( cdfid, 'i0', [coldimid,rowdimid], /LONG )
;  i1id = NCDF_VARDEF( cdfid, 'i1', [coldimid,rowdimid], /FLOAT )
;  i2id = NCDF_VARDEF( cdfid, 'i2', [coldimid,rowdimid], /FLOAT )
;  i3id = NCDF_VARDEF( cdfid, 'i3', [coldimid,rowdimid], /FLOAT )
;  w0id = NCDF_VARDEF( cdfid, 'w0', [coldimid,rowdimid], /FLOAT )
;  w1id = NCDF_VARDEF( cdfid, 'w1', [coldimid,rowdimid], /FLOAT )
;  w2id = NCDF_VARDEF( cdfid, 'w2', [coldimid,rowdimid], /FLOAT )
;  w3id = NCDF_VARDEF( cdfid, 'w3', [coldimid,rowdimid], /FLOAT )

  ;NCDF_ATTPUT, cdfid, colid, 'long_name', 'column-id'
  ;NCDF_ATTPUT, cdfid, colid, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, rowid, 'long_name', 'row-id'
  ;NCDF_ATTPUT, cdfid, rowid, 'units', 'none'
  
  ;NCDF_ATTPUT, cdfid, i0id, 'long_name', 'index of lower-left grid point'
  ;NCDF_ATTPUT, cdfid, i0id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, i1id, 'long_name', 'index of lower-right grid point'
  ;NCDF_ATTPUT, cdfid, i1id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, i2id, 'long_name', 'index of upper-left grid point'
  ;NCDF_ATTPUT, cdfid, i2id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, i3id, 'long_name', 'index of upper-right grid point'
  ;NCDF_ATTPUT, cdfid, i3id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, w0id, 'long_name', 'weight of lower-left grid point'
  ;NCDF_ATTPUT, cdfid, w0id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, w1id, 'long_name', 'weight of lower-right grid point'
  ;NCDF_ATTPUT, cdfid, w1id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, w2id, 'long_name', 'weight of upper-left grid point'
  ;NCDF_ATTPUT, cdfid, w2id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, w3id, 'long_name', 'weight of upper-right grid point'
  ;NCDF_ATTPUT, cdfid, w3id, 'units', 'none'

  ;NCDF_ATTPUT, cdfid, 'description', $
  ;             'indices and weights for regridding ' + srcgrid + ' to tile ' + tile, /GLOBAL
  ;NCDF_ATTPUT, cdfid, 'creation_date', SYSTIME(), /GLOBAL
  ;NCDF_ATTPUT, cdfid, 'created_by', 'A.P.Barrett apbarret@nsidc.org', /GLOBAL
  
  NCDF_CONTROL, cdfid, /ENDEF

  NCDF_VARPUT, cdfid, rowid, FINDGEN(nrow)
  NCDF_VARPUT, cdfid, colid, FINDGEN(ncol)

  NCDF_VARPUT, cdfid, i0id, i0
;  NCDF_VARPUT, cdfid, i1id, i1
;  NCDF_VARPUT, cdfid, i2id, i2
;  NCDF_VARPUT, cdfid, i3id, i3
;  NCDF_VARPUT, cdfid, w0id, w0
;  NCDF_VARPUT, cdfid, w1id, w1
;  NCDF_VARPUT, cdfid, w2id, w2
;  NCDF_VARPUT, cdfid, w3id, w3

  NCDF_CLOSE, cdfid

  RETURN

END

PRO MAKE_MODIS_BILINEAR_WEIGHT, srcfile, tile, fileout, $
                             LATVAR=LATVAR, LONVAR=LONVAR, $
                             VERBOSE=VERBOSE, SRCGRID=SRCGRID

  IF ( N_PARAMS() NE 3 ) THEN BEGIN
     PRINT, 'USAGE: MAKE_MODIS_BILINEAR_WGT, srcfile, tile, fileout,'
     PRINT, '                          LATVAR=LATVAR, LONVAR=LONVAR'
     RETURN
  ENDIF

  IF ( N_ELEMENTS( srcfile ) EQ 0 ) THEN BEGIN
     PRINT, 'Argument SRCFILE is not defined'
     RETURN
  ENDIF

  IF ( N_ELEMENTS( tile ) EQ 0 ) THEN BEGIN
     PRINT, 'Argument TILE is not defined'
     RETURN
  ENDIF

  IF ( N_ELEMENTS( fileout ) EQ 0 ) THEN BEGIN
     PRINT, 'Argument FILEOUT is not defined'
     RETURN
  ENDIF

  latvar = N_ELEMENTS( latvar ) GT 0 ? latvar : 'lat'
  lonvar = N_ELEMENTS( lonvar ) GT 0 ? lonvar : 'lon'

  srcgrid = N_ELEMENTS( srcgrid ) EQ 0 ? srcfile : srcgrid

  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN BEGIN
     PRINT, 'Calculating bilinear weights to regrid'
     PRINT, '          From: '+srcgrid
     PRINT, ' To MODIS Tile: '+tile
     PRINT, ' Using coordinates in ' + srcfile
     PRINT, ' Writing results to: ' + fileout
  ENDIF

  ;; Get MODIS coordinates
  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting MODIS tile coordinates...'
  mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
  mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
  nmod = N_ELEMENTS( mlat )
  dims = SIZE( mlat, /DIMENSION )
  ncol = dims[0]
  nrow = dims[1]
  
  ;; Get ERA-Interim coordinates
  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting source grid coordinates...'
  elat = NCDF_READV( srcfile, VARNAME=latvar )
  elon = NCDF_READV( srcfile, VARNAME=lonvar )
  nlon = N_ELEMENTS( elon )
  nlat = N_ELEMENTS( elat )

  ;; Reverse elat to make it monotonic increasing
  elat = REVERSE( elat )

  w0 = MAKE_ARRAY( ncol, nrow, /FLOAT )
  w1 = MAKE_ARRAY( ncol, nrow, /FLOAT )
  w2 = MAKE_ARRAY( ncol, nrow, /FLOAT )
  w3 = MAKE_ARRAY( ncol, nrow, /FLOAT )
  i0 = MAKE_ARRAY( ncol, nrow, /LONG )
  i1 = MAKE_ARRAY( ncol, nrow, /LONG )
  i2 = MAKE_ARRAY( ncol, nrow, /LONG )
  i3 = MAKE_ARRAY( ncol, nrow, /LONG )
  
  ;; Find closest row and column of ERA-I data to top row of MODIS tile
  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Calculating weights for...'
  FOR ii=0, nmod-1 DO BEGIN

     IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN BEGIN
        PRINT, ii, FORMAT='("Grid-point: ",i10)'
     ENDIF

     ilat = VALUE_LOCATE( elat, mlat[ii] )
     ilon = VALUE_LOCATE( elon, mlon[ii] )
     
     w = BILINT_WGT( mlon[ii], mlat[ii], elon[ilon], elon[ilon+1], $
                     elat[ilat], elat[ilat+1] )
     
     w0[ii] = w[0]
     w1[ii] = w[1]
     w2[ii] = w[2]
     w3[ii] = w[3]
     i0[ii] = ( ilat * nlon ) + ilon
     i1[ii] = ( ilat * nlon ) + (ilon+1)
     i2[ii] = ( (ilat+1) * nlon ) + ilon
     i3[ii] = ( (ilat+1) * nlon ) + (ilon+1)
     
  ENDFOR

  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN BEGIN
     PRINT, 'Writing weights and indices to ' + fileout
  ENDIF
  ;WRITE_TO_NCDF, i0, i1, i2, i3, w0, w1, w2, w3, fileout, $
  ;               SRCGRID=srcgrid, TILE=tile  
  SAVE, i0, i1, i2, i3, w0, w1, w2, w3, FILE=fileout ;tile+'_binlinear_wgt.sav'

  RETURN

END


