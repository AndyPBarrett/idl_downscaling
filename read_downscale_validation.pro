;;----------------------------------------------------------------------
;; Reads files downscaled_tair_<basin>_validation_<method>.txt
;;
;; 2015-06-10 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION READ_DOWNSCALE_VALIDATION, fili, station, NREC=nrec

  nstat = 5

  station = MAKE_ARRAY( nstat, /STRING, VALUE='' )

  MAXREC = 366
  struct = { year: 0, $
             month: 0, $
             day: 0, $
             doy: 0, $
             tair: MAKE_ARRAY( nstat, /FLOAT, VALUE=-9999. ) }
  result = REPLICATE( struct, MAXREC )

  OPENR, U, fili, /GET_LUN

  line = ''
  k = 0
  istat = 0
  WHILE ( NOT EOF(U) ) DO BEGIN
     
     READF, U, line

     IF ( STREGEX( line, '^#', /BOOLEAN ) EQ 1 ) THEN BEGIN

        field = STRSPLIT( line, /EXTRACT )
        station[istat] = STRTRIM(field[1],2)
        istat = istat + 1

     ENDIF ELSE BEGIN

        field = STRSPLIT( line, /EXTRACT )
        
        result[k].year  = field[0]
        result[k].month = field[1]
        result[k].day   = field[2]
        result[k].doy   = field[3]
        result[k].tair   = field[4:4+nstat-1]

        k = k + 1

     ENDELSE

     IF ( k GE MAXREC ) THEN BREAK

  ENDWHILE

  CLOSE, U
  FREE_LUN, U

  nrec = k

  result = result[0:nrec-1]

  RETURN, result

END
