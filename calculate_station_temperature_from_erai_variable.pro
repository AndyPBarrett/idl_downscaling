
;; Restore bilinear weights
RESTORE, '/home/apbarret/data/CEOP_Himalaya/bilinear_wgt_ceop_station_vector.sav'

;; Get station metadata
station = READ_CEOP_METADATA( '/home/apbarret/data/CEOP_Himalaya/ceop_himalaya_metadata.txt', NREC=nstation ) 

;; Get surface geopotential
diri = '/disks/arctic6_raid/ERA_Interim/t2m'
zsfili = 'era_interim.zsurf.charis.invariant.nc'
zs   = NCDF_READV( diri + '/' + zsfili, VARNAME='z' )
zs   = ((zs*0.808142462576107)+26240.6926694914)/9.812

;;----------------------------------------------------------------------
;; Loop through years
;;----------------------------------------------------------------------
ybeg = 2002
yend = 2010
time = TIMEGEN( START=JULDAY(1,1,ybeg), FINAL=JULDAY(12,31,yend), UNITS='Days' )
CALDAT, time, mo, dy, yr
nrecord = N_ELEMENTS( time )
struct = { year: 0, $
           month: 0, $
           day: 0, $
           lukla: -9999.99, $
           namche: -9999.99, $
           pheriche: -9999.99, $
           pyramid: -9999.99 }
result = REPLICATE( struct, nrecord )
result.year = yr
result.month = mo
result.day = dy

;; Get ERA-I T2m
irec = 0
FOR year=ybeg, yend DO BEGIN

   t2fili = 'era_interim.t2m.day_mean.charis.'+STRING(year,FORMAT='(i4.0)')+'.nc'
   t2m = NCDF_READV( diri + '/' + STRING(year,FORMAT='(i4.0)') + '/' + t2fili, VARNAME='t2m' )
   dims = SIZE( t2m, /DIMENSIONS )
   ntim = dims[2]

   zfili = 'era_interim.z.day_mean.charis.'+STRING(year,FORMAT='(i4.0)')+'.nc'
   z = NCDF_READV( diri + '/' + STRING(year,FORMAT='(i4.0)') + '/' + zfili, VARNAME='z' )
   z = REVERSE( z / 9.821, 3 )
   dims = SIZE( z, /DIMENSIONS )
   nlat = dims[0]
   nlon = dims[1]
   nlev = dims[2]

   gammafile = 'era_interim.lapse_rate.day_mean.charis.'+STRING(year,FORMAT='(i4.0)')+'.nc'
   gamma = NCDF_READV( diri + '/' + STRING(year,FORMAT='(i4.0)') + '/' + gammafile, VARNAME='gamma' )
   gamma = REVERSE( gamma, 3 )
   ngam = nlev-1

;;----------------------------------------------------------------------
;; Calculate temperatures
;;----------------------------------------------------------------------
   zref = 0.0

   tstn = MAKE_ARRAY( nstation, ntim, /FLOAT )
   FOR it=0, ntim-1 DO BEGIN
      tmp = t2m[*,*,it]
      
      ;; Calculate temperature
      tref = tmp + ( gamma[*,*,1,it] * ( zref - zs ) )
      
      ;; Interpolate tref to stations
      tref_stn = ( tref[q11]*w11 ) + (tref[q12]*w12) + (tref[q21]*w21) + (tref[q22]*w22)
      
      gamm_stn = MAKE_ARRAY( nstation, ngam, /FLOAT )
      FOR ig=0, ngam-1 DO BEGIN
         gtmp = gamma[ *, *, ig, it ]
         gamm_stn[ *, ig ] = (gtmp[q11]*w11) + (gtmp[q12]*w12) + (gtmp[q21]*w21) + (gtmp[q22]*w22)
      ENDFOR

      z_stn = MAKE_ARRAY( nstation, nlev, /FLOAT )
      FOR il=0, nlev-1 DO BEGIN
         ztmp = z[ *, *, il, it ]
         z_stn[ *, il ] =  (ztmp[q11]*w11) + (ztmp[q12]*w12) + (ztmp[q21]*w21) + (ztmp[q22]*w22)
      ENDFOR

      
      ttmp = MAKE_ARRAY( nstation, /FLOAT )
      zbase = zref
      FOR ig = 1, ngam-1 DO BEGIN
         inlevel = (station.elev GT z_stn[*,ig] AND station.elev LT z_stn[*,ig+1])
         ablevel = (station.elev GT z_stn[*,ig+1])

         tref_stn = tref_stn + $
                    ( inlevel * gamm_stn[*,ig] * ( station.elev - zbase ) ) + $
                    ( ablevel * gamm_stn[*,ig] * ( z_stn[*,ig] - zbase ) )

         tstn[*,it] = tref_stn

         zbase = z_stn[*,ig]

      ENDFOR

      ;STOP
      ;; Lapse reference temperature to station elevations
      ;tstn[*,it] = tref_stn + ( gamma * ( station.elev - zref ) )
      
   ENDFOR
   
   result[irec:irec+ntim-1].lukla     = REFORM(tstn[0,*])
   result[irec:irec+ntim-1].namche    = REFORM(tstn[1,*])
   result[irec:irec+ntim-1].pheriche  = REFORM(tstn[2,*])
   result[irec:irec+ntim-1].pyramid   = REFORM(tstn[3,*])

   irec = irec+ntim

ENDFOR

OPENW, U, 'ceop_station_temperature_from_erai_variable.txt', /GET_LUN
fmt = '(i4,1x,i02,1x,i02,4(1x,f8.2))'
FOR ir=0, nrecord-1 DO PRINTF, U, result[ir], FORMAT=fmt
CLOSE, U
FREE_LUN, U

;;----------------------------------------------------------------------
;; Plot results
;;----------------------------------------------------------------------

PLOT, time, result.lukla, MIN_VALUE=-9999., $
      /NODATA, $
      XRANGE=[JULDAY(1,1,ybeg),JULDAY(12,31,yend)], XSTYLE=1, $
      YRANGE=[ FLOOR( MIN(tstn)/10. )*10., CEIL( MAX(tstn)/10. )*10. ], $
      YSTYLE=1, YTITLE='Temperature (K)'
OPLOT, time, result.lukla,    MIN_VALUE=-9999., COLOR=FSC_COLOR('pink')
OPLOT, time, result.namche,   MIN_VALUE=-9999., COLOR=FSC_COLOR('cyan')
OPLOT, time, result.pheriche, MIN_VALUE=-9999., COLOR=FSC_COLOR('coral')
OPLOT, time, result.pyramid,  MIN_VALUE=-9999., COLOR=FSC_COLOR('olive')


END


