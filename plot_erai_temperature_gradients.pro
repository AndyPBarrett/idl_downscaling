
station = READ_CEOP_METADATA( '/home/apbarret/data/CEOP_Himalaya/ceop_himalaya_metadata.txt', NREC=nmod ) 

;; Get ERA-Interim coordinates and data
diri = '/disks/arctic6_raid/ERA_Interim/t2m'
tfili = 'era_interim.tair.day_mean.charis.2000.nc'
elat = NCDF_READV( diri + '/2000/' + tfili, VARNAME='lat' )
elat = REVERSE( elat ) ; to monotonic increasing
elon = NCDF_READV( diri + '/2000/' + tfili, VARNAME='lon' )
nlat = N_ELEMENTS( elat )
nlon = N_ELEMENTS( elon )

tair = NCDF_READV( diri + '/2000/' + tfili, VARNAME='tair' )

t2fili = 'era_interim.t2m.day_mean.charis.2000.nc'
t2m = NCDF_READV( diri + '/2000/' + t2fili, VARNAME='t2m' )

zfili = 'era_interim.z.day_mean.charis.2000.nc'
z    = NCDF_READV( diri + '/2000/' + zfili, VARNAME='z' )
z = z / 9.812

zsfili = 'era_interim.zsurf.charis.invariant.nc'
zs   = NCDF_READV( diri + '/' + zsfili, VARNAME='z' )
zs   = ((zs*0.808142462576107)+26240.6926694914)/9.812

ipt = MAKE_ARRAY( 4, nmod, /LONG )
FOR imod=0, nmod-1 DO BEGIN
   ilat = VALUE_LOCATE( elat, station[imod].lat )
   ilon = VALUE_LOCATE( elon, station[imod].lon )
   ipt[*,imod]  = ( (ilat+[0,1,0,1]) * nlon ) + ilon+[0,0,1,1]
ENDFOR

ntim = 366

ilat = ilat+[0,1,0,1]
ilon = ilon+[0,0,1,1]
FOR it=0, ntim-1 DO BEGIN

   PLOT, RANDOMU(seed,10), RANDOMU(seed,10), $
         XRANGE=[200,300], XSTYLE=1, XTITLE='Temperature (K)', $
         YRANGE=[0,10000.], YSTYLE=1, YTITLE='Height (m)', $
         /NODATA, TITLE='Day '+STRTRIM(it+1,2)
   
   FOR ip=0, 3 DO OPLOT, tair[ ilon[ip], ilat[ip], *, it ], z[ ilon[ip], ilat[ip], *, it ], PSYM=-1

   FOR ip=0, 3 DO PLOTS, t2m[ ilon[ip], ilat[ip], it ], zs[ ilon[ip], ilat[ip] ], PSYM=2
   WAIT, 1

ENDFOR

END
