;;----------------------------------------------------------------------
;; Reads estimates the ERA-Interim bias correction from stations
;;
;; 2015-06-19 A.P.Barrett
;;----------------------------------------------------------------------
PRO READ_BIAS_CORRECTION, fili, bias, station

;erai_diri = '/disks/arctic6_raid/ERA_Interim/daily'
;fili = erai_diri + '/' + 'bias_erai_hunza_tair.txt'

  nstat = 3  ;; This will need to read from file in a future version
  nday = 365

  struct = { name: '', elevation: -9999.99, latitude: -9999.99, longitude: -9999.99 }
  station = REPLICATE( struct, nstat )
  tmp = MAKE_ARRAY( nstat+1, nday, /FLOAT )

  OPENR, U, fili, /GET_LUN
  
  line = ''
  FOR is=0, nstat-1 DO BEGIN
     
     READF, U, line
     
     field = STRSPLIT( line, /EXTRACT )
     
     station[is].name      = field[1]
     station[is].elevation = field[2]
     station[is].latitude  = field[3]
     station[is].longitude = field[4]

  ENDFOR

  READF, U, tmp
  
  CLOSE, U
  FREE_LUN, U

  doy = tmp[0,*]
  bias = tmp[1:3,*]

  RETURN

END

