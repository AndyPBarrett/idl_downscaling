
PRO MAKE_MAP, data, lon, lat, SCALE=SCALE, MINX=MINX, MAXX=MAXX, ADVANCE=ADVANCE

  scale = N_ELEMENTS( scale ) EQ 0 ? 1 : scale
 
  minlat = 28.
  minlon = 67.
  maxlat = 42.
  maxlon = 94.

  cntlat = MEAN( [minlat,maxlat] )
  cntlon = MEAN( [minlon,maxlon] )

  image = data
  idx = WHERE( image LE -9999., num )
  IF ( num GT 0 ) THEN image[idx] = !VALUES.F_NAN
  image = image * scale

  minx  = N_ELEMENTS( minx ) EQ 0 ? FLOOR( MIN( image, /NAN ) ) : minx
  maxx  = N_ELEMENTS( maxx ) EQ 0 ? CEIL( MAX( image, /NAN ) ) : maxx
  range = maxx - minx
  nbin = 11.
  width = range / (nbin-1)
  level = ( FINDGEN( nbin ) * width ) + minx
  color = BYTSCL( INDGEN(nbin), TOP=240 ) + 14
  
  print, minx, maxx
  print, range, width
  PRINT, level
  PRINT, color

  LOADCT, 39

  MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, LIMIT=[minlat,minlon,maxlat,maxlon], $
           ADVANCE=ADVANCE
  CONTOUR, image, lon, lat, /OVERPLOT, /CELL_FILL, LEVEL=level, C_COLOR=color

  MAP_CONTINENTS, /COUNTRIES, /HIRES
  MAP_GRID, /box

  RETURN

END

FUNCTION BILINT_WITH_WGT, ingrid, i0, i1, i2, i3, w0, w1, w2, w3

  tmp0 = ingrid[ i0 ]
  tmp1 = ingrid[ i1 ]
  tmp2 = ingrid[ i2 ]
  tmp3 = ingrid[ i3 ]

  avgData = ( tmp0 + tmp1 + tmp2 + tmp3 ) * 0.25

  idx = WHERE( FINITE( tmp0 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp0[ idx ] = avgData[ idx ]

  idx = WHERE( FINITE( tmp1 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp1[ idx ] = avgData[ idx ]

  idx = WHERE( FINITE( tmp2 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp2[ idx ] = avgData[ idx ]

  idx = WHERE( FINITE( tmp3 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp3[ idx ] = avgData[ idx ]

  outgrid = ( tmp0 * w0 ) + ( tmp1 * w1 ) + $
            ( tmp2 * w2 ) + ( tmp3 * w3 )
  
  RETURN, outgrid

END

;; Tile
tile = 'h24v05'
srcfile = 'era_interim.lapserate.hkh.200806.nc'

latvar = 'lat'
lonvar = 'lon'

VERBOSE=1

;; Get MODIS geolocation
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting MODIS tile coordinates...'
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Get ERA-Interim coordinates
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting source grid coordinates...'
elat   = NCDF_READV( srcfile, VARNAME=latvar )
elon   = NCDF_READV( srcfile, VARNAME=lonvar )
tref0  = NCDF_READV( srcfile, VARNAME='tref0' )
gamma0 = NCDF_READV( srcfile, VARNAME='gamma0' )
tref1  = NCDF_READV( srcfile, VARNAME='tref1' )
gamma1 = NCDF_READV( srcfile, VARNAME='gamma1' )
zinv   = NCDF_READV( srcfile, VARNAME='zinv' )
isinv   = NCDF_READV( srcfile, VARNAME='isinv' )
nlon   = N_ELEMENTS( elon )
nlat   = N_ELEMENTS( elat )

;; Get MODIS elevation
direl = '/projects/CHARIS/elevation_data/'
filel = 'sin_'+tile+'.tif'
elev = READ_TIFF( direl + '/' + filel )

;; Restore weights files
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Restoring weights files...'
RESTORE, tile+'_hkh_bilinear_wgt.sav'

;; Get MODIS cell indices file
cell_index = OPEN_IMAGE( 'modis_era_gridcells_' + tile + '.long.bin', 2400, 2400, 3 )

;; Reverse elat to make it monotonic increasing
elat = REVERSE( elat )
;; ... and data
data = REVERSE( data, 2 )

ismissing = WHERE( data LE -9999.99, nummissing )
IF ( nummissing GT 0 ) THEN data[ ismissing ] = !VALUES.F_NAN

elat2d = REBIN( TRANSPOSE( elat ), nlon, nlat, /SAMPLE )
elon2d = REBIN( elon, nlon, nlat, /SAMPLE )

;; get time dimension
dims = SIZE( data, /DIMENSIONS )
ntim = dims[2]

!P.MULTI = [0,1,2]

scale = 1.

ntim = 10
FOR it = 0, ntim-1 DO BEGIN

   ingrid = data[*,*,it]

   ;; Regrid data
   tref0_m  = BILINT_WITH_WGT( tref0, i0, i1, i2, i3, w0, w1, w2, w3 )
   gamma0_m = BILINT_WITH_WGT( gamma0, i0, i1, i2, i3, w0, w1, w2, w3 )
   tref1_m  = BILINT_WITH_WGT( tref0, i0, i1, i2, i3, w0, w1, w2, w3 )
   gamma1_m = BILINT_WITH_WGT( gamma0, i0, i1, i2, i3, w0, w1, w2, w3 )
   zinv_m   = BILINT_WITH_WGT( zinv, i0, i1, i2, i3, w0, w1, w2, w3 )


   ;; Set MODIS cells within missing ERAI cells to missing
   ;; Still needs to be done

   tsurf = MAKE_ARRAY( ncol, nrow, /FLOAT )

   ;; Calculate surface temperature for above inversion or no inversion cases
   tsurf = tref0_m + ( gamma0_m * elev )
   
   ;; Calculate surface temperature for below inversion
   idx = WHERE( elev GT zinv, num )
   IF ( num GT 0 ) THEN BEGIN
      tsurf[ idx ] = tref1[ idx ] + ( gamma1[ idx ] * elev[ idx ] )
   ENDIF

;;----------------------------------------------------------------------
;; Plot results
;;----------------------------------------------------------------------
   minx = FLOOR( MIN( [ MIN(ingrid), MIN(outgrid) ]*scale ) )
   maxx = CEIL( MAX( [ MAX(ingrid), MAX(outgrid) ]*scale ) )
 
   PRINT, it
   ERASE
   ;MAKE_MAP, ingrid, elon, elat, SCALE=scale, MINX=minx, MAXX=maxx

   MAKE_MAP, tsurf, mlon, mlat, SCALE=scale, /ADVANCE, MINX=minx, MAXX=maxx

   STOP

ENDFOR

END

