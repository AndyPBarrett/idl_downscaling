;;----------------------------------------------------------------------
;; Makes a comparison time series plot of observed 2m air temperatures and
;; temperatures downscaled from ERA-Interim using the Jarosch method.
;;
;; 2015-06-10 A.P.Barrett
;;----------------------------------------------------------------------

PRO PLOT_PANEL, obs, dscale1, dscale2, title

  not_valid = WHERE( obs.tavg LE -9999., num_notvalid )
  IF ( num_notvalid GT 0 ) THEN obs[not_valid].tavg = !VALUES.F_NAN

  ymin = FLOOR( MIN( [obs.tavg, dscale1, dscale2], /NAN ) / 10. ) * 10.
  ymax = CEIL( MAX( [obs.tavg, dscale1, dscale2], /NAN ) / 10. ) * 10.
  PLOT, obs.doy, obs.tavg, MIN_VALUE=-9998., $
      XRANGE=[0,366], XSTYLE=1, XTITLE='Day of Year', $
      YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Air temperature (!Uo!NC)', $
      TITLE=title, THICK=2
  PLOTS, !X.CRANGE, 0

  OPLOT, obs.doy, dscale1, PSYM=1, COLOR=FSC_COLOR('red'), THICK=2
  OPLOT, obs.doy, dscale2, PSYM=1, COLOR=FSC_COLOR('cyan'), THICK=2

END


;;----------------------------------------------------------------------
;; Main routine
;;----------------------------------------------------------------------

year = 2001

;;----------------------------------------------------------------------
;; Get data
;;----------------------------------------------------------------------

;; Observations
obs_diri = '/projects/CHARIS/surface_met_data/WAPDA'
fili = STRING( obs_diri, 'khunjerab', 'khunjerab', year, FORMAT='(a,"/",a,"/",a,"_",i4,".qc1.txt")' )
khun = READ_CHARIS_CLIMATE( fili, NREC=n_khun )

fili = STRING( obs_diri, 'naltar', 'naltar', year, FORMAT='(a,"/",a,"/",a,"_",i4,".qc1.txt")' )
nalt = READ_CHARIS_CLIMATE( fili, NREC=n_nalt )

fili = STRING( obs_diri, 'ziarat', 'ziarat', year, FORMAT='(a,"/",a,"/",a,"_",i4,".qc1.txt")' )
ziar = READ_CHARIS_CLIMATE( fili, NREC=n_ziar )

;; Downscaled - with inversion
dscale_inv = READ_DOWNSCALE_VALIDATION( 'downscaled_tair_hunza_validation.txt', station, NREC=ninv )

;; Downscaled - no inversion
dscale_noinv = READ_DOWNSCALE_VALIDATION( 'downscaled_tair_hunza_validation_noinversion.txt', station, NREC=nnoinv )

;;----------------------------------------------------------------------
;; Make time series plot
;;----------------------------------------------------------------------

!P.MULTI=[0,1,3]

PLOT_PANEL, khun, dscale_inv.tair[0]-273.15, dscale_noinv.tair[0]-273.15, 'Khunjerab'
PLOT_PANEL, nalt, dscale_inv.tair[1]-273.15, dscale_noinv.tair[1]-273.15, 'Naltar'
PLOT_PANEL, ziar, dscale_inv.tair[2]-273.15, dscale_noinv.tair[2]-273.15, 'Ziarat'

!P.MULTI=0

END
 
