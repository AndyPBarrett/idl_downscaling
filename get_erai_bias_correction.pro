;;----------------------------------------------------------------------
;; Estimates the ERA-Interim bias correction from stations
;;
;; 2015-06-19 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION GET_ERAI_VALIDATION_MEAN, fili

  tmp = MAKE_ARRAY( 11, 365, /FLOAT )

  OPENR, U, fili, /GET_LUN
  READF, U, tmp
  CLOSE, U
  FREE_LUN, U

  doy     = tmp[0,*]
  clim    = tmp[1:5,*]
  climvar = tmp[6:10,*]

  result = {doy: doy, clim: clim, climvar:climvar }

  RETURN, result

END

FUNCTION READ_OBSERVED_CLIMATOLOGY, fili

  
  tmp = MAKE_ARRAY( 4, 365, /FLOAT )

  OPENR, U, fili, /GET_LUN
  READF, U, tmp
  CLOSE, U
  FREE_LUN, U

  doy     = tmp[0,*]
  clim    = tmp[1:3,*] + 273.15

  result = {doy: doy, clim: clim }

  RETURN, result

END

;;----------------------------------------------------------------------
;; Get ERA-Interim derived temperatures
;;----------------------------------------------------------------------

erai_diri = '/disks/arctic6_raid/ERA_Interim/daily'
erai_fili = 'downscaled_tair_hunza_validation_mean.txt'

erai = GET_ERAI_VALIDATION_MEAN( erai_diri + '/' + erai_fili )

;;----------------------------------------------------------------------
;; Get Station Observations
;;----------------------------------------------------------------------
obs_diri = '/projects/CHARIS/surface_met_data/WAPDA'

fili = obs_diri + '/' + 'khunjerab' + '/' + 'khunjerab_climatology_1995to2009.txt'
khun = READ_OBSERVED_CLIMATOLOGY( fili )

fili = obs_diri + '/' + 'naltar' + '/' + 'naltar_climatology_1995to2009.txt'
nalt = READ_OBSERVED_CLIMATOLOGY( fili )

fili = obs_diri + '/' + 'ziarat' + '/' + 'ziarat_climatology_1995to2009.txt'
ziar = READ_OBSERVED_CLIMATOLOGY( fili )

;;----------------------------------------------------------------------
;; Calculate bias for each station
;;----------------------------------------------------------------------

bias = MAKE_ARRAY( 3, 365, /FLOAT )
bias[2,*] = khun.clim[0,*] - erai.clim[0,*]
bias[1,*] = ziar.clim[0,*] - erai.clim[2,*]
bias[0,*] = nalt.clim[0,*] - erai.clim[1,*]

;;----------------------------------------------------------------------
;; get station info
;;----------------------------------------------------------------------

stations = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0, $
                          'LATITUDE', 36.1667, 'LONGITUDE', 74.1833 ), $
             CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0, $
                          'LATITUDE', 36.7980, 'LONGITUDE', 74.4820  ), $
             CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0, $
                          'LATITUDE', 36.8411, 'LONGITUDE', 75.4192 ) ]
;; Make sure stations are ordered by elevation
stations = stations[ SORT( stations.elevation ) ]
nstat = N_ELEMENTS( stations )

;;----------------------------------------------------------------------
;; Write bias to file
;;----------------------------------------------------------------------
filo = 'bias_erai_hunza_tair.txt'
OPENW, U, erai_diri + '/' + filo, /GET_LUN
FOR is=0, nstat-1 DO PRINTF, U, stations[is], FORMAT='("# ",a15,1x,f6.1,2(1x,f9.4))'
FOR id=0, 364 DO PRINTF, U, id+1, bias[*,id], FORMAT='(i3,3(1x,f6.2))'
CLOSE, U
FREE_LUN, U

;;----------------------------------------------------------------------
;; Make plot
;;----------------------------------------------------------------------

z = 2500 + ( FINDGEN( 25 ) * 100. )

FOR id = 0, 1 DO BEGIN

   yest0 = MEAN( bias[*,id ] )

   b = POLY_FIT( stations.elevation, bias[*,id], 1, YFIT=yhat )
   yest1 = b[0] + (b[1]*z)

   b = POLY_FIT( stations.elevation, bias[*,id], 2, YFIT=yhat )
   yest2 = b[0] + (b[1]*z) + (b[2]*z*z)

   ;; Piecewise correction
   b0 = POLY_FIT( stations[[0,1]].elevation, bias[[0,1],id], 1, YFIT=yhat )
   b1 = POLY_FIT( stations[[1,2]].elevation, bias[[1,2],id], 1, YFIT=yhat )
   yest3 = MAKE_ARRAY( N_ELEMENTS(z), /FLOAT )
   idz = WHERE( z LT stations[1].elevation, numz )
   IF ( numz GT 0 ) THEN yest3[idz] = b0[0] + ( z[idz] * b0[1] )
   idz = WHERE( z GE stations[1].elevation, numz )
   IF ( numz GT 0 ) THEN yest3[idz] = b1[0] + ( z[idz] * b1[1] )

   ymin = FLOOR( MIN( [yest0, yest1, yest2, yest3 ] ) )
   ymax = CEIL( MAX( [yest0, yest1, yest2, yest3 ] ) )

   PLOT, stations.elevation, bias[*,id], $
         XTITLE='Elevation (m)', $
         YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Correction (K)', $
         PSYM=2, TITLE=STRING(id+1,FORMAT='("Day = ",i3)'), FONT=0, $
         THICK=4, SYMSIZE=2

   PLOTS, !X.CRANGE, yest0, COLOR=FSC_COLOR('cyan'), THICK=4
   OPLOT, z, yest1, COLOR=FSC_COLOR('red'), THICK=4
   OPLOT, z, yest2, COLOR=FSC_COLOR('pink'), THICK=4
   OPLOT, z, yest3, COLOR=FSC_COLOR('firebrick'), THICK=4

ENDFOR

END

