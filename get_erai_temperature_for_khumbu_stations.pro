;;----------------------------------------------------------------------
;; Extracts T2m from ERAI for Khumbu CEOP stations
;;----------------------------------------------------------------------
ybeg = 2002
yend = 2010

;; Get station metadata
station = READ_CEOP_METADATA( '/home/apbarret/data/CEOP_Himalaya/ceop_himalaya_metadata.txt', NREC=nstation ) 

;;----------------------------------------------------------------------
;; Find nearest ERA-I grid cell to stations
;;----------------------------------------------------------------------
erai_diri = '/raid/ERA_Interim/t2m'
erai_fili = 'era_interim.zsurf.charis.invariant.nc'
lat = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='latitude' )
lon = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='longitude' )

FIND_STATION_GRID_CELL, lat, lon, station.lat, station.lon, ii, jj

PRINT, ii
PRINT, jj

;;----------------------------------------------------------------------
;; Define results structure
;;----------------------------------------------------------------------
time = TIMEGEN( START=JULDAY(1,1,ybeg), FINAL=JULDAY(12,31,yend), UNITS='Days' )
CALDAT, time, mo, dy, yr
nrecord = N_ELEMENTS( time )
struct = { year: 0, $
           month: 0, $
           day: 0, $
           lukla: -9999.99, $
           namche: -9999.99, $
           pheriche: -9999.99, $
           pyramid: -9999.99 }
result = REPLICATE( struct, nrecord )
result.year = yr
result.month = mo
result.day = dy

END

