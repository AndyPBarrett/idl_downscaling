
PRO FIND_STATION_GRID_CELL, lat, lon, mlat, mlon, ii, jj

  nlat = N_ELEMENTS( lat )
  nlon = N_ELEMENTS( lon )

;; Check if lat and lon are monotonic
  reverse_lat = 0
  reverse_lon = 0
  IF ( lat[0] GT lat[nlat-1] ) THEN reverse_lat = 1
  IF ( lon[0] GT lon[nlon-1] ) THEN reverse_lon = 1
  
;; Find grid cell spacing - assumes constant for lat and lon
  dlat = ABS( lat[1] - lat[0] )
  dlon = ABS( lon[1] - lon[0] )
  
  ii = ROUND( (mlon - MIN(lon))/dlon )
  jj = ROUND( (mlat - MIN(lat))/dlat )
  
  IF ( reverse_lat EQ 1 ) THEN jj = nlat - 1 - jj
  IF ( reverse_lon EQ 1 ) THEN ii = nlon - 1 - ii

  RETURN

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------

station  = [ 'Khunjrab',  'Naltar',  'Ziarat', 'Gilgit', 'Islamabad' ]
mlat     = [  36.850000, 36.160300, 36.798000,  35.9219,     33.7167 ] 
mlon     = [  75.400000, 74.179883, 74.482000,  72.2892,     73.0667 ]
nstat = N_ELEMENTS( station )

erai_diri = RAID_PATH('arctic6') + '/ERA_Interim/t2m/2001'
erai_fili = 'era_interim.t2m.hkh.200101.nc'
lat = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lat' )
lon = NCDF_READV( erai_diri + '/' + erai_fili, VARNAME='lon' )

lat2d = REBIN( TRANSPOSE(lat), N_ELEMENTS(lon), N_ELEMENTS(lat), /SAMPLE )
lon2d = REBIN( lon, N_ELEMENTS(lon), N_ELEMENTS(lat), /SAMPLE )
latg = (lat + SHIFT(lat,1))*0.5
long = (lon + SHIFT(lon,1))*0.5

;;----------------------------------------------------------------------
;; Find grid cells containing stations
;;----------------------------------------------------------------------

FIND_STATION_GRID_CELL, lat, lon, mlat, mlon, ii, jj

;;----------------------------------------------------------------------
;; Write results
;;----------------------------------------------------------------------

FOR im=0, nstat-1 DO BEGIN
   PRINT, station[im], mlat[im], mlon[im], ii[im], jj[im], $
          lat2d[ii[im],jj[im]] , $
          lon2d[ii[im],jj[im]] , $
          FORMAT='(a10,2(1x,f7.2),2(1x,i3),2(1x,f7.2))'
ENDFOR

;;----------------------------------------------------------------------
;; Plot results
;;----------------------------------------------------------------------

;; For checking plot points
lat0 = 33.
lon0 = 70.
lat1 = 38.
lon1 = 77.

latx = 36.
lonx = 74.

idx = WHERE( ( lat2d GE lat0 AND lat2d LE lat1 ) AND $
             ( lon2d GE lon0 AND lon2d LE lon1 ), numlat )

MAP_SET, 36., 74., /LAMBERT, /ISOTROPIC, LIMIT=[33.,70.,38.,77.]
MAP_CONTINENTS, /COUNTRIES, /HIRES
;MAP_GRID
PLOTS, mlon, mlat, PSYM=2, THICK=2, COLOR=FSC_COLOR('red')
PLOTS, lon2d[idx], lat2d[idx], PSYM=1, THICK=1, COLOR=FSC_COLOR('green')

idx = WHERE( latg GE lat0 AND latg LE lat1, num )
FOR il=0, num-1 DO PLOTS, MAP_2POINTS( lon0, latg[idx[il]], lon1, latg[idx[il]], DPATH=0.1 ), $
                          COLOR=FSC_COLOR('green')
idx = WHERE( long GE lon0 AND long LE lon1, num )
FOR il=0, num-1 DO PLOTS, MAP_2POINTS( long[idx[il]], lat0, long[idx[il]], lat1, DPATH=0.1 ), $
                          COLOR=FSC_COLOR('green')

XYOUTS, mlon+0.1, mlat+0.1, station

FOR im=0, nstat-1 DO $
   PLOTS, lon2d[ ii[im], jj[im] ], lat2d[ ii[im], jj[im] ], $
          PSYM=2, COLOR=FSC_COLOR('yellow')

END

