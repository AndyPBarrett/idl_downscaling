;;----------------------------------------------------------------------
;; Uses a MLR approach to generate a 2 m temperature surface.
;;
;; 2015-06-15 A.P.Barrett
;;----------------------------------------------------------------------

PRO PLOT_PANEL, x, y, _EXTRA=EXTRA_KEYWORDS

   b = REGRESS( x, y, CONST=a, YFIT=yhat )
   error = y - yhat
   mae  = MEAN( ABS( error ) )
   rmse = SQRT( MEAN( error * error ) )
   PLOT, x, y, PSYM=2, THICK=2, $
         _EXTRA=EXTRA_KEYWORDS
   ypr = a + ( !X.CRANGE * b[0] )
   PLOTS, !X.CRANGE, ypr, THICK=2, COLOR=FSC_COLOR('red')

   x0 = !X.CRANGE[0] + ( 0.6 * ( !X.CRANGE[1]-!X.CRANGE[0] ) )
   y0 = !Y.CRANGE[0] + ( 0.9 * ( !Y.CRANGE[1]-!Y.CRANGE[0] ) )
   dy = 0.05 * ( !Y.CRANGE[1]-!Y.CRANGE[0] )
   XYOUTS, x0, y0, STRING( b[0]*1000., FORMAT='("b = ",f8.2)' )
   XYOUTS, x0, y0-dy, STRING( mae, FORMAT='("MAE = ",f6.2)' )
   XYOUTS, x0, y0-(2*dy), STRING( rmse, FORMAT='("RMSE = ",f6.2)' )
     
END

year = 2001

;;----------------------------------------------------------------------
;; Dummy structure holding station info
;; To be replaced by reader from file
;;----------------------------------------------------------------------

stations = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0, $
                          'LATITUDE', 36.1667, 'LONGITUDE', 74.1833 ), $
             CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0, $
                          'LATITUDE', 36.7980, 'LONGITUDE', 74.4820  ), $
             CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0, $
                          'LATITUDE', 36.8411, 'LONGITUDE', 75.4192 ) ]
;; Make sure stations are ordered by elevation
stations = stations[ SORT( stations.elevation ) ]
nstat = N_ELEMENTS( stations )

;;----------------------------------------------------------------------
;; Get data for a given year
;;----------------------------------------------------------------------
wapda_diri = '/projects/CHARIS/surface_met_data/WAPDA'
khun = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'khunjerab/khunjerab_'+STRTRIM(year,2)+'.qc1.fill.txt' )
nalt = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'naltar/naltar_'+STRTRIM(year,2)+'.qc1.fill.txt' )
ziar = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'ziarat/ziarat_'+STRTRIM(year,2)+'.qc1.fill.txt' )
nday = N_ELEMENTS( khun )

;;----------------------------------------------------------------------
;; Generate relationships between stations based on coordinates and elevation
;;----------------------------------------------------------------------

!P.MULTI=[0,3,1]

FOR id = 0, nday-1 DO BEGIN

   y = [ nalt[id].tavg, ziar[id].tavg, khun[id].tavg ] 
   idx = WHERE( y LT -9998., num )
   IF ( num GT 0 ) THEN y[idx] = !VALUES.F_NAN

   x = stations.elevation
   PLOT_PANEL, x, y, XRANGE=[2800,4800], XSTYLE=1, XTITLE='Elevation (m)', $
               TITLE=STRING( id+1, FORMAT='("Day = ",i3)' )

   x = HAVERSINE( stations[0].latitude, 74.5, stations.latitude, 74.5 )
   PLOT_PANEL, x, y, XRANGE=[0.,25.], XSTYLE=1, XTITLE='Latitude (!Uo!N N)'

   x = HAVERSINE( 36.5, stations[0].longitude, 36.5, stations.longitude )
   PLOT_PANEL, x, y, XRANGE=[0, 150.], XSTYLE=1, XTITLE='Longitude (!Uo!N N)'

   xx = MAKE_ARRAY( 3, nstat, /FLOAT )
   xx[0,*] = stations.elevation
   xx[1,*] = HAVERSINE( stations[0].latitude, 74.5, stations.latitude, 74.5 )
   xx[2,*] = HAVERSINE( 36.5, stations[0].longitude, 36.5, stations.longitude )

   b = REGRESS( xx, y, CONST=a, YFIT=yhat )
   error = y - yhat
   mae = MEAN( ABS( error ) )
   rmse = SQRT( MEAN( error*error ) )
   
   PRINT, STRING( b[0]*1000., FORMAT='("b = ",f8.2)' )
   PRINT, STRING( b[1], FORMAT='("b = ",f8.4)' )
   PRINT, STRING( b[2], FORMAT='("b = ",f8.4)' )
   PRINT, STRING( mae, FORMAT='("MAE = ",f6.2)' )
   PRINT, STRING( rmse, FORMAT='("RMSE = ",f6.2)' )

   STOP

ENDFOR

END
