PRO WRITE_LT_DAILY_MEAN_TO_NETCDF, data, lat, lon, filo, GLOBAL=GLOBAL, MISSING=MISSING

missing = N_ELEMENTS( missing ) GT 0 ? missing : 1e20

dims = SIZE( data, /DIMENSIONS )

nlon = dims[0]
nlat = dims[1]
ntim = dims[2]

time = FINDGEN( ntim ) + 1

cdfid = NCDF_CREATE( filo, /CLOBBER )

;; Define dimensions
xdimid = NCDF_DIMDEF( cdfid, 'lon', nlon )
ydimid = NCDF_DIMDEF( cdfid, 'lat', nlat )
tdimid = NCDF_DIMDEF( cdfid, 'time', ntim )

latid = NCDF_VARDEF( cdfid, 'lat', ydimid, /FLOAT )
lonid = NCDF_VARDEF( cdfid, 'lon', xdimid, /FLOAT )
timid = NCDF_VARDEF( cdfid, 'time', tdimid, /FLOAT )

varid = NCDF_VARDEF( cdfid, 't2m', [xdimid, ydimid, tdimid], /FLOAT )

NCDF_ATTPUT, cdfid, latid, 'long_name', 'latitude'
NCDF_ATTPUT, cdfid, latid, 'units', 'degrees_north'

NCDF_ATTPUT, cdfid, lonid, 'long_name', 'longitude'
NCDF_ATTPUT, cdfid, lonid, 'units', 'degrees_east'

NCDF_ATTPUT, cdfid, timid, 'long_name', 'time'
NCDF_ATTPUT, cdfid, timid, 'units', 'day of year; 1 = Jan 1'

NCDF_ATTPUT, cdfid, varid, 'long_name', '2 m air temperature'
NCDF_ATTPUT, cdfid, varid, 'units', 'K'
NCDF_ATTPUT, cdfid, varid, '_FillValue', missing


IF ( KEYWORD_SET(GLOBAL) EQ 1 ) THEN BEGIN
   tagname = STRLOWCASE( TAG_NAMES( global ) )
   nglobal = N_ELEMENTS( tagname )
   FOR ig=0, nglobal-1 DO BEGIN
      NCDF_ATTPUT, cdfid, tagname[ig], global.(ig), /GLOBAL
   ENDFOR
ENDIF

NCDF_ATTPUT, cdfid, 'created_by', 'A.P.Barrett <apbarret@nsidc.org>', $
             /GLOBAl
NCDF_ATTPUT, cdfid, 'created_on', SYSTIME(), /GLOBAL

NCDF_CONTROL, cdfid, /endef

NCDF_VARPUT, cdfid, latid, lat
NCDF_VARPUT, cdfid, lonid, lon
NCDF_VARPUT, cdfid, timid, time
NCDF_VARPUT, cdfid, varid, data

NCDF_CLOSE, cdfid

END

 
