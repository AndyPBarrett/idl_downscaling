;;----------------------------------------------------------------------
;; Calculates temperature lapse rates based on WAPDA climate stations in Hunza
;; basin.
;;
;; 2014-03-06 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION LAPSE_RATE, t0, t1, h0, h1

  idx = WHERE( t0 GT -9998 AND t1 GT -9998, num )
  IF ( num LE 0 ) THEN RETURN, -9999.99

  tdiff = t0[idx] - t1[idx]
  hdiff = h0 - h1
  lapse = (-1000.) * tdiff / hdiff

  RETURN, lapse

END

FUNCTION READ_ERAI_LAPSE_FOR_STATION, fili

  MAXREC = 366
  struct = {year:   0, $
            month:  0, $
            day:    0, $
            gamma0: MAKE_ARRAY(3,/FLOAT,VALUE=-9999.99), $
            gamma1: MAKE_ARRAY(3,/FLOAT,VALUE=-9999.99) }
  result = REPLICATE( struct, MAXREC )

  OPENR, U, fili, /GET_LUN

  line = ''
  FOR i=0,8 DO READF, U, line

  k = 0
  WHILE ( NOT EOF(U) ) DO BEGIN

     READF, U, line
     field = STRSPLIT( line, /EXTRACT )

     result[k].year   = field[0]
     result[k].month  = field[1]
     result[k].day    = field[2]
     result[k].gamma0 = field[3:5]
     result[k].gamma1 = field[6:8]

     k = k + 1

     IF ( k GE MAXREC ) THEN BEGIN
        PRINT, '% READ_ERAI_LAPSE_FOR_STATION: number of records read exceeds MAXREC'
        BREAK
     ENDIF

  ENDWHILE

  nrec = k

  result = result[0:nrec-1]

  RETURN, result

END
     
;;----------------------------------------------------------------------
;; Dummy structure holding station info
;; To be replaced by reader from file
;;----------------------------------------------------------------------

stations = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0 ), $
             CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0 ), $
             CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0 ) ]
;; Make sure stations are ordered by elevation
stations = stations[ SORT( stations.elevation ) ]
nstat = N_ELEMENTS( stations )

;;----------------------------------------------------------------------
;; Get station data
;;----------------------------------------------------------------------
wapda_diri = '/projects/CHARIS/streamflow/Pakistan/' + $
             'Data_from_DHashmi/from_danial/Data_for_Andrew_Barnett/climate/'
year = [2008]
nyear = N_ELEMENTS( year )
FOR iy=0, nyear-1 DO BEGIN
   khun = READ_WAPDA_CLIMATE( wapda_diri + 'khunjerab/khunjerab_'+STRTRIM(year[iy],2)+'.txt' )
   nalt = READ_WAPDA_CLIMATE( wapda_diri + 'naltar/naltar_'+STRTRIM(year[iy],2)+'.txt' )
   ziar = READ_WAPDA_CLIMATE( wapda_diri + 'ziarat/ziarat_'+STRTRIM(year[iy],2)+'.txt' )
   nday = N_ELEMENTS( khun )

   ;; Calculate lapse rates between Naltar and Ziarat, and Ziarat and Khunjerab
   lapse_zk = LAPSE_RATE( ziar.tavg, khun.tavg, $
                                 stations[1].elevation, stations[2].elevation  )
   lapse_nz = LAPSE_RATE( nalt.tavg, ziar.tavg, $
                                 stations[0].elevation, stations[1].elevation  )

   IF ( iy EQ 0 ) THEN BEGIN
      lapse_ziar_khun = lapse_zk
      lapse_nalt_ziar = lapse_nz
      month = khun.month
   ENDIF ELSE BEGIN
      lapse_ziar_khun = [lapse_ziar_khun, lapse_zk]
      lapse_nalt_ziar = [lapse_nalt_ziar, lapse_nz]
      month = [month, khun.month]
   ENDELSE

ENDFOR

;; Quick and dirty removal of bad data - to be checked
idx = WHERE( lapse_ziar_khun GT -20 AND lapse_ziar_khun LT 20, num )
IF ( num GT 0 ) THEN lapse_ziar_khun = lapse_ziar_khun[idx]
idx = WHERE( lapse_nalt_ziar GT -20 AND lapse_nalt_ziar LT 20, num )
IF ( num GT 0 ) THEN lapse_nalt_ziar = lapse_nalt_ziar[idx]

;;----------------------------------------------------------------------
;; Get ERA-Interim lapse rates
;;----------------------------------------------------------------------
erai_diri = '.'
erai_fili = 'era_interim.lapse_rate.hkh.station_locations.'+STRING(year[0],FORMAT='(i4)')+'.txt'
erai = READ_ERAI_LAPSE_FOR_STATION( erai_diri + '/' + erai_fili )

;;----------------------------------------------------------------------
;; Generate Plot frame work
;;----------------------------------------------------------------------
ymin = FLOOR( MIN( [lapse_ziar_khun,lapse_nalt_ziar] ) )
ymax = CEIL( MAX( [lapse_ziar_khun,lapse_nalt_ziar] ) )

A = FINDGEN(31) * (!PI*2/30.)
USERSYM, COS(A), SIN(A), /FILL

PLOT, RANDOMU(seed,11), RANDOMU(seed,11), $
      XRANGE=[0.5,12.5], XSTYLE=5, XTITLE='Month', $
      YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Lapse rate (!Uo!NC/km)', $
      /NODATA, FONT=0
FOR im=0, 10 DO PLOTS, im+1.5, !Y.CRANGE
PLOTS, !X.CRANGE, 6.5, LINE=2, THICK=2
PLOTS, !X.CRANGE, 0.
AXIS, XAXIS=0, XTICKS=12, XTICKV=INDGEN(12)+1, $
      XTICKNAME='!C'+['J','F','M','A','M','J','J','A','S','O','N','D'], $
      XTICKLEN=0.0002, FONT=0
AXIS, XAXIS=1, XTICKS=12, XTICKV=INDGEN(12)+1, $
      XTICKNAME=REPLICATE(' ',12), $
      XTICKLEN=0.0002

;;----------------------------------------------------------------------
;; Get monthly distribution of lapse rates
;;----------------------------------------------------------------------

lapse_zk_dist = MAKE_ARRAY( 8, 12, /FLOAT )
FOR im=0, 11 DO BEGIN
   this_month = im+1
   ismonth = WHERE( month EQ this_month, num_ismonth )
   tmp = lapse_ziar_khun[ismonth]
   BOXW_PLOT, this_month-0.4, tmp, WIDTH=0.07, /FILL, COLOR=FSC_COLOR('red'), $
              THICK=3, /ADD_MEAN, PSYM=8
   tmp = lapse_nalt_ziar[ismonth]
   BOXW_PLOT, this_month-0.2, tmp, WIDTH=0.07, /FILL, COLOR=FSC_COLOR('blue'), $
              THICK=3, /ADD_MEAN, PSYM=8

   ;; Era-Interim data
   ismonth = WHERE( erai.month EQ this_month, num_ismonth )
   tmp = erai[ismonth].gamma0
   ;; Khunjerab
   BOXW_PLOT, this_month-0.0, tmp[0,*], WIDTH=0.07, /FILL, COLOR=FSC_COLOR('firebrick'), $
              THICK=3, /ADD_MEAN, PSYM=8
   ;; Naltar
   BOXW_PLOT, this_month+0.2, tmp[1,*], WIDTH=0.07, /FILL, COLOR=FSC_COLOR('light coral'), $
              THICK=3, /ADD_MEAN, PSYM=8
   ;; Ziarat
   BOXW_PLOT, this_month+0.4, tmp[2,*], WIDTH=0.07, /FILL, COLOR=FSC_COLOR('olive'), $
              THICK=3, /ADD_MEAN, PSYM=8
   
ENDFOR

XYOUTS, 0.15, 0.90, 'Ziarat and Khunjerab', COLOR=FSC_COLOR('red'), FONT=0, /NORM
XYOUTS, 0.15, 0.85, 'Naltar and Ziarat', COLOR=FSC_COLOR('blue'), FONT=0, /NORM

XYOUTS, 0.15, 0.3, 'ERA-Interim', FONT=0, /NORM
XYOUTS, 0.15, 0.25, 'Khunjerab', COLOR=FSC_COLOR('firebrick'), FONT=0, /NORM
XYOUTS, 0.15, 0.2, 'Naltar', COLOR=FSC_COLOR('light coral'), FONT=0, /NORM
XYOUTS, 0.15, 0.15, 'Ziarat', COLOR=FSC_COLOR('olive'), FONT=0, /NORM

END
