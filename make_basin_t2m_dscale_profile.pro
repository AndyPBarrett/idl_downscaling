FUNCTION GET_2TILES, type, basin

  udiri = '/projects/CHARIS'
  xdim = 2400
  ydim = 2400

  CASE STRUPCASE(basin) OF
     'INDUS': BEGIN
        tile = ['h23v05','h24v05']
        basin_str = 'IN_UpperIndus_at_Besham_'
     END
     'HUNZA': BEGIN
        tile = ['h23v05','h24v05']
        basin_str = 'IN_Hunza_at_Danyour_'
     END
     ELSE: BEGIN
        PRINT, '% GET_2TILES: Unknown BASIN'
        RETURN, -1
     END
  ENDCASE

  CASE type OF
     'lat': BEGIN
        file = udiri + '/ancillary/modis_geolocation/' + tile + '.lat.2400x2400x1.float.dat'
     END
     'lon': BEGIN
        file = udiri + '/ancillary/modis_geolocation/' + tile + '.lon.2400x2400x1.float.dat'
     END
     'mask': BEGIN
        file = udiri + '/basins/basin_MODIS_tiles/' + tile + '/'+basin_str + tile+'.tif'
     END
     'elev': BEGIN
        file = udiri + '/elevation_data/SRTM_esri/' + 'sin_' + tile + '.tif'
     END
     ELSE: BEGIN
        PRINT, '% GET_2TILES: Unknown TYPE'
        RETURN, -1
     END
  ENDCASE

  IF ( type EQ 'lat' OR type EQ 'lon' ) THEN BEGIN
     grid0 = OPEN_IMAGE( file[0], xdim, ydim, 4 )
     grid1 = OPEN_IMAGE( file[1], xdim, ydim, 4 )
  ENDIF ELSE BEGIN
     grid0 = READ_TIFF( file[0] )
     grid1 = READ_TIFF( file[1] )
  ENDELSE

  grid = MAKE_ARRAY( 2*xdim, ydim, /FLOAT )
  
  grid[0:xdim-1,*] = grid0
  grid[xdim:(2*xdim)-1,*] = grid1

  RETURN, grid

END
     
FUNCTION GET_T2M_GRID, date

  xdim = 2400
  ydim = 2400

  tile = ['h23v05','h24v05']

  year = STRMID( date, 0, 4 )
  diri = RAID_PATH('arctic6') + '/ERA_Interim/t2m/' + year

  file = diri + '/t2m_'+tile+'_day_by_lapse_'+date+'.bin'

  grid0 = OPEN_IMAGE( file[0], xdim, ydim, 4 )
  grid1 = OPEN_IMAGE( file[1], xdim, ydim, 4 )

  grid = MAKE_ARRAY( 2*xdim, ydim, /FLOAT )
  
  grid[0:xdim-1,*] = grid0
  grid[xdim:(2*xdim)-1,*] = grid1

  RETURN, grid

END

FUNCTION DATE_SERIES, year

  jday = TIMEGEN( START=JULDAY(1,1,year), FINAL=JULDAY(12,31,year), UNITS='Day' )
  result = STRING( jday, FORMAT='(C(CYI04,CMOI02,CDI02))' )

  RETURN, result

END

FUNCTION DATEINT, year

  jday = TIMEGEN( START=JULDAY(1,1,year), FINAL=JULDAY(12,31,year), UNITS='Day' )
  CALDAT, jday, month, day, yeari

  doy = jday - JULDAY(1,1,year) + 1

  ndate = N_ELEMENTS(jday)

  result = MAKE_ARRAY( 4, ndate, /INTEGER )
  result[0,*] = yeari
  result[1,*] = month
  result[2,*] = day
  result[3,*] = doy

  RETURN, result

END

;;----------------------------------------------------------------------
;; MAIN ROUTINE
;;----------------------------------------------------------------------
PRO MAKE_BASIN_T2M_DSCALE_PROFILE, basin, year

;year = 2001
;basin = 'Hunza'

;;----------------------------------------------------------------------
;; Get geolocation, elevation and mask grids
;;----------------------------------------------------------------------

;; Get geolocation tiles and stitch together
lat = GET_2TILES( 'lat', basin )
lon = GET_2TILES( 'lon', basin )
mask = GET_2TILES( 'mask', basin )
elev = GET_2TILES( 'elev', basin )

;;----------------------------------------------------------------------
;; Find indices for elevation bands in mask area
;;----------------------------------------------------------------------

hwidth = 100
notbasin = WHERE( mask NE 1, numnotbasin )
elev[ notbasin ] = !VALUES.F_NAN
hmin = FLOOR( MIN( elev, /NAN ) / hwidth ) * hwidth

h = HISTOGRAM( elev, MIN=hmin, BINSIZE=hwidth, REVERSE_INDICES=ri, LOCATION=location, /NAN )
nh = N_ELEMENTS(h)

;;----------------------------------------------------------------------
;; Loop through dates to get temperature series for grid points
;;----------------------------------------------------------------------
date = DATE_SERIES( year )
ndate = N_ELEMENTS( date )

tempAvg = MAKE_ARRAY( nh, ndate, /FLOAT, VALUE=-999.99 )
tempMin = MAKE_ARRAY( nh, ndate, /FLOAT, VALUE=-999.99 )
tempMax = MAKE_ARRAY( nh, ndate, /FLOAT, VALUE=-999.99 )
tempStd = MAKE_ARRAY( nh, ndate, /FLOAT, VALUE=-999.99 )

FOR id = 0, ndate-1 DO BEGIN

   PRINT, '% Processing temperatures for ' + date[id]
   tair = GET_T2M_GRID( date[id] )

   FOR ih=0, nh-1 DO BEGIN
      IF ( ri[ih] NE ri[ih+1] ) THEN BEGIN
         tempAvg[ ih, id ] = MEAN( tair[ ri[ ri[ih]:ri[ih+1]-1 ] ] )
         tempMin[ ih, id ] = MIN( tair[ ri[ ri[ih]:ri[ih+1]-1 ] ] )
         tempMax[ ih, id ] = MAX( tair[ ri[ ri[ih]:ri[ih+1]-1 ] ] )
         tempStd[ ih, id ] = STDDEV( tair[ ri[ ri[ih]:ri[ih+1]-1 ] ] )
      ENDIF
   ENDFOR

ENDFOR

;;----------------------------------------------------------------------
;; Write to a file
;;----------------------------------------------------------------------
datei = DATEINT( year )

filo = STRLOWCASE(basin)+'_era_interim_t2m_profile_100m.'+STRTRIM(year,2)+'.dscale.asc'
OPENW, U, filo, /GET_LUN
PRINTF, U, nh
PRINTF, U, location+50, FORMAT='('+STRTRIM(nh,2)+'(1x,i4))'
FOR id=0, ndate-1 DO BEGIN
   PRINTF, U, datei[*,id], tempAvg[*,id], FORMAT='(i4,2(1x,i2),1x,i3,'+STRTRIM(nh,2)+'(1x,f6.2))'
ENDFOR
CLOSE, U
FREE_LUN, U

END

