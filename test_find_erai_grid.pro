
station = READ_CEOP_METADATA( '/home/apbarret/data/CEOP_Himalaya/ceop_himalaya_metadata.txt', NREC=nmod ) 

;; Get ERA-Interim coordinates
diri = '/disks/arctic6_raid/ERA_Interim/t2m/2000'
fili = 'era_interim.t2m.day_mean.charis.2000.nc'
elat = NCDF_READV( diri + '/' + fili, VARNAME='lat' )
elat = REVERSE( elat ) ; to monotonic increasing
elon = NCDF_READV( diri + '/' + fili, VARNAME='lon' )
nlat = N_ELEMENTS( elat )
nlon = N_ELEMENTS( elon )
elat2D = REBIN( TRANSPOSE( elat ), nlon, nlat )
elon2D = REBIN( elon, nlon, nlat )

ipt = MAKE_ARRAY( 4, nmod, /LONG )
FOR imod=0, nmod-1 DO BEGIN
   ilat = VALUE_LOCATE( elat, station[imod].lat )
   ilon = VALUE_LOCATE( elon, station[imod].lon )
   ipt[*,imod]  = ( (ilat+[0,1,0,1]) * nlon ) + ilon+[0,0,1,1]
ENDFOR

latmin = 26
latmax = 31
lonmin = 80
lonmax = 90
cntlat = MEAN( [latmin,latmax] )
cntlon = MEAN( [lonmin,lonmax] )
MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, $
         LIMIT=[latmin, lonmin, latmax, lonmax]
MAP_CONTINENTS, /COUNTRIES, /HIRES
MAP_GRID, /box, latdel=1., londel=1.
OPLOT, elon2D, elat2D, PSYM=2, COLOR=FSC_COLOR('red')
PLOTS, station.lon, station.lat, PSYM=2, COLOR=FSC_COLOR('green')
PLOTS, elon2D[ ipt[*,0] ], elat2D[ ipt[*,0] ], PSYM=2, COLOR=FSC_COLOR('blue')

END
