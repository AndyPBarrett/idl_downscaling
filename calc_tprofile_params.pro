
FUNCTION CENTRAL_DIFF, X, Y

  nx = N_ELEMENTS(x)

  result = ( SHIFT( y, 1 ) - SHIFT( y, -1 ) ) / $
           ( SHIFT( x, 1 ) - SHIFT( x, -1 ) )

  ;; calculate one-sided difference for end-points
  result[0]    = ( y[1] - y[0] ) / ( x[1] - x[0] ) ;!VALUES.F_NAN
  result[nx-1] = ( y[nx-1] - y[nx-2] ) / ( x[nx-1] - x[nx-2] ) ;!VALUES.F_NAN

  RETURN, result

END

FUNCTION FIND_INVERSION, dydx, ISINV=isinv, VERBOSE=VERBOSE

  maxindex = 2

  n = N_ELEMENTS( dydx )

  index = LINDGEN( n )

  lpsgn = dydx GT 0

  ;; using index avoids returning positive lapse rates at top of column
  idinv = WHERE( lpsgn GT 0 AND SHIFT(lpsgn,-1) GT 0 AND index GT maxindex, numinv )
     
  isinv = numinv GT 0

  IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN BEGIN
     check = REPLICATE( ' ', n )
     IF ( isinv GT 0 ) THEN check[idinv[0]] = '*'
     FOR i=0, n-1 DO PRINT, i, 1000./dydx[i], lpsgn[i], check[i], $
                            FORMAT='(i2,1x,f6.2,1x,i1,1x,a1)'
     IF ( isinv LE 0 ) THEN PRINT, '% FIND_INVERSION: No inversion found'
  ENDIF
  
  RETURN, idinv[0]

END

FUNCTION GET_RMSE, yhat, y

  error = yhat - y
  se = error^2
  mse = MEAN( se )
  rmse = SQRT( mse )

  RETURN, rmse

END

PRO PLOT_FIELD, image

  IMSIZE, image, x0, y0, xs, ys

  tmp = image

  idx = WHERE( tmp LE -9999., num )
  IF ( num GT 0 ) THEN tmp[idx] = !VALUES.F_NAN

  tmp = BYTSCL( tmp, TOP=240, /NAN )+14
  tmp[idx] = !VALUES.F_NAN

  TV, CONGRID( tmp, xs, ys ), x0, y0

  RETURN

END

PRO CHECK_FIELDS, field
  
  dims = SIZE( field, /DIMENSIONS )
  ntim = dims[2]

  FOR i=0, ntim-1 DO BEGIN

     PRINT, 'it='+STRTRIM(i,2)

     PLOT_FIELD, field[*,*,i]

     WAIT, 1

  ENDFOR

  RETURN

END

PRO WRITE_TO_NCDF, isinv, zinv, gamma0, tref0, gamma1, tref1, lat, lon, time, filo

  dims = SIZE( isinv, /DIMENSIONS )
  nlon = dims[0]
  nlat = dims[1]
  ntim = dims[2]

  cdfid = NCDF_CREATE( filo, /CLOBBER )
  
  latdimid = NCDF_DIMDEF( cdfid, 'lat', nlat )
  londimid = NCDF_DIMDEF( cdfid, 'lon', nlon )
  timdimid = NCDF_DIMDEF( cdfid, 'time', ntim )

  latid = NCDF_VARDEF( cdfid, 'lat', latdimid, /FLOAT )
  lonid = NCDF_VARDEF( cdfid, 'lon', londimid, /FLOAT )
  timid = NCDF_VARDEF( cdfid, 'time', timdimid, /FLOAT )

  g0id = NCDF_VARDEF( cdfid, 'gamma0', [londimid,latdimid,timdimid], /FLOAT )
  t0id = NCDF_VARDEF( cdfid, 'tref0', [londimid,latdimid,timdimid], /FLOAT )
  g1id = NCDF_VARDEF( cdfid, 'gamma1', [londimid,latdimid,timdimid], /FLOAT )
  t1id = NCDF_VARDEF( cdfid, 'tref1', [londimid,latdimid,timdimid], /FLOAT )
  ziid = NCDF_VARDEF( cdfid, 'zinv', [londimid,latdimid,timdimid], /FLOAT )
  isid = NCDF_VARDEF( cdfid, 'isinv', [londimid,latdimid,timdimid], /BYTE )

  NCDF_ATTPUT, cdfid, latid, 'long_name', 'latitude'
  NCDF_ATTPUT, cdfid, latid, 'units', 'degrees_north'

  NCDF_ATTPUT, cdfid, lonid, 'long_name', 'longitude'
  NCDF_ATTPUT, cdfid, lonid, 'units', 'degrees_east'

  NCDF_ATTPUT, cdfid, timid, 'long_name', 'time'
  NCDF_ATTPUT, cdfid, timid, 'units', 'hours since 1800-01-01 00:00'

  NCDF_ATTPUT, cdfid, g0id, 'long_name', 'upper lapse rate'
  NCDF_ATTPUT, cdfid, g0id, 'units', 'K/m'

  NCDF_ATTPUT, cdfid, t0id, 'long_name', 'upper reference temperature at 0 m'
  NCDF_ATTPUT, cdfid, t0id, 'units', 'K'

  NCDF_ATTPUT, cdfid, g1id, 'long_name', 'lower lapse rate'
  NCDF_ATTPUT, cdfid, g1id, 'units', 'K/m'

  NCDF_ATTPUT, cdfid, t1id, 'long_name', 'lower reference temperature at 0 m'
  NCDF_ATTPUT, cdfid, t1id, 'units', 'K'

  NCDF_ATTPUT, cdfid, ziid, 'long_name', 'height of inversion'
  NCDF_ATTPUT, cdfid, ziid, 'units', 'm'

  NCDF_ATTPUT, cdfid, isid, 'long_name', 'Inversion present'
  NCDF_ATTPUT, cdfid, isid, 'units', '0 - no inversion, 1 inversion'

  NCDF_ATTPUT, cdfid, 'description', $
               'lapse rate interpolation parameters for HKH ERA-Interim grid', /GLOBAL
  NCDF_ATTPUT, cdfid, 'creation_date', SYSTIME(), /GLOBAL
  NCDF_ATTPUT, cdfid, 'created_by', 'A.P.Barrett apbarret@nsidc.org', /GLOBAL
  
  NCDF_CONTROL, cdfid, /ENDEF

  NCDF_VARPUT, cdfid, latid, lat
  NCDF_VARPUT, cdfid, lonid, lon
  NCDF_VARPUT, cdfid, timid, time

  NCDF_VARPUT, cdfid, g0id, gamma0
  NCDF_VARPUT, cdfid, t0id, tref0
  NCDF_VARPUT, cdfid, g1id, gamma1
  NCDF_VARPUT, cdfid, t1id, tref1
  NCDF_VARPUT, cdfid, ziid, zinv
  NCDF_VARPUT, cdfid, isid, isinv

  NCDF_CLOSE, cdfid

  RETURN

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------
PRO CALC_TPROFILE_PARAMS, date
 
start = SYSTIME( /JULIAN )

DEBUG = 0
VERBOSE = 0
PLOT_FIELD = 0

;date = '200806'
filo = 'era_interim.lapserate.hkh.'+date+'.nc'

f = 'era_interim.t.hkh.'+date+'.nc'
t = NCDF_READV( f, VARNAME='t' )
lat = NCDF_READV( f, VARNAME='lat' )
lon = NCDF_READV( f, VARNAME='lon' )
time = NCDF_READV( f, VARNAME='time' )

f = 'era_interim.z.hkh.'+date+'.nc'
z = NCDF_READV( f, VARNAME='zf' )


dims = SIZE( t, /DIMENSION )
ncol = dims[0]
nrow = dims[1]
nlev = dims[2]
ntim = dims[3]

it = 118

;; Define result arrays
isinv  = MAKE_ARRAY( ncol, nrow, ntim, /BYTE, VALUE=0 )
gamma0 = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT, VALUE=-9999.99 )
gamma1 = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT, VALUE=-9999.99 )
tref0  = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT, VALUE=-9999.99 )
tref1  = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT, VALUE=-9999.99 )
zinv   = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT, VALUE=-9999.99 )
rmse   = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT, VALUE=-9999.99 )

FOR it=0, ntim-1 DO BEGIN

   PRINT, 'Processing time = ' + STRTRIM(it,2)

   FOR ic=0, ncol-1 DO BEGIN

      FOR ir=0, nrow-1 DO BEGIN

         x = REFORM( t[ic,ir,*,it] )
         y = REFORM( z[ic,ir,*,it] )

         ;; Locate inversion if one exists - must have 2 consecutive +ve gradients
         dydx = CENTRAL_DIFF( x, y )
         thisone = FIND_INVERSION( dydx, ISINV=ilog, VERBOSE=VERBOSE )
         isinv[ic,ir,it] = ilog
         
         IF ( isinv[ic,ir,it] GT 0 AND thisone NE nlev-1 ) THEN BEGIN
            
            g0 = REGRESS( y[0:thisone-1], x[0:thisone-1], YFIT=x0, CONST=t0 )
            g1 = REGRESS( y[thisone:nlev-1], x[thisone:nlev-1], YFIT=x1, CONST=t1 )
            
            ;; Save to arrays
            gamma0[ic,ir,it] = g0[0]
            gamma1[ic,ir,it] = g1[0]
            tref0[ic,ir,it]  = t0
            tref1[ic,ir,it]  = t1
         
            tp0 = ( g0[0] * y ) + t0
            tp1 = ( g1[0] * y ) + t1
            
            rmse[ic,ir,it] = GET_RMSE( [REFORM(x0),REFORM(x1)], x )
            
            IF ( gamma1[ic,ir,it] GT gamma0[ic,ir,it] ) THEN BEGIN
               zinv[ic,ir,it] = ( tref0[ic,ir,it] - tref1[ic,ir,it] ) / $
                                ( gamma1[ic,ir,it] - gamma0[ic,ir,it] )
            ENDIF ELSE BEGIN
               zinv[ic,ir,it] = y[thisone]
            ENDELSE
            
            ;; If both lapse rates are negative and the difference between zinv
            ;; and observed inversion level is greater than 500 m then set zinv
            ;; to observed
            IF ( ( gamma0[ic,ir,it] LT 0 AND gamma1[ic,ir,it] LT 0 ) AND $
                 ( ABS( zinv[ic,ir,it] - y[thisone] ) GT 500. ) ) THEN BEGIN
               zinv[ic,ir,it] = y[thisone]
            ENDIF
            
         ENDIF ELSE BEGIN
            
            g0 = REGRESS( y, x, YFIT=x0, CONST=t0 )
            gamma0[ic,ir,it] = g0[0]
            tref0[ic,ir,it]  = t0
            
            tp0 = ( g0[0] * y ) + t0
            
            rmse[ic,ir,it] = GET_RMSE( x0, x )
            
         ENDELSE
         
         IF ( VERBOSE EQ 1 ) THEN BEGIN
            
            gs0 = gamma0[ic,ir,it] GT -9999. ? gamma0[ic,ir,it]*1000. : gamma0[ic,ir,it]
            gs1 = gamma1[ic,ir,it] GT -9999. ? gamma1[ic,ir,it]*1000. : gamma1[ic,ir,it]
            
            PRINT, ic, ir, FORMAT='("Col: ",i3," Row: ",i3)'
            PRINT, gs0, tref0[ic,ir,it], FORMAT='("Gamma0: ",f8.2," Tref0: ",f8.2)'
            PRINT, gs1, tref1[ic,ir,it], FORMAT='("Gamma1: ",f8.2," Tref1: ",f8.2)'
            IF ( isinv[ic,ir,it] GT 0 ) THEN BEGIN
               PRINT, zinv[ic,ir,it], y[thisone], zinv[ic,ir,it]-y[thisone], $
                      FORMAT='("Zinv: ",f8.2," Zobs: ",f8.2," Diff: ",f8.2)'
            ENDIF ELSE BEGIN
               PRINT, zinv[ic,ir,it], FORMAT='("Zinv: ",f8.2)'
            ENDELSE
            PRINT, rmse[ic,ir,it], FORMAT='("RMSE: ", f8.2)'
            PRINT, ' '
            
         ENDIF
         
;;**********************************************************************
;; Plotting for Debugging
;;**********************************************************************
         IF ( DEBUG EQ 1 ) THEN BEGIN
            
            ;; Plot lapse rate
            minx = FLOOR( MIN(x)/10. ) * 10.
            maxx = CEIL( MAX(x)/10. ) * 10.
            miny = 0
            maxy = CEIL(MAX(y)/1000.) * 1000.
            
            PLOT, x, y, $
                  XRANGE=[minx,maxx], XSTYLE=1, $
                  YRANGE=[miny,maxy], YSTYLE=1, $
                  PSYM=-2, THICK=2
            PLOTS, 273.15, !Y.CRANGE, LINE=1
            
            IF ( isinv[ic,ir,it] GT 0 ) THEN BEGIN
               IF ( zinv[ic,ir,it] GT -9999. ) THEN PLOTS, !X.CRANGE, zinv[ic,ir,it], LINE=2, THICK=1.5
               PLOTS, x[thisone], y[thisone], PSYM=1, COLOR=FSC_COLOR('red'), SYMSIZE=2, THICK=2
               PLOTS, tp0, y, COLOR=FSC_COLOR( 'orange' )
               PLOTS, tp1, y, COLOR=FSC_COLOR( 'cyan' )
            ENDIF ELSE BEGIN
               PLOTS, tp0, y, COLOR=FSC_COLOR( 'orange' )
            ENDELSE
            
            IF ( isinv[ic,ir,it] GT 0 AND (zinv[ic,ir,it]-y[thisone]) GT 200. ) THEN STOP
            
            IF ( ( gamma1[ic,ir,it] LT gamma0[ic,ir,it] ) AND isinv[ic,ir,it] GT 0 ) THEN BEGIN
               STOP
            ENDIF ELSE BEGIN
               WAIT, 1
            ENDELSE
            
         ENDIF
;;*************** END OF DEBUG CODE *********************************
         
      ENDFOR
      
   ENDFOR
   
ENDFOR

;;*************************** FOR DEBUGGING *****************************
IF ( PLOT_FIELD EQ 1 ) THEN BEGIN

   LOADCT, 39
   PLOT_FIELD, gamma0
   STOP
   PLOT_FIELD, gamma1
   STOP
   PLOT_FIELD, tref0
   STOP
   PLOT_FIELD, tref1
   STOP
   PLOT_FIELD, zinv 
   STOP

ENDIF

;;----------------------------------------------------------------------
;; Write data to netcdf file
;;----------------------------------------------------------------------

PRINT, 'Writing data to ' + filo
WRITE_TO_NCDF, isinv, zinv, gamma0, tref0, gamma1, tref1, lat, lon, time, filo

finish = SYSTIME( /JULIAN )
runtime = finish - start
runtime_sec = runtime * 86400./60.
PRINT, runtime_sec, FORMAT='("Runtime: ",f10.1," minutes")'

END
