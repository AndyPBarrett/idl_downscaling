FUNCTION READ_ERAI_STATION, fili

  struct = {year:  0, $
            month: 0, $
            day:   0, $
            doy:   0, $
            khunjerab: -9999.99, $
            naltar:    -9999.99, $
            ziarat:    -9999.99, $
            gilgit:    -9999.99, $
            islamabad: -9999.99 }
  data = REPLICATE( struct, 366 )

  OPENR, U, fili, /GET_LUN

  line = ''
  ir = 0
  WHILE ( NOT EOF(U) ) DO BEGIN

     READF, U, line

     IF ( STREGEX( line, '^#', /BOOLEAN ) EQ 0 ) THEN BEGIN

        field = STRSPLIT( line, /EXTRACT )

        data[ir].year      = FIX( field[0] )
        data[ir].month     = FIX( field[1] )
        data[ir].day       = FIX( field[2] )
        data[ir].doy       = FIX( field[3] )
        data[ir].khunjerab = FLOAT( field[4] )
        data[ir].naltar    = FLOAT( field[5] )
        data[ir].ziarat    = FLOAT( field[6] )
        data[ir].gilgit    = FLOAT( field[7] )
        data[ir].islamabad = FLOAT( field[8] )

        ir = ir + 1
        
     ENDIF

  ENDWHILE

  CLOSE, U
  FREE_LUN, U

  nrec = ir

  data = data[0:nrec-1]

  RETURN, data

END
