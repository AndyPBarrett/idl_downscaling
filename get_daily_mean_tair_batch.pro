;; Makes daily means of downscaled tair files

diri = '/projects/CHARIS/forcing_data/Downscaled/ERA-Interim/day/uncorrected/v0'

ybeg = 2011
yend = 2014

tile = ['h23v05','h24v05','h25v06']
ntile = N_ELEMENTS( tile )

;ntile = 1
;FOR it=0, ntile-1 DO BEGIN

;   FOR iy=ybeg, yend DO BEGIN

;      GET_DAILY_MEAN_TAIR, iy, tile[it], DIRI=diri

;   ENDFOR

;ENDFOR

FOR iy=2002, 2014 DO BEGIN

   GET_INDUS_SITES_TAIR_DSCALE, iy, /VERBOSE

ENDFOR

END

