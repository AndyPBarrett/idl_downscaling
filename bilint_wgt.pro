FUNCTION BILINT_WGT, x, y, x1, x2, y1, y2

  w11 = (x2 - x) * (y2 - y) / ( (x2-x1) * (y2-y1) )
  w21 = (x - x1) * (y2 - y) / ( (x2-x1) * (y2-y1) )
  w12 = (x2 - x) * (y - y1) / ( (x2-x1) * (y2-y1) )
  w22 = (x - x1) * (y - y1) / ( (x2-x1) * (y2-y1) )

  w = [ w11, w21, w12, w22 ]

  RETURN, w

END
