;;----------------------------------------------------------------------
;; Calculates temperature lapse rates based on WAPDA climate stations in Hunza
;; basin.
;;
;; 2014-03-06 A.P.Barrett
;;----------------------------------------------------------------------
FUNCTION LAPSE_RATE, t0, t1, h0, h1

  idx = WHERE( t0 GT -9998 AND t1 GT -9998, num )
  IF ( num LE 0 ) THEN RETURN, -9999.99

  tdiff = t0[idx] - t1[idx]
  hdiff = h0 - h1
  lapse = (1000.) * tdiff / hdiff

  RETURN, lapse

END

;;----------------------------------------------------------------------
;; Dummy structure holding station info
;; To be replaced by reader from file
;;----------------------------------------------------------------------

stations = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0 ), $
             CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0 ), $
             CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0 ) ]
;; Make sure stations are ordered by elevation
stations = stations[ SORT( stations.elevation ) ]
nstat = N_ELEMENTS( stations )

;;----------------------------------------------------------------------
;; Get station data
;;----------------------------------------------------------------------
wapda_diri = '/projects/CHARIS/streamflow/Pakistan/' + $
             'Data_from_DHashmi/from_danial/Data_for_Andrew_Barnett/climate/'
year = [1995,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2009]
nyear = N_ELEMENTS( year )
FOR iy=0, nyear-1 DO BEGIN
   khun = READ_WAPDA_CLIMATE( wapda_diri + 'khunjerab/khunjerab_'+STRTRIM(year[iy],2)+'.qc1.txt' )
   nalt = READ_WAPDA_CLIMATE( wapda_diri + 'naltar/naltar_'+STRTRIM(year[iy],2)+'.qc1.txt' )
   ziar = READ_WAPDA_CLIMATE( wapda_diri + 'ziarat/ziarat_'+STRTRIM(year[iy],2)+'.qc1.txt' )
   nday = N_ELEMENTS( khun )

   ;; Calculate lapse rates between Naltar and Ziarat, and Ziarat and Khunjerab
   lapse_zk = LAPSE_RATE( ziar.tavg, khun.tavg, $
                                 stations[1].elevation, stations[2].elevation  )
   lapse_nz = LAPSE_RATE( nalt.tavg, ziar.tavg, $
                                 stations[0].elevation, stations[1].elevation  )

   IF ( iy EQ 0 ) THEN BEGIN
      lapse_ziar_khun = lapse_zk
      lapse_nalt_ziar = lapse_nz
      month = khun.month
   ENDIF ELSE BEGIN
      lapse_ziar_khun = [lapse_ziar_khun, lapse_zk]
      lapse_nalt_ziar = [lapse_nalt_ziar, lapse_nz]
      month = [month, khun.month]
   ENDELSE

ENDFOR

;; Quick and dirty removal of bad data - to be checked
idx = WHERE( lapse_ziar_khun GT -20 AND lapse_ziar_khun LT 20, num )
IF ( num GT 0 ) THEN lapse_ziar_khun = lapse_ziar_khun[idx]
idx = WHERE( lapse_nalt_ziar GT -20 AND lapse_nalt_ziar LT 20, num )
IF ( num GT 0 ) THEN lapse_nalt_ziar = lapse_nalt_ziar[idx]

;;----------------------------------------------------------------------
;; Generate Plot frame work
;;----------------------------------------------------------------------
ymin = -12.  ;FLOOR( MIN( [lapse_ziar_khun,lapse_nalt_ziar] ) )
ymax = 2.0   ;CEIL( MAX( [lapse_ziar_khun,lapse_nalt_ziar] ) )

A = FINDGEN(31) * (!PI*2/30.)
USERSYM, COS(A), SIN(A), /FILL

PLOT, RANDOMU(seed,11), RANDOMU(seed,11), $
      XRANGE=[0.5,12.5], XSTYLE=5, XTITLE='Month', $
      YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Temperature Gradient (K km!U-1!N)', $
      /NODATA, FONT=0, TITLE='Hunza Temperature Gradients'
;FOR im=0, 10 DO PLOTS, im+1.5, !Y.CRANGE
PLOTS, !X.CRANGE, 6.5, LINE=2, THICK=2
PLOTS, !X.CRANGE, 0.
AXIS, XAXIS=0, XTICKS=12, XTICKV=INDGEN(12)+1, $
      XTICKNAME='!C!C' + ['Jan','Feb','Mar','Apr','May','Jun', $
                          'Jul','Aug','Sep','Oct','Nov','Dec'], $
      XTICKLEN=0.0002, FONT=0
AXIS, XAXIS=1, XTICKS=12, XTICKV=INDGEN(12)+1, $
      XTICKNAME=REPLICATE(' ',12), $
      XTICKLEN=0.0002

;;----------------------------------------------------------------------
;; Get monthly distribution of lapse rates
;;----------------------------------------------------------------------

lapse_zk_dist = MAKE_ARRAY( 8, 12, /FLOAT )
FOR im=0, 11 DO BEGIN
   this_month = im+1
   ismonth = WHERE( month EQ this_month, num_ismonth )
   tmp = lapse_ziar_khun[ismonth]
   BOXW_PLOT, this_month-0.12, tmp, WIDTH=0.09, /FILL, COLOR=FSC_COLOR('coral'), $
              THICK=3, /ADD_MEAN, PSYM=8, /NO_OUTLIERS
   tmp = lapse_nalt_ziar[ismonth]
   BOXW_PLOT, this_month+0.12, tmp, WIDTH=0.09, /FILL, COLOR=FSC_COLOR('cyan'), $
              THICK=3, /ADD_MEAN, PSYM=8, /NO_OUTLIERS
ENDFOR

XYOUTS, 0.15, 0.90, 'Naltar (2810 m) and Ziarat (3669 m)', COLOR=FSC_COLOR('cyan'), $
        FONT=0, /NORM
XYOUTS, 0.15, 0.85, 'Ziarat (3669 m) and Khunjerab (4730 m)', COLOR=FSC_COLOR('coral'), $
        FONT=0, /NORM



END
