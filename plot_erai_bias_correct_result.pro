;;----------------------------------------------------------------------
;; generates scatter plots showing bias correction
;;
;; 2015-06-19 A.P.Barrett
;;----------------------------------------------------------------------
PRO READ_BIAS_CORRECTION, fili, bias, station

  stn_struct = {name: '', elev: 0.0, lat: 0.0, lon: 0.0 }
  nstat = 3
  station = REPLICATE( stn_struct, 3 )

  bias_struct = {doy: 0, value: MAKE_ARRAY( nstat, /FLOAT ) }
  nday = 365
  bias = REPLICATE( bias_struct, nday )

  OPENR, U, fili, /GET_LUN

  k = 0
  is = 0
  line = ''
  WHILE ( NOT EOF(U) ) DO BEGIN

     READF, U, line

     IF ( STREGEX( line, '^#', /BOOLEAN ) EQ 1 ) THEN BEGIN

        field = STRSPLIT( line, /EXTRACT )

        station[is].name = STRTRIM( field[1], 2 )
        station[is].elev = FLOAT( field[2] )
        station[is].lat  = FLOAT( field[3] )
        station[is].lon  = FLOAT( field[4] )

        is = is + 1

     ENDIF ELSE BEGIN

        field = STRSPLIT( line, /EXTRACT )

        bias[k].doy   = FIX(field[0])
        bias[k].value = FLOAT(field[1:3])

        k = k + 1

     ENDELSE

  ENDWHILE

  RETURN

END

PRO PLOT_PANEL, x, y, TITLE=title

  error = x - y
  rmse = SQRT( MEAN( error * error, /NAN ) )

  xmin = FLOOR( MIN( [ REFORM(x), REFORM(y) ] ) / 5. ) * 5.
  xmax = CEIL( MAX( [ REFORM(x), REFORM(y) ] ) / 5. ) * 5.

  PLOT, x, y, $
        XRANGE=[xmin,xmax], XSTYLE=1, XTITLE='Observed (K)', $
        YRANGE=[xmin,xmax], YSTYLE=1, YTITLE='Estimated (K)', $
        PSYM=2, /NODATA, FONT=0, /ISOTROPIC
  PLOTS, !X.CRANGE, !Y.CRANGE
  OPLOT, x, y, PSYM=2

  x0 = !X.CRANGE[0] + ( 0.1 * (!X.CRANGE[1]-!X.CRANGE[0]) )
  y0 = !Y.CRANGE[0] + ( 0.9 * (!Y.CRANGE[1]-!Y.CRANGE[0]) )
  XYOUTS, x0, y0, STRING( rmse, FORMAT='("RMSE=",f6.2)'), FONT=0, /DATA

  RETURN

END

      

;;----------------------------------------------------------------------
;; MAIN ROUTINE
;;----------------------------------------------------------------------

erai_diri = '/disks/arctic6_raid/ERA_Interim/daily'

;; Get biases
bias_diri = '/disks/arctic6_raid/ERA_Interim/daily'
bias_fili = 'bias_erai_hunza_tair.txt'
READ_BIAS_CORRECTION, bias_diri+'/'+bias_fili, bias, station

ybeg = 2001
yend = 2009

FOR iy=ybeg, yend DO BEGIN

   ;; Get erai data
   erai_fili = 'downscaled_tair_hunza_validation_'+STRTRIM(iy,2)+'.txt'
   erai = READ_DOWNSCALE_VALIDATION( erai_diri + '/' + erai_fili, station, NREC=nrec )

   ;; Get Observations for plotting
   obs_diri = '/projects/CHARIS/surface_met_data/WAPDA'
   obs_fili = obs_diri + '/' + 'khunjerab' + '/' + 'khunjerab_' + STRTRIM(iy,2) +'.qc1.fill.txt'
   khun = READ_CHARIS_CLIMATE( obs_fili )
   idx = WHERE( khun.tavg LE -9998., num )
   IF ( num GT 0 ) THEN khun[idx].tavg = !VALUES.F_NAN

   obs_fili = obs_diri + '/' + 'naltar' + '/' + 'naltar_' + STRTRIM(iy,2) +'.qc1.fill.txt'
   nalt = READ_CHARIS_CLIMATE( obs_fili )
   idx = WHERE( nalt.tavg LE -9998., num )
   IF ( num GT 0 ) THEN nalt[idx].tavg = !VALUES.F_NAN

   obs_fili = obs_diri + '/' + 'ziarat' + '/' + 'ziarat_' + STRTRIM(iy,2) +'.qc1.fill.txt'
   ziar = READ_CHARIS_CLIMATE( obs_fili )
   idx = WHERE( ziar.tavg LE -9998., num )
   IF ( num GT 0 ) THEN ziar[idx].tavg = !VALUES.F_NAN

   erai_corr = MAKE_ARRAY( 3, nrec, /FLOAT )
   erai_xval = MAKE_ARRAY( 3, nrec, /FLOAT )

   IF ( nrec EQ 366 ) THEN BEGIN
      erai_corr[*,0:364] = erai[0:364].tair[[1,2,0]] + bias.value
      erai_corr[*,365]   = erai[365].tair[[1,2,0]] + bias[364,*].value
   ENDIF ELSE BEGIN
      erai_corr = erai.tair[[1,2,0]] + bias.value
   ENDELSE

   IF ( nrec EQ 366 ) THEN BEGIN
      erai_xval[0,0:364] = erai[0:364].tair[1] + MEAN(bias.value[[1,2]],DIM=1 )
      erai_xval[0,365]   = erai[365].tair[1] + MEAN(bias[364,*].value[[1,2]], DIM=1 )

      erai_xval[1,0:364] = erai[0:364].tair[2] + MEAN(bias.value[[0,2]],DIM=1 )
      erai_xval[1,365]   = erai[365].tair[2] + MEAN(bias[364,*].value[[0,2]], DIM=1 )

      erai_xval[2,0:364] = erai[0:364].tair[0] + MEAN(bias.value[[0,1]],DIM=1 )
      erai_xval[2,365]   = erai[365].tair[0] + MEAN(bias[364,*].value[[0,1]], DIM=1 )


   ENDIF ELSE BEGIN

      erai_xval[0,*] = erai.tair[1] + MEAN(bias.value[[1,2]],DIM=1 )
      erai_xval[1,*] = erai.tair[2] + MEAN(bias.value[[0,2]],DIM=1 )
      erai_xval[2,*] = erai.tair[0] + MEAN(bias.value[[0,1]],DIM=1 )

   ENDELSE

   !P.MULTI = [0,3,3]
   PLOT_PANEL, khun.tavg+273.15, erai.tair[0,*], TITLE='Khunjerab - raw' 
   PLOT_PANEL, khun.tavg+273.15, erai_corr[2,*], TITLE='Khunjerab - corr' 
   PLOT_PANEL, khun.tavg+273.15, erai_xval[2,*], TITLE='Khunjerab - xval' 

   PLOT_PANEL, ziar.tavg+273.15, erai.tair[2,*], TITLE='Ziarat - raw' 
   PLOT_PANEL, ziar.tavg+273.15, erai_corr[1,*], TITLE='Ziarat - corr' 
   PLOT_PANEL, ziar.tavg+273.15, erai_xval[1,*], TITLE='Ziarat - xval' 

   PLOT_PANEL, nalt.tavg+273.15, erai.tair[1,*], TITLE='Naltar - raw' 
   PLOT_PANEL, nalt.tavg+273.15, erai_corr[0,*], TITLE='Naltar - corr' 
   PLOT_PANEL, nalt.tavg+273.15, erai_xval[0,*], TITLE='Naltar - xval' 

   STOP

ENDFOR

END
