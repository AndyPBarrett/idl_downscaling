;;----------------------------------------------------------------------
;; Plots temperature data fields from AphroTemp
;;
;; 2014-11-21 A.P.Barrett 
;;----------------------------------------------------------------------
PRO PLOT_APHROTEMP_STATIONS, image, lat, lon, TMIN=TMIN, TMAX=TMAX, $
                             TINT=TINT, _EXTRA=EXTRA_KEYWORDS

  tmin = N_ELEMENTS( tmin ) EQ 0 ?   0. : tmin
  tmax = N_ELEMENTS( tmax ) EQ 0 ? 100. : tmax
  tint = N_ELEMENTS( tint ) EQ 0 ?  10. : tint
  
  lat0 =  19.75
  lat1 =  50.25
  lon0 =  60.25
  lon1 = 110.25

  MAP_SET, MEAN( [lat0,lat1] ), MEAN( [lon0,lon1] ), /MERCATOR, $
           LIMIT=[lat0,lon0,lat1,lon1], /ISOTROPIC, _EXTRA=EXTRA_KEYWORDS

  lat2d = REBIN( TRANSPOSE(lat), N_ELEMENTS(lon), N_ELEMENTS(lat), /SAMPLE )
  lon2d = REBIN(  lon, N_ELEMENTS(lon), N_ELEMENTS(lat), /SAMPLE )

  timage = image
  iszero = WHERE( timage LE 0.0, numzero )
  IF ( numzero GT 0 ) THEN timage[ iszero ] = !VALUES.F_NAN

  h = HISTOGRAM( timage, MIN=tmin, MAX=tmax, BIN=tint, REVERSE_INDICES=ri, /NAN, $
                 LOCATIONS=location )
  nh = N_ELEMENTS( h )
  
  COLORS = BYTSCL( INDGEN(nh+1) )
  
  cimage = MAKE_ARRAY( SIZE(image, /DIMENSIONS ), /BYTE, VALUE=0 )
  FOR ih=0, nh-1 DO BEGIN
     IF ( ri[ih] NE ri[ih+1] ) THEN BEGIN
        cimage[ ri[ ri[ih]:ri[ih+1]-1 ] ] = colors[ih]
     ENDIF
  ENDFOR
;  idx = WHERE( image LT tmin, num )
;  IF ( num GT 0 ) THEN cimage[idx] = colors[0]
  idx = WHERE( image GT tmax, num )
  IF ( num GT 0 ) THEN cimage[idx] = colors[nh]
  
  ni = N_ELEMENTS( image )

  dx = (lon[1]-lon[0])*0.5
  dy = (lat[1]-lat[0])*0.5

  FOR ii=0, ni-1 DO BEGIN

     IF ( cimage[ii] GT 0 ) THEN BEGIN

        ;PRINT, ii, cimage[ii]
        ;STOP

        xx = lon2d[ii]+( dx * [-1.,-1.,1.,1.,-1.] )
        yy = lat2d[ii]+( dy * [-1.,1.,1.,-1.,-1.] )

        POLYFILL, xx, yy, COLOR=cimage[ii]
        
     ENDIF

  ENDFOR

  MAP_CONTINENTS, /HIRES, /COUNTRIES
  MAP_CONTINENTS, /HIRES
  MAP_GRID, /BOX

  labels = [-1,location]
  COLORBAR, 0.1, 0.9, 0.1, 0.12, labels, colors, /NORM, EVERY=1, UNIT='%' 

  RETURN

END
