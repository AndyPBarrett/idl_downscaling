
PRO ADD_COLORBAR, X0, X1, Y0, Y1, COLORS, LABELS, EVERY=EVERY, UNITS=UNITS

  every = N_ELEMENTS( every ) EQ 0 ? 1 : every

  ncolor = N_ELEMENTS( COLORS )
  
  dx = (x1 - x0)/FLOAT(ncolor)
  dy = y1 - y0

  FOR ic=0, ncolor-1 DO BEGIN
     xx = x0+(dx*ic)+(dx*[0.,0.,1.,1.,0.])
     yy = y0+(dy*[0.,1.,1.,0.,0.])
     POLYFILL, xx, yy, COLOR=colors[ic], /NORM
     PLOTS, xx, yy, /NORM
  ENDFOR

  nlabel = N_ELEMENTS( labels )
  FOR il=0, nlabel-1 DO BEGIN
     IF (( il MOD every ) EQ 1) THEN BEGIN
        XYOUTS, x0+(dx*il), y0-0.02, labels[il], /NORM, FONT=0, ALIGNMENT=0.5, CHARSIZE=0.8
     ENDIF
  ENDFOR
  IF ( N_ELEMENTS( UNITS ) GT 0 ) THEN XYOUTS, MEAN([x0,x1]), y0-0.05, units, /NORM, FONT=0, ALIGNMENT=0.5  

  RETURN

END


;; Get data
erai_dir = '/disks/arctic6_raid/ERA_Interim/t2m/'
erai_fil = 'seasonal_aphrodite_minus_erai_bias.nc'
erai = NCDF_READV( erai_dir + '/' + erai_fil, VARNAME='t2m' )
erai_lat = NCDF_READV( erai_dir + '/' + erai_fil, VARNAME='lat' )
erai_lon = NCDF_READV( erai_dir + '/' + erai_fil, VARNAME='lon' )

merra_dir = '/disks/arctic6_raid/MERRA/daily/T2M'
merra_fil = 'seasonal_aphrodite_minus_merra_bias.nc'
merra = NCDF_READV( merra_dir + '/' + merra_fil, VARNAME='t2m' )
merra_lat = NCDF_READV( merra_dir + '/' + merra_fil, VARNAME='lat' )
merra_lon = NCDF_READV( merra_dir + '/' + merra_fil, VARNAME='lon' )

cfsr_dir = '/disks/arctic6_raid/CFSR/T2M'
cfsr_fil = 'seasonal_aphrodite_minus_cfsr_bias.nc'
cfsr = NCDF_READV( cfsr_dir + '/' + cfsr_fil, VARNAME='t2m' )
cfsr_lat = NCDF_READV( cfsr_dir + '/' + cfsr_fil, VARNAME='lat' )
cfsr_lon = NCDF_READV( cfsr_dir + '/' + cfsr_fil, VARNAME='lon' )

erai = erai*(-1)
cfsr = cfsr*(-1.)
merra = merra*(-1.)

;PRINT, MIN( [MIN(erai,/NAN),MIN(merra,/NAN),MIN(cfsr,/NAN)] ), MAX( [MAX(erai,/NAN),MAX(merra,/NAN),MAX(cfsr,/NAN)] )

;!P.MULTI=[0,1,4]
tmin = -5.
tmax =  5.
tint =  0.5 

LOAD_NCLCOLOR, 'cmp_b2r'

pxmin = 0.05
pxmax = 1.00
pymin = 0.1
pymax = 0.95

dx = (pxmax - pxmin)/3.
dy = (pymax - pymin)/4.

PLOT_APHROTEMP, erai[*,*,0], erai_lat, erai_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin, pymin+(3*dy), pxmin+(1*dx), pymin+(4*dy) ]
PLOT_APHROTEMP, erai[*,*,1], erai_lat, erai_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin, pymin+(2*dy), pxmin+(1*dx), pymin+(3*dy) ], /ADVANCE
PLOT_APHROTEMP, erai[*,*,2], erai_lat, erai_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin, pymin+(1*dy), pxmin+(1*dx), pymin+(2*dy) ], /ADVANCE
PLOT_APHROTEMP, erai[*,*,3], erai_lat, erai_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin, pymin+(0*dy), pxmin+(1*dx), pymin+(1*dy) ], /ADVANCE

PLOT_APHROTEMP, merra[*,*,0], merra_lat, merra_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(1*dx), pymin+(3*dy), pxmin+(2*dx), pymin+(4*dy) ], /ADVANCE
PLOT_APHROTEMP, merra[*,*,1], merra_lat, merra_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(1*dx), pymin+(2*dy), pxmin+(2*dx), pymin+(3*dy) ], /ADVANCE
PLOT_APHROTEMP, merra[*,*,2], merra_lat, merra_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(1*dx), pymin+(1*dy), pxmin+(2*dx), pymin+(2*dy) ], /ADVANCE
PLOT_APHROTEMP, merra[*,*,3], merra_lat, merra_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(1*dx), pymin+(0*dy), pxmin+(2*dx), pymin+(1*dy) ], /ADVANCE

PLOT_APHROTEMP, cfsr[*,*,0], cfsr_lat, cfsr_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(2*dx), pymin+(3*dy), pxmin+(3*dx), pymin+(4*dy) ], /ADVANCE
PLOT_APHROTEMP, cfsr[*,*,1], cfsr_lat, cfsr_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(2*dx), pymin+(2*dy), pxmin+(3*dx), pymin+(3*dy) ], /ADVANCE
PLOT_APHROTEMP, cfsr[*,*,2], cfsr_lat, cfsr_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(2*dx), pymin+(1*dy), pxmin+(3*dx), pymin+(2*dy) ], /ADVANCE
PLOT_APHROTEMP, cfsr[*,*,3], cfsr_lat, cfsr_lon, TMIN=tmin, TMAX=tmax, TINT=tint, $
                POSITION=[ pxmin+(2*dx), pymin+(0*dy), pxmin+(3*dx), pymin+(1*dy) ], $
                COLORS=colors, LEVELS=levels, /ADVANCE

XYOUTS, pxmin+(0.5*dx), 0.96, 'ERA-Interim', FONT=0, ALIGNMENT=0.5, /NORM
XYOUTS, pxmin+(1.5*dx), 0.96, 'MERRA', FONT=0, ALIGNMENT=0.5, /NORM
XYOUTS, pxmin+(2.5*dx), 0.96, 'CFSR', FONT=0, ALIGNMENT=0.5, /NORM

XYOUTS, 0.035, pymin+(3.5*dy), 'DJF', FONT=0, ALIGNMENT=0.5, /NORM, ORIENTATION=90.
XYOUTS, 0.035, pymin+(2.5*dy), 'MAM', FONT=0, ALIGNMENT=0.5, /NORM, ORIENTATION=90.
XYOUTS, 0.035, pymin+(1.5*dy), 'JJA', FONT=0, ALIGNMENT=0.5, /NORM, ORIENTATION=90.
XYOUTS, 0.035, pymin+(0.5*dy), 'SON', FONT=0, ALIGNMENT=0.5, /NORM, ORIENTATION=90.

labels = [' ',STRING(levels[1:N_ELEMENTS(levels)-2],FORMAT='(f4.1)'),' ']

ADD_COLORBAR, 0.2, 0.8, 0.05, 0.075, colors, labels, EVERY=2, UNITS='K'

END

