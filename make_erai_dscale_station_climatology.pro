;; Generate climatologies for the stations based on downscaled ERA-Interim temperatures
;;
;; 2015-06-17 A.P.Barrett
;;-----------------------------------------------------------------------------------------
ybeg = 2001
yend = 2009

fili = 'downscaled_tair_hunza_validation_'+STRTRIM(ybeg,2)+'.txt'
data = READ_DOWNSCALE_VALIDATION( fili, station, NREC=nrec )

FOR iy=ybeg+1, yend DO BEGIN
	fili = 'downscaled_tair_hunza_validation_'+STRTRIM(iy,2)+'.txt'
	data = [ data, READ_DOWNSCALE_VALIDATION( fili, station, NREC=nrec ) ]
ENDFOR
notvalid = WHERE( FINITE(data.tair) EQ 0, num_notvalid )
IF ( num_notvalid GT 0 ) THEN data.tair[notvalid] = !VALUES.F_NAN
ndata = N_ELEMENTS( data )

;; Get climatology
clim = MAKE_ARRAY( 5, 365, /FLOAT )
climvar = MAKE_ARRAY( 5, 365, /FLOAT )
FOR id=0, 364 DO BEGIN

	doy = id+1
	idx = WHERE( data.doy EQ doy, num )
	ibeg = idx-15
	iend = idx+15

	ii = WHERE( ibeg LT 0, num )
	IF ( num GT 0 ) THEN ibeg[ii] = 0

	ie = WHERE( iend GT ndata-1, num )
	IF ( num GT 0 ) THEN iend[ie] = ndata-1

	ni = N_ELEMENTS(ibeg)
	x0 = []
	x1 = []
	x2 = []
	x3 = []
	x4 = []
	FOR i=0, ni-1 DO BEGIN
		x0 = [ x0, REFORM( data[ibeg[i]:iend[i]].tair[0] ) ]
		x1 = [ x1, REFORM( data[ibeg[i]:iend[i]].tair[1] ) ]
		x2 = [ x2, REFORM( data[ibeg[i]:iend[i]].tair[2] ) ]
		x3 = [ x3, REFORM( data[ibeg[i]:iend[i]].tair[3] ) ]
		x4 = [ x4, REFORM( data[ibeg[i]:iend[i]].tair[4] ) ]
	ENDFOR

	clim[0,id] = MEAN( x0, /NAN )
	clim[1,id] = MEAN( x1, /NAN )
	clim[2,id] = MEAN( x2, /NAN )
	clim[3,id] = MEAN( x3, /NAN )
	clim[4,id] = MEAN( x4, /NAN )

	climvar[0,id] = STDDEV( x0, /NAN )
	climvar[1,id] = STDDEV( x1, /NAN )
	climvar[2,id] = STDDEV( x2, /NAN )
	climvar[3,id] = STDDEV( x3, /NAN )
	climvar[4,id] = STDDEV( x4, /NAN )

ENDFOR

OPENW, U, 'downscaled_tair_hunza_validation_mean.txt', /GET_LUN
FOR id = 0, 364 DO PRINTF, U, id+1, clim[*,id], climvar[*,id], FORMAT='(i3,10(1x,f6.2))'
CLOSE, U
FREE_LUN, U

;PSON, FILE='downscaled_tair_hunza_stations.eps'
;DEVICE, /ENCAPSULATED
;DEVICE, XSIZE=15., YSIZE=9.

!P.MULTI=[0,1,3]
PLOT, clim[0,*], MIN_VALUE=-9998., YRANGE=[250.,285.], YSTYLE=1
PLOT, clim[1,*], MIN_VALUE=-9998., YRANGE=[260.,290.], YSTYLE=1
PLOT, clim[2,*], MIN_VALUE=-9998., YRANGE=[250.,285.], YSTYLE=1
!P.MULTI=0

;PSOFF

END

