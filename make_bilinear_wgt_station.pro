;;----------------------------------------------------------------------
;; Finds z(x,y) using bilinear interpolation.
;;
;; x - x-coord to be estimated
;; y - y-coord to be estimated
;; z - 4-element array [ z(x1,y1), z(x2,y1), z(x1,y2), z(x2,y2) ]
;; x1,x2 - x-coords
;; y1,y2 - y-coords
;;----------------------------------------------------------------------
DEBUG=1

;; Get station coordinates
station = READ_CEOP_METADATA( '/home/apbarret/data/CEOP_Himalaya/ceop_himalaya_metadata.txt', NREC=nmod ) 

;; Get ERA-Interim coordinates
diri = '/disks/arctic6_raid/ERA_Interim/t2m/2000'
fili = 'era_interim.t2m.day_mean.charis.2000.nc'
elat = NCDF_READV( diri + '/' + fili, VARNAME='lat' )
elat = REVERSE( elat ) ; to monotonic increasing
elon = NCDF_READV( diri + '/' + fili, VARNAME='lon' )
nlat = N_ELEMENTS( elat )
nlon = N_ELEMENTS( elon )
elat2D = REBIN( TRANSPOSE( elat ), nlon, nlat )
elon2D = REBIN( elon, nlon, nlat )

;; For testing plot MODIS and ERA-Interim points in Hunza
minlat = 26. ;FLOOR( MIN( mlat ) )
maxlat = 31. ;CEIL( MAX( mlat ) )
minlon = 80. ;FLOOR( MIN( mlon ) )
maxlon = 90. ;CEIL( MAX( mlon ) )
cntlat = MEAN( [ minlat, maxlat ] )
cntlon = MEAN( [ minlon, maxlon ] )


;; Define result arrays
q11 = MAKE_ARRAY( nmod, /LONG )
q21 = MAKE_ARRAY( nmod, /LONG )
q12 = MAKE_ARRAY( nmod, /LONG )
q22 = MAKE_ARRAY( nmod, /LONG )
w11 = MAKE_ARRAY( nmod, /FLOAT )
w21 = MAKE_ARRAY( nmod, /FLOAT )
w12 = MAKE_ARRAY( nmod, /FLOAT )
w22 = MAKE_ARRAY( nmod, /FLOAT )

;nmod = 100
FOR imod=0, nmod-1 DO BEGIN

;; Find indices of 4 corner cells in ERA-Interim grid
   ilat = VALUE_LOCATE( elat, station[imod].lat )
   ilon = VALUE_LOCATE( elon, station[imod].lon )
   ipt  = ( (ilat+[0,1,0,1]) * nlon ) + ilon+[0,0,1,1]

   weight = BILINT_WGT( station[imod].lon, station[imod].lat, $
                        elon[ilon], elon[ilon+1], $
                        elat[ilat], elat[ilat+1] )

   PRINT, imod
   PRINT, ipt
   print, weight

   q11[ imod ] = ipt[0]
   q21[ imod ] = ipt[1]
   q12[ imod ] = ipt[2]
   q22[ imod ] = ipt[3]

   w11[ imod ] = weight[0]
   w21[ imod ] = weight[1]
   w12[ imod ] = weight[2]
   w22[ imod ] = weight[3]

   IF ( DEBUG EQ 1 ) THEN BEGIN

;      PRINT, ii
      PRINT, station[imod].lon, station[imod].lat
      PRINT, elon[ilon], elat[ilat]
      PRINT, weight
   
      MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, $
               LIMIT=[minlat, minlon, maxlat, maxlon]
      MAP_CONTINENTS, /COUNTRIES, /HIRES
      MAP_GRID, /box, latdel=1., londel=1.
      OPLOT, elon2D, elat2D, PSYM=2, COLOR=FSC_COLOR('red')
      PLOTS, station[imod].lon, station[imod].lat, PSYM=2, COLOR=FSC_COLOR('green')
      PLOTS, elon2D[ ipt ], elat2D[ ipt ], PSYM=2, COLOR=FSC_COLOR('blue')

      STOP

   ENDIF

ENDFOR

SAVE, station, q11, q21, q12, q22, w11, w21, w12, w22, $
      FILE='bilinear_wgt_ceop_station_vector.sav'
 
;; Calculate weights


;; Write to file


END
