
f = 'era_interim.t.hkh.200806.nc'
t = NCDF_READV( f, VARNAME='t' )

f = 'era_interim.z.hkh.200806.nc'
z = NCDF_READV( f, VARNAME='zf' )


dims = SIZE( t, /DIMENSION )
ncol = dims[0]
nrow = dims[1]
nlev = dims[2]
ntim = dims[3]

minz = 0
maxz = CEIL(MAX(z)/1000.) * 1000.
;STOP

ic = 0
ir = 0
;FOR ic = 0, ncol-1 DO BEGIN

;   FOR ir=0, nrow - 1 DO BEGIN

      
      print, ic, ir
      PLOT, t[ic,ir,*,0], z[ic,ir,*,0], $
            XRANGE=[200, 310], XSTYLE=1, $
            YRANGE=[minz,maxz], YSTYLE=1, $
            PSYM=-2

;      WAIT,1

;   ENDFOR

;ENDFOR      

END
