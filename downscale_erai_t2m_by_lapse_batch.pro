;; Batch process to downscale ERA-Interim T2m to MODIS tiles

tile = ['h23v05','h24v05']
ntile = N_ELEMENTS( tile )

;year = [1995]
year = [1996,1997,1998,1999,2000,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013]
nyear = N_ELEMENTS(year)

FOR itil = 0, ntile-1 DO BEGIN

   FOR iyr=0, nyear-1 DO BEGIN

      DOWNSCALE_ERAI_T2M_BY_LAPSE, tile[itil], year[iyr]

   ENDFOR

ENDFOR

END
