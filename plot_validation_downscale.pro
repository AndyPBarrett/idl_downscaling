;;----------------------------------------------------------------------
;; Plots validation data for the 3 Hunza stations.
;;
;; 2015-06-18 A.P.Barrett
;;----------------------------------------------------------------------

FUNCTION GET_ERAI_VALIDATION_MEAN, fili

  tmp = MAKE_ARRAY( 11, 365, /FLOAT )

  OPENR, U, fili, /GET_LUN
  READF, U, tmp
  CLOSE, U
  FREE_LUN, U

  doy     = tmp[0,*]
  clim    = tmp[1:5,*]
  climvar = tmp[6:10,*]

  result = {doy: doy, clim: clim, climvar:climvar }

  RETURN, result

END

FUNCTION GET_INTERP_ESTIMATES, fili

  tmp = MAKE_ARRAY( 4, 365, /FLOAT )

  OPENR, U, fili, /GET_LUN
  READF, U, tmp
  CLOSE, U
  FREE_LUN, U

  doy     = tmp[0,*]
  clim    = tmp[1:3,*] + 273.15

  result = {doy: doy, clim: clim }

  RETURN, result

END

FUNCTION READ_OBSERVED_CLIMATOLOGY, fili

  
  tmp = MAKE_ARRAY( 4, 365, /FLOAT )

  OPENR, U, fili, /GET_LUN
  READF, U, tmp
  CLOSE, U
  FREE_LUN, U

  doy     = tmp[0,*]
  clim    = tmp[1:3,*] + 273.15

  result = {doy: doy, clim: clim }

  RETURN, result

END

PRO PLOT_PANEL, erai, obs_estimate, observation, TITLE=title, ADD_LEGEND=ADD_LEGEND

  time = FINDGEN(365)+1.

  xmin = 0
  xmax = 366
  ymin = FLOOR( MIN( [REFORM(erai), REFORM(obs_estimate), REFORM(observation)] ) / 5. ) * 5
  ymax = CEIL( MAX( [REFORM(erai), REFORM(obs_estimate), REFORM(observation)] ) / 5. ) * 5

  PLOT, time, erai, $
        XRANGE=[xmin,xmax], XSTYLE=1, XTITLE='Day of Year', $
        YRANGE=[ymin,ymax], YSTYLE=1, YTITLE='Temperature (!Uo!NC)', $
        TITLE='', /NODATA, FONT=0
  PLOTS, !X.CRANGE, 273.15

  OPLOT, time, observation, THICK=4
  OPLOT, time, obs_estimate, COLOR=FSC_COLOR('cyan'), THICK=4
  OPLOT, time, erai, COLOR=FSC_COLOR('red'), THICK=4

  x0 = !X.CRANGE[0] + ( 0.05 * (!X.CRANGE[1]-!X.CRANGE[0]) )
  y0 = !Y.CRANGE[0] + ( 0.83 * (!Y.CRANGE[1]-!Y.CRANGE[0]) )

  XYOUTS, x0, y0, title, FONT=0, CHARSIZE=1.7, /DATA

  IF ( KEYWORD_SET( ADD_LEGEND ) ) THEN BEGIN
     x0 = !X.CRANGE[0] + ( 0.03 * (!X.CRANGE[1]-!X.CRANGE[0]) )
     y0 = !Y.CRANGE[0] + ( 0.6 * (!Y.CRANGE[1]-!Y.CRANGE[0]) )
     dx = ( 0.05 * (!X.CRANGE[1]-!X.CRANGE[0]) )
     dy = ( 0.1 * (!Y.CRANGE[1]-!Y.CRANGE[0]) )
     PLOTS, x0+(dx*[0.,1.]), y0, THICK=4
     XYOUTS, x0+(dx*1.1), y0-(dy*0.4), 'Observed', FONT=0, CHARSIZE=1.1

     PLOTS, x0+(dx*[0.,1.]), y0-dy, THICK=4, COLOR=FSC_COLOR('red')
     XYOUTS, x0+(dx*1.1), y0-(dy*1.4), 'ERA-I', FONT=0, CHARSIZE=1.1

     PLOTS, x0+(dx*[0.,1.]), y0-(2*dy), THICK=4, COLOR=FSC_COLOR('cyan')
     XYOUTS, x0+(dx*1.1), y0-(dy*2.4), 'Lapse', FONT=0, CHARSIZE=1.1

  ENDIF

  RETURN

END
 
;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------

;;----------------------------------------------------------------------
;; Get ERA-Interim derived temperatures
;;----------------------------------------------------------------------

erai_diri = '/disks/arctic6_raid/ERA_Interim/daily'
erai_fili = 'downscaled_tair_hunza_validation_mean.txt'

erai = GET_ERAI_VALIDATION_MEAN( erai_diri + '/' + erai_fili )

;;----------------------------------------------------------------------
;; Get Station Observations
;;----------------------------------------------------------------------
obs_diri = '/projects/CHARIS/surface_met_data/WAPDA'

fili = obs_diri + '/' + 'khunjerab' + '/' + 'khunjerab_climatology_1995to2009.txt'
khun = READ_OBSERVED_CLIMATOLOGY( fili )

fili = obs_diri + '/' + 'naltar' + '/' + 'naltar_climatology_1995to2009.txt'
nalt = READ_OBSERVED_CLIMATOLOGY( fili )

fili = obs_diri + '/' + 'ziarat' + '/' + 'ziarat_climatology_1995to2009.txt'
ziar = READ_OBSERVED_CLIMATOLOGY( fili )

;;----------------------------------------------------------------------
;; Get station-derived estimates of temperatures
;;----------------------------------------------------------------------
est_diri = obs_diri
est_fili = 'interp_t2m_hunza_validation_climatology_1995to2009.txt'
interp= GET_INTERP_ESTIMATES( est_diri + '/' + est_fili )

khun_est = interp.clim[2,*]
nalt_est = interp.clim[0,*]
ziar_est = interp.clim[1,*]

;;----------------------------------------------------------------------
;; Make plot
;;----------------------------------------------------------------------
!P.MULTI=[0,1,3]

PLOT_PANEL, erai.clim[0,*], khun_est, khun.clim[0,*], TITLE='Khunjerab', /ADD_LEGEND
PLOT_PANEL, erai.clim[2,*], ziar_est, ziar.clim[0,*], TITLE='Ziarat'
PLOT_PANEL, erai.clim[1,*], nalt_est, nalt.clim[0,*], TITLE='Naltar'

!P.MULTI=0

END

 
