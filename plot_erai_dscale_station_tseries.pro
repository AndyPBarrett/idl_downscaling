;; Plots time series of downscaled ERA-Interim temperatures
;;
;; 2015-09-15 A.P.Barrett
;;-----------------------------------------------------------------------------------------
ybeg = 2002
yend = 2014

fili = 'downscaled_tair_hunza_validation_'+STRTRIM(ybeg,2)+'.txt'
data = READ_DOWNSCALE_VALIDATION( fili, station, NREC=nrec )

HELP, data, /struct

FOR iy=ybeg+1, yend DO BEGIN
	fili = 'downscaled_tair_hunza_validation_'+STRTRIM(iy,2)+'.txt'
	data = [ data, READ_DOWNSCALE_VALIDATION( fili, station, NREC=nrec ) ]
ENDFOR
notvalid = WHERE( FINITE(data.tair) EQ 0, num_notvalid )
IF ( num_notvalid GT 0 ) THEN data.tair[notvalid] = !VALUES.F_NAN
ndata = N_ELEMENTS( data )

time = JULDAY( data.month, data.day, data.year )

xmin = JULDAY( 1, 1, 2001 )
xmax = JULDAY( 12, 31, 2014 )
dummy = LABEL_DATE(DATE_FORMAT=['%Y'])

!P.MULTI=[0,1,5]
PLOT, time, data.tair[0], MIN_VALUE=-9998., YRANGE=[205.,315.], YSTYLE=1, $
      XRANGE=[xmin,xmax], XTICKUNITS = ['Time'], XMINOR=0, $
      XTICKFORMAT='LABEL_DATE', XSTYLE=1, XTICKS=13, YMARGIN=[6,2]
PLOT, time, data.tair[1], MIN_VALUE=-9998., YRANGE=[205.,315.], YSTYLE=1, $
      XRANGE=[xmin,xmax], XTICKUNITS = ['Time'], XMINOR=0, $
      XTICKFORMAT='LABEL_DATE', XSTYLE=1, XTICKS=13, YMARGIN=[6,2]
PLOT, time, data.tair[2], MIN_VALUE=-9998., YRANGE=[205.,315.], YSTYLE=1, $
      XRANGE=[xmin,xmax], XTICKUNITS = ['Time'], XMINOR=0, $
      XTICKFORMAT='LABEL_DATE', XSTYLE=1, XTICKS=13, YMARGIN=[6,2]
PLOT, time, data.tair[3], MIN_VALUE=-9998., YRANGE=[205.,315.], YSTYLE=1, $
      XRANGE=[xmin,xmax], XTICKUNITS = ['Time'], XMINOR=0, $
      XTICKFORMAT='LABEL_DATE', XSTYLE=1, XTICKS=13, YMARGIN=[6,2]
PLOT, time, data.tair[4], MIN_VALUE=-9998., YRANGE=[205.,315.], YSTYLE=1, $
      XRANGE=[xmin,xmax], XTICKUNITS = ['Time'], XMINOR=0, $
      XTICKFORMAT='LABEL_DATE', XSTYLE=1, XTICKS=13, YMARGIN=[6,2]
!P.MULTI=0

END
