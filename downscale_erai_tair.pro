
PRO MAKE_MAP, data, lon, lat, SCALE=SCALE, MINX=MINX, MAXX=MAXX, ADVANCE=ADVANCE

  scale = N_ELEMENTS( scale ) EQ 0 ? 1 : scale
  interval = [ 1., 2., 2.5, 5., 7.5, 10. ]

  minlat = 28.
  minlon = 67.
  maxlat = 42.
  maxlon = 94.

  cntlat = MEAN( [minlat,maxlat] )
  cntlon = MEAN( [minlon,maxlon] )

  image = data
  idx = WHERE( image LE -9999., num )
  IF ( num GT 0 ) THEN image[idx] = !VALUES.F_NAN
  image = image * scale

  minx  = N_ELEMENTS( minx ) EQ 0 ? FLOOR( MIN( image, /NAN ) ) : minx
  maxx  = N_ELEMENTS( maxx ) EQ 0 ? CEIL( MAX( image, /NAN ) ) : maxx
  range = maxx - minx
  nbin = 15
  width = 5.

  level = [200., (( FINDGEN( nbin ) * width ) + minx), 350. ]
  color = BYTSCL( INDGEN(nbin+2), TOP=240 ) + 14
  
;  print, minx, maxx
;  print, range, width
;  PRINT, level
;  PRINT, color

  LOADCT, 39

  MAP_SET, cntlat, cntlon, /LAMBERT, /ISOTROPIC, LIMIT=[minlat,minlon,maxlat,maxlon], $
           ADVANCE=ADVANCE
  CONTOUR, image, lon, lat, /OVERPLOT, /CELL_FILL, LEVEL=level, C_COLOR=color

  MAP_CONTINENTS, /COUNTRIES, /HIRES
  MAP_GRID, /box

  COLORBAR, 0.85, 0.9, 0.1, 0.9, level, color, /NORM

  RETURN

END

FUNCTION BILINT_WITH_WGT, ingrid, i0, i1, i2, i3, w0, w1, w2, w3

  tmp0 = ingrid[ i0 ]
  tmp1 = ingrid[ i1 ]
  tmp2 = ingrid[ i2 ]
  tmp3 = ingrid[ i3 ]

  avgData = ( tmp0 + tmp1 + tmp2 + tmp3 ) * 0.25

  idx = WHERE( FINITE( tmp0 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp0[ idx ] = avgData[ idx ]

  idx = WHERE( FINITE( tmp1 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp1[ idx ] = avgData[ idx ]

  idx = WHERE( FINITE( tmp2 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp2[ idx ] = avgData[ idx ]

  idx = WHERE( FINITE( tmp3 ) EQ 0, num )
  IF ( num GT 0 ) THEN tmp3[ idx ] = avgData[ idx ]

  outgrid = ( tmp0 * w0 ) + ( tmp1 * w1 ) + $
            ( tmp2 * w2 ) + ( tmp3 * w3 )
  
  RETURN, outgrid

END

;; Need to add time def
PRO WRITE_TO_NETCDF, result, lat2d, lon2d, filo

  HELP, result
  HELP, lat2d
  HELP, lon2d

  dims = SIZE( result, /DIMENSION )
  ncol = dims[0]
  nrow = dims[1]
  ntim = dims[2]

  cdfid = NCDF_CREATE( filo, /CLOBBER )

  xdimid = NCDF_DIMDEF( cdfid, 'col', ncol )
  ydimid = NCDF_DIMDEF( cdfid, 'row', nrow )
  tdimid = NCDF_DIMDEF( cdfid, 'time', ntim )

  xid   = NCDF_VARDEF( cdfid, 'col', [xdimid], /FLOAT )
  yid   = NCDF_VARDEF( cdfid, 'row', [ydimid], /FLOAT )
  tid   = NCDF_VARDEF( cdfid, 'time', [tdimid], /FLOAT )
  datid = NCDF_VARDEF( cdfid, 'tsurf', [xdimid,ydimid,tdimid], /FLOAT )
  latid = NCDF_VARDEF( cdfid, 'lat2d', [xdimid,ydimid], /FLOAT )
  lonid = NCDF_VARDEF( cdfid, 'lon2d', [xdimid,ydimid], /FLOAT )

  NCDF_ATTPUT, cdfid, xid, 'long_name', 'row'
  NCDF_ATTPUT, cdfid, xid, 'units', 'none'
  NCDF_ATTPUT, cdfid, yid, 'long_name', 'col'
  NCDF_ATTPUT, cdfid, yid, 'units', 'none'
  NCDF_ATTPUT, cdfid, tid, 'long_name', 'time'
  NCDF_ATTPUT, cdfid, tid, 'units', 'none - currently'

  NCDF_ATTPUT, cdfid, datid, 'long_name', '2m temperature'
  NCDF_ATTPUT, cdfid, datid, 'units', 'degrees Kelvin'
  NCDF_ATTPUT, cdfid, latid, 'long_name', 'latitude'
  NCDF_ATTPUT, cdfid, latid, 'units', 'degrees-north'
  NCDF_ATTPUT, cdfid, lonid, 'long_name', 'longitude'
  NCDF_ATTPUT, cdfid, lonid, 'units', 'degrees-east'

  NCDF_ATTPUT, cdfid, 'creation_date', SYSTIME(), /GLOBAL
  NCDF_ATTPUT, cdfid, 'created_by', 'A.P.Barrett apbarret@nsidc.org', /GLOBAL
  NCDF_ATTPUT, cdfid, 'description', '2m temperature downscaled from ERA-Interim reanalysis', /GLOBAL

  NCDF_CONTROL, cdfid, /ENDEF

  NCDF_VARPUT, cdfid, xid, FINDGEN(ncol)
  NCDF_VARPUT, cdfid, yid, FINDGEN(nrow)
  NCDF_VARPUT, cdfid, tid, FINDGEN(ntim)
  NCDF_VARPUT, cdfid, latid, lat2d
  NCDF_VARPUT, cdfid, lonid, lon2d
  NCDF_VARPUT, cdfid, datid, result

  NCDF_CLOSE, cdfid

  RETURN

END

PRO WRITE_TO_BINARY, data, filo
  
  OPENW, U, filo, /GET_LUN
  WRITEU, U, data
  CLOSE, U
  FREE_LUN, U

  RETURN

END

;;----------------------------------------------------------------------
;; Main routine
;;----------------------------------------------------------------------
PRO DOWNSCALE_ERAI_TAIR, tile, date, NO_INVERSION=NO_INVERSION, MAKE_IMAGE=MAKE_IMAGE, $
                         DIRO=DIRO

diro = ( N_ELEMENTS( diro ) GT 0 ) ? diro : '.'
diri = '/raid/ERA_Interim/daily'

srcfile = diri + '/' + 'era_interim.lapserate.hkh.'+date+'.nc'

latvar = 'lat'
lonvar = 'lon'

VERBOSE=1

;; Get MODIS geolocation
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting MODIS tile coordinates...'
mlat = GET_MODIS_GEOLOCATION_PT( tile, /LAT )
mlon = GET_MODIS_GEOLOCATION_PT( tile, /LON )
nmod = N_ELEMENTS( mlat )
dims = SIZE( mlat, /DIMENSION )
ncol = dims[0]
nrow = dims[1]

;; Get ERA-Interim coordinates
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Getting source grid coordinates...'
elat   = NCDF_READV( srcfile, VARNAME=latvar )
elon   = NCDF_READV( srcfile, VARNAME=lonvar )
tref0  = NCDF_READV( srcfile, VARNAME='tref0' )
gamma0 = NCDF_READV( srcfile, VARNAME='gamma0' )
tref1  = NCDF_READV( srcfile, VARNAME='tref1' )
gamma1 = NCDF_READV( srcfile, VARNAME='gamma1' )
zinv   = NCDF_READV( srcfile, VARNAME='zinv' )
isinv   = NCDF_READV( srcfile, VARNAME='isinv' )
nlon   = N_ELEMENTS( elon )
nlat   = N_ELEMENTS( elat )

;; Get MODIS elevation
direl = '/projects/CHARIS/elevation_data/SRTM_esri_obsolete/'
filel = 'sin_'+tile+'.tif'
elev = FLOAT( READ_TIFF( direl + '/' + filel ) )

;; Restore weights files
IF ( KEYWORD_SET( VERBOSE ) EQ 1 ) THEN PRINT, 'Restoring weights files...'
RESTORE, diri + '/' + 'bilinear_wgt_hkh_'+tile+'_055latx089lon.sav'

;; Newer runs contain q11 etc 
IF ( N_ELEMENTS( i0 ) EQ 0 ) THEN i0 = q11
IF ( N_ELEMENTS( i1 ) EQ 0 ) THEN i1 = q21
IF ( N_ELEMENTS( i2 ) EQ 0 ) THEN i2 = q12
IF ( N_ELEMENTS( i3 ) EQ 0 ) THEN i3 = q22
IF ( N_ELEMENTS( w0 ) EQ 0 ) THEN w0 = w11
IF ( N_ELEMENTS( w1 ) EQ 0 ) THEN w1 = w21
IF ( N_ELEMENTS( w2 ) EQ 0 ) THEN w2 = w12
IF ( N_ELEMENTS( w3 ) EQ 0 ) THEN w3 = w22

;;; Get MODIS cell indices file
;cell_index = OPEN_IMAGE( 'modis_era_gridcells_' + tile + '.long.bin', 2400, 2400, 3 )

;; Reverse elat to make it monotonic increasing
elat = REVERSE( elat )
;; ... and data
tref0  = REVERSE( tref0, 2 )
gamma0 = REVERSE( gamma0, 2 )
tref1  = REVERSE( tref1, 2 )
gamma1 = REVERSE( gamma1, 2 )
zinv   = REVERSE( zinv, 2 )
isinv  = REVERSE( isinv, 2 )

;;----------------------------------------------------------------------
;; Set parameters to missing
;;----------------------------------------------------------------------
ismissing = WHERE( tref0 LE -9999.99, nummissing )
IF ( nummissing GT 0 ) THEN tref0[ ismissing ] = !VALUES.F_NAN

ismissing = WHERE( tref1 LE -9999.99, nummissing )
IF ( nummissing GT 0 ) THEN tref1[ ismissing ] = !VALUES.F_NAN

ismissing = WHERE( gamma0 LE -9999.99, nummissing )
IF ( nummissing GT 0 ) THEN gamma0[ ismissing ] = !VALUES.F_NAN

ismissing = WHERE( gamma1 LE -9999.99, nummissing )
IF ( nummissing GT 0 ) THEN gamma1[ ismissing ] = !VALUES.F_NAN

;;----------------------------------------------------------------------
;; Make 2D lat and lon for era interim for plotting
;;----------------------------------------------------------------------
elat2d = REBIN( TRANSPOSE( elat ), nlon, nlat, /SAMPLE )
elon2d = REBIN( elon, nlon, nlat, /SAMPLE )

;;----------------------------------------------------------------------
;; get time dimension
dims = SIZE( tref0, /DIMENSIONS )
ntim = dims[2]

scale = 1.

result = MAKE_ARRAY( ncol, nrow, ntim, /FLOAT )

FOR it = 0, ntim-1 DO BEGIN

   ;; Regrid data
   tref0_m  = BILINT_WITH_WGT( tref0[*,*,it], i0, i1, i2, i3, w0, w1, w2, w3 )
   gamma0_m = BILINT_WITH_WGT( gamma0[*,*,it], i0, i1, i2, i3, w0, w1, w2, w3 )
   tref1_m  = BILINT_WITH_WGT( tref1[*,*,it], i0, i1, i2, i3, w0, w1, w2, w3 )
   gamma1_m = BILINT_WITH_WGT( gamma1[*,*,it], i0, i1, i2, i3, w0, w1, w2, w3 )
   zinv_m   = BILINT_WITH_WGT( zinv[*,*,it], i0, i1, i2, i3, w0, w1, w2, w3 )

   ;; Set MODIS cells within missing ERAI cells to missing
   ;; Still needs to be done

   ;; Calculate surface temperature for above inversion or no inversion cases
   tsurf = tref0_m + ( gamma0_m * elev )
   
   ;; Calculate surface temperature for below inversion
   IF ( NOT KEYWORD_SET( NO_INVERSION ) ) THEN BEGIN
;      STOP
      idx = WHERE( elev LT zinv_m AND isinv[*,*,it] EQ 1, num )
      IF ( num GT 0 ) THEN BEGIN
         tsurf[ idx ] = tref1_m[ idx ] + ( gamma1_m[ idx ] * elev[ idx ] )
      ENDIF
   ENDIF

   result[ *, *, it ] = tsurf

;;----------------------------------------------------------------------
;; Plot results
;;----------------------------------------------------------------------
   
   IF ( KEYWORD_SET( MAKE_IMAGE ) EQ 1 ) THEN BEGIN

      minx = 240.               ;FLOOR( MIN( [ MIN(tsurf), MIN(tsurf) ]*scale ) )
      maxx = 310.               ;CEIL( MAX( [ MAX(tsurf), MAX(tsurf) ]*scale ) )
      
      PRINT, it, MIN(tsurf)-273.15, MAX(tsurf)-273.15
      
      MAKE_MAP, tsurf, mlon, mlat, SCALE=scale, MINX=minx, MAXX=maxx
      
      SAVEIMAGE, diro + '/' + 'tsurf_'+tile+'_'+date+'_'+STRING(it,FORMAT='(i03)')+'.png', /PNG

      IF ( KEYWORD_SET( DEBUG ) EQ 1 ) THEN STOP

   ENDIF

ENDFOR

;;----------------------------------------------------------------------
;; Calculate daily means from 6h data
;;----------------------------------------------------------------------

;y = result[ 185, 1507, * ]
;ymin = FLOOR( MIN( y ) / 10. ) * 10.
;ymax = CEIL( MAX( y ) / 10. ) * 10.
;PLOT, y, MIN_VALUE=0.1, YRANGE=[ymin,ymax]

;;----------------------------------------------------------------------
;; Write to netCDF
;;----------------------------------------------------------------------

nday = ntim / 4
id = 0
FOR it = 0, ntim-1, 4 DO BEGIN

   id = id + 1

   datestr = date+STRING(id,FORMAT='(i02)')
   filo = diro + '/' + 'tsurf_'+tile+'_'+datestr
   IF ( KEYWORD_SET( NO_INVERSION ) ) THEN BEGIN
      filo = filo + '_noinversion' + '.bin'
   ENDIF ELSE BEGIN
      filo = filo + '.bin'
   ENDELSE

;   WRITE_TO_NETCDF, result[*,*,it:it+3], mlat, mlon, filo
   PRINT, '% Writing results to ' + filo
   WRITE_TO_BINARY, result[*,*,it:it+3], filo

ENDFOR

END

