;;----------------------------------------------------------------------
;; Uses a MLR approach to generate a 2 m temperature surface.
;;
;; 2015-06-15 A.P.Barrett
;;----------------------------------------------------------------------
PRO MODEL_XYZ, x, y, b, a, mae, rmse, sigmae, YFIT=yhat

  xdims = SIZE( x, /DIMENSIONS )
  p = xdims[0]
  n = xdims[1]

  b = REGRESS( x, y, CONST=a, YFIT=yhat )
  error = y - yhat

  mae = MEAN( ABS( error ) )
  rmse = SQRT( MEAN( error*error ) )

  IF ( (n-p) GT 0 ) THEN BEGIN
     sigmae = TOTAL( error * error ) / (n-p)
  ENDIF ELSE BEGIN
     sigmae = !VALUES.F_NAN
  ENDELSE

  RETURN

END

PRO GET_DATA, year_list, result, NTIME=ntime

  struct = {year: 0, month: 0, day: 0, doy: 0, tavg: MAKE_ARRAY( 3, /FLOAT, VALUE=-9999.99 ) }

  wapda_diri = '/projects/CHARIS/surface_met_data/WAPDA'

  nyear = N_ELEMENTS( year_list )

  ybeg = MIN( year_list )
  yend = MAX( year_list )

  time = TIMEGEN( START=JULDAY(1,1,ybeg), FINAL=JULDAY(12,31,yend), UNITS='Day' )
  CALDAT, time, month, day, year
  doy = time - JULDAY( 1, 1, year ) + 1

  ntime = N_ELEMENTS( time )
  result = REPLICATE( struct, ntime )

  result.year  = year
  result.month = month
  result.day   = day
  result.doy   = doy
    
  FOR iy=0, nyear-1 DO BEGIN

     khun = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'khunjerab/khunjerab_' + $
                                STRTRIM(year_list[iy],2) + '.qc1.fill.txt' )
     
     nalt = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'naltar/naltar_' + $
                                STRTRIM(year_list[iy],2) + '.qc1.fill.txt' )

     ziar = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'ziarat/ziarat_' + $
                                STRTRIM(year_list[iy],2) + '.qc1.fill.txt' )

     idx = WHERE( result.year EQ year_list[iy] AND result.doy EQ 1, num )
     result[idx[0]+khun.doy-1].tavg[0] = nalt.tavg
     result[idx[0]+khun.doy-1].tavg[1] = ziar.tavg
     result[idx[0]+khun.doy-1].tavg[2] = khun.tavg

  ENDFOR

  RETURN

END  

PRO PLOT_PANEL, x, y, _EXTRA=EXTRA_KEYWORDS

   b = REGRESS( x, y, CONST=a, YFIT=yhat )
   error = y - yhat
   mae  = MEAN( ABS( error ) )
   rmse = SQRT( MEAN( error * error ) )
   PLOT, x, y, PSYM=2, THICK=2, $
         _EXTRA=EXTRA_KEYWORDS
   ypr = a + ( !X.CRANGE * b[0] )
   PLOTS, !X.CRANGE, ypr, THICK=2, COLOR=FSC_COLOR('red')

   x0 = !X.CRANGE[0] + ( 0.6 * ( !X.CRANGE[1]-!X.CRANGE[0] ) )
   y0 = !Y.CRANGE[0] + ( 0.9 * ( !Y.CRANGE[1]-!Y.CRANGE[0] ) )
   dy = 0.05 * ( !Y.CRANGE[1]-!Y.CRANGE[0] )
   XYOUTS, x0, y0, STRING( b[0], FORMAT='("b = ",f8.2)' )
   XYOUTS, x0, y0-dy, STRING( mae, FORMAT='("MAE = ",f6.2)' )
   XYOUTS, x0, y0-(2*dy), STRING( rmse, FORMAT='("RMSE = ",f6.2)' )
     
END

;;----------------------------------------------------------------------
;; MAIN ROUTINE
;;----------------------------------------------------------------------

year_list = [1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2009]

;;----------------------------------------------------------------------
;; Dummy structure holding station info
;; To be replaced by reader from file
;;----------------------------------------------------------------------

stations = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0, $
                          'LATITUDE', 36.1667, 'LONGITUDE', 74.1833 ), $
             CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0, $
                          'LATITUDE', 36.7980, 'LONGITUDE', 74.4820  ), $
             CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0, $
                          'LATITUDE', 36.8411, 'LONGITUDE', 75.4192 ) ]
;; Make sure stations are ordered by elevation
stations = stations[ SORT( stations.elevation ) ]
nstat = N_ELEMENTS( stations )

;; Make array to hold coordinated (independent variables)
xx = MAKE_ARRAY( 3, nstat, /FLOAT )
xx1 = MAKE_ARRAY( 3, nstat, /FLOAT )
xx[0,*] = stations.elevation / 1000.

xx[1,*] = HAVERSINE( stations[0].latitude, 74.5, stations.latitude, 74.5 )
xx[2,*] = HAVERSINE( 36.5, stations[0].longitude, 36.5, stations.longitude )

;;----------------------------------------------------------------------
;; Get data for a given year
;;----------------------------------------------------------------------
GET_DATA, year_list, data, NTIME=ntime

isvalid = WHERE( data.tavg[0] GT -9999. AND $
                 data.tavg[1] GT -9999. AND $
                 data.tavg[2] GT -9999., num_valid )
;IF ( num_valid GT 0 ) THEN data = odata[isvalid]

ndata = N_ELEMENTS( data )

;;----------------------------------------------------------------------
;; Generate relationships between stations based on coordinates and elevation
;;----------------------------------------------------------------------

rstr = { year: 0, month: 0, day: 0, doy: 0, $
         b: MAKE_ARRAY(3, /FLOAT, VALUE=-9999.99), $
         a: -9999.99, $
         yhat: MAKE_ARRAY(3, /FLOAT, VALUE=-9999.99 ), $
         y: MAKE_ARRAY(3, /FLOAT, VALUE=-9999.99 ), $
         mae: 0.0, $
         rmse: 0.0 }
model = REPLICATE( rstr, ntime )

model.year  = data.year
model.month = data.month
model.day   = data.day
model.doy   = data.doy

;!P.MULTI=[0,3,1]

FOR id = 0, num_valid-1 DO BEGIN

   y = data[isvalid[id]].tavg

;   PLOT_PANEL, xx[0,*], y, XRANGE=[2.8,4.8], XSTYLE=1, XTITLE='Elevation (km)', $
;               TITLE=STRING( id+1, FORMAT='("Day = ",i3)' )

   MODEL_XYZ, xx[0,*], y, b, a, mae, rmse, sigma, YFIT=yfit
;   model[ isvalid[id] ].year = data[id].year
;   model[ isvalid[id] ].month = data[id].month
;   model[ isvalid[id] ].day = data[id].day
;   model[ isvalid[id] ].doy = data[id].doy
   model[ isvalid[id] ].b[0] = b[0]
   model[ isvalid[id] ].a = a
   model[ isvalid[id] ].mae = mae
   model[ isvalid[id] ].rmse = rmse
   model[ isvalid[id] ].yhat = REFORM(yfit)
   model[ isvalid[id] ].y = y

ENDFOR

;!P.MULTI=0

;;----------------------------------------------------------------------
;; Calculate long term mean estimated temperatures and gradient terms
;;----------------------------------------------------------------------
clim = MAKE_ARRAY( 3, 365, /FLOAT, VALUE=-9999.99 )
lapse_avg = MAKE_ARRAY( 365, /FLOAT, VALUE=-9999.99 )

ttmp = model.yhat
ltmp = model.b[0]


notvalid = WHERE( ttmp LE -9998., num_notvalid )
IF ( num_notvalid GT 0 ) THEN ttmp[notvalid] = !VALUES.F_NAN

notvalid = WHERE( ltmp LE -9998., num_notvalid )
IF ( num_notvalid GT 0 ) THEN ltmp[notvalid] = !VALUES.F_NAN

FOR id=0, 364 DO BEGIN

   doy = id + 1
   idx = WHERE( model.doy EQ doy, num )
   ibeg = idx-15
   iend = idx+15

   ii = WHERE( ibeg LT 0, num )
   IF ( num GT 0 ) THEN ibeg[ii] = 0

   ie = WHERE( iend GT ntime-1, num )
   IF ( num GT 0 ) THEN iend[ie] = ntime-1

   ni = N_ELEMENTS(ibeg)
   x0 = []
   x1 = []
   x2 = []
   l0 = []
   FOR i=0, ni-1 DO BEGIN
      x0 = [ x0, REFORM(ttmp[0,ibeg[i]:iend[i]]) ]
      x1 = [ x1, REFORM(ttmp[1,ibeg[i]:iend[i]]) ]
      x2 = [ x2, REFORM(ttmp[2,ibeg[i]:iend[i]]) ]
      l0 = [ l0, REFORM(ltmp[ibeg[i]:iend[i]]) ]
   ENDFOR

   clim[0,id] = MEAN( x0, /NAN )
   clim[1,id] = MEAN( x1, /NAN )
   clim[2,id] = MEAN( x2, /NAN )
   lapse_avg[id] = MEAN( l0, /NAN )

END

!P.MULTI=[0,1,3]

time = FINDGEN(365)+1

PLOT, time, clim[0,*], XRANGE=[0,366], XSTYLE=1
PLOT, time, clim[1,*], XRANGE=[0,366], XSTYLE=1
PLOT, time, clim[2,*], XRANGE=[0,366], XSTYLE=1

!P.MULTI=0

;;----------------------------------------------------------------------
;; Write results to file
;;----------------------------------------------------------------------

;; Daily file
fmt = '(i4,1x,i2,1x,i2,1x,i3,4(1x,f8.2),6(1x,f6.2),2(1x,f5.2))'
OPENW, U, 'interp_t2m_hunza_validation_day_' + $
       STRTRIM(model[0].year,2)+'to'+STRTRIM(model[ntime-1].year,2)+'.txt', $
       /GET_LUN
FOR it=0, ntime-1 DO PRINTF, U, model[it], FORMAT=fmt
CLOSE, U
FREE_LUN, U

;; Climatology
fmt = '(i3,3(1x,f6.2))'
OPENW, U, 'interp_t2m_hunza_validation_climatology_' + $
       STRTRIM(model[0].year,2)+'to'+STRTRIM(model[ntime-1].year,2)+'.txt', $
       /GET_LUN
FOR it=0, 364 DO PRINTF, U, it+1, clim[*,it], FORMAT=fmt
CLOSE, U
FREE_LUN, U

END
