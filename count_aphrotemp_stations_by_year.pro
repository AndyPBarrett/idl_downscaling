;;----------------------------------------------------------------------
;; Calculates long-term daily mean temperature fields from Aphrodite AphroTemp
;; data
;;
;; 2014-11-14 A.P.Barrett <apbarret@nsidc.org>
;;----------------------------------------------------------------------

VERBOSE = 1

diri = RAID_PATH('arctic6') + '/' + 'APHRODITE/AphroTemp/V1204R1/APHRO_MA/050deg'
ffmt = '("APHRO_MA_TAVE_050deg_V1204R1.",i4,".nc")'

nday = 365  ;; Number of days in a year (ignores 366)

ybeg = 1979
yend = 2007
nyear = yend - ybeg + 1

window = 31

;; Define limits to subset input array
i0 = 0
i1 = 100
j0 = 69
j1 = 130

;; Define array to hold results
di = i1 - i0 + 1
dj = j1 - j0 + 1
result = MAKE_ARRAY( di, dj, nyear, /FLOAT )

;;----------------------------------------------------------------------
;; Loop through each year
;;----------------------------------------------------------------------
k = 0
FOR iy = ybeg, yend DO BEGIN
      
   fili = diri + '/' + STRING( iy, FORMAT=ffmt )
   CASE 1 OF
      FILE_TEST( fili ) EQ 1: BEGIN
         isgz = 0
      END
      FILE_TEST( fili+'.gz' ) EQ 1: BEGIN
         isgz = 1
         IF ( VERBOSE EQ 1 ) THEN PRINT, '% Unzipping ' + fili+'.gz'
         SPAWN, 'gunzip ' + fili+'.gz'
      END
      ELSE: BEGIN
         PRINT, '% File not found'
         STOP
      END
   ENDCASE
      
   ;; Get the data
   data = NCDF_READV( fili, VARNAME='rstn' )
   ;; Subset it
   tmp = data[ i0:i1, j0:j1, * ]

   idx = WHERE( tmp LT -98., num )
   IF ( num GT 0 ) THEN tmp[idx] = !VALUES.F_NAN

   result[ *, *, k ] = TOTAL( tmp GT 0.0, 3, /NAN )

   k = k + 1

   IF ( isgz EQ 1 ) THEN BEGIN
      IF ( VERBOSE EQ 1 ) THEN PRINT, '% Zipping ' + fili
      SPAWN, 'gzip ' + fili
   ENDIF

ENDFOR

PRINT, MAX( result, /NAN )
STOP

result_pc = result / 366

lat = NCDF_READV( fili, VARNAME='latitude')
lon = NCDF_READV( fili, VARNAME='longitude')

latsub = lat[ j0:j1 ]
lonsub = lon[ i0:i1 ]

k = 0
FOR iy = ybeg, yend DO BEGIN

   PLOT_APHROTEMP_STATIONS, result[*,*,k], latsub, lonsub, TMIN=0, TMAX=100, TINIT=10., $
                            TITLE=STRING( iy, FORMAT='("Year = ",i4)' )
   STOP

   k = k + 1

ENDFOR


SAVE, result, FILENAME='aphrodite_annual mean_station_count_1979to2007.sav'

END
