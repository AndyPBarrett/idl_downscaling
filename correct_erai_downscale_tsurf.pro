;;----------------------------------------------------------------------
;; Corrects downscaled tsurf (2 m temperature) derived from ERA-Interim
;; using bias correction increments calculated from station data.
;;
;; 2015-07-27 A.P.Barrett
;;----------------------------------------------------------------------
PRO CORRECT_ERAI_DOWNSCALE_TSURF, tile, ybeg, yend

start_time = SYSTIME(/SECONDS)

;tile = 'h24v05'
;ybeg = 2001
mbeg = 1
dbeg = 1
;yend = 2009
mend = 12
dend = 31

uncorr_version = 'v0'
corr_version   = 'v0'

root_dir = '/projects/CHARIS/forcing_data/Downscaled/ERA-Interim/day'
uncorr_dir = root_dir + '/' + 'uncorrected' + '/' + uncorr_version

corr_dir = root_dir + '/' + 'corrected' + '/' + corr_version

time = TIMEGEN( START=JULDAY(mbeg,dbeg,ybeg), FINAL=JULDAY(mend,dend,yend), UNITS='Days' )
ntime = N_ELEMENTS( time )

FOR it = 0L, ntime-1 DO BEGIN

   date = STRING( time[it], FORMAT='(C(CYI4.0,CMOI2.2,CDI2.2))' )
   ;; Get day of year
   ;; - Extract and parse date string
   year = FIX( STRMID( date, 0, 4 ) )
   month = FIX( STRMID( date, 4, 2 ) )
   day = FIX( STRMID( date, 6, 2 ) )
   doy = JULDAY( month, day, year ) - JULDAY( 1, 1, year ) + 1
   IF ( doy EQ 366 ) THEN doy = 365

;   infile = FILE_SEARCH( uncorr_dir+'/'+tile+'/'+'????'+'/'+'tsurf_'+tile+'_day_????????.bin', $
;                      COUNT=nfile )

   infile = FILE_SEARCH( uncorr_dir + '/' + tile + '/' + STRING(year,FORMAT='(i4)') + $
                         '/'+'tsurf_'+tile+'_day_' + date + '.bin', $
                      COUNT=nfile )
   PRINT, uncorr_dir + '/' + tile + '/' + STRING(year,FORMAT='(i4)') + $
                         '/'+'tsurf_'+tile+'_day_' + date + '.bin'
   STOP

   IF ( nfile EQ 0 ) THEN BEGIN
      PRINT, '% CORRECT_ERAI_DOWNSCALE_TSURF: Error'
      PRINT, '     File: ' + infile
      PRINT, ' No such file'
      BREAK
   ENDIF
   
   PRINT, 'Processing grid for ' + date

   ;; Open uncorrected grid
   ingrid = OPEN_IMAGE( infile[0], 2400, 2400, 4 )

   ;; Open bias correction grid
   bias_dir = corr_dir + '/' + 'bias_correction_grids' + '/' + tile 
   bias_file = 'erai_tsurf_bias_correction_' + tile + '_' + $
               STRING( doy, FORMAT='(i03)' ) + '.' + corr_version + '.bin'
   bias = OPEN_IMAGE( bias_dir + '/' + bias_file, 2400, 2400, 4 )

   ;; Add bias correction increment to uncorrected grid
   outgrid = ingrid + bias

   ;; Write corrected grid to file
   outdir = corr_dir + '/' + tile + '/' + STRING( year, FORMAT='(i4)' )
   outfil = 'tsurf_' + tile + '_day_' + date + '.corrected.' + corr_version + '.bin'

   ;; . - Make directory if directory does not exist
   IF ( FILE_TEST( outdir, /DIRECTORY ) EQ 0 ) THEN BEGIN
      FILE_MKDIR, outdir
   ENDIF

   OPENW, U, outdir + '/' + outfil, /GET_LUN
   WRITEU, U, outgrid
   CLOSE, U
   FREE_LUN, U

ENDFOR

time_elapsed = SYSTIME(/SECONDS) - start_time
PRINT, 'TIME ELAPSED: ' + STRING((time_elapsed / 3600.))

END
