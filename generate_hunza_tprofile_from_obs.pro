;;----------------------------------------------------------------------
;; Generates air temperatures for elevation bands using station observations.
;;
;; 2014-01-21 A.P.Barrett
;;
;; Currently this a quick and dirty programme with lots of hard coded
;; parameters but it could be modified to be more slick.
;;
;;----------------------------------------------------------------------

FUNCTION DATEINT, year

  jday = TIMEGEN( START=JULDAY(1,1,year), FINAL=JULDAY(12,31,year), UNITS='Day' )
  CALDAT, jday, month, day, yeari

  doy = jday - JULDAY(1,1,year) + 1

  ndate = N_ELEMENTS(jday)

  result = MAKE_ARRAY( 4, ndate, /INTEGER )
  result[0,*] = yeari
  result[1,*] = month
  result[2,*] = day
  result[3,*] = doy

  RETURN, result

END

;;----------------------------------------------------------------------
;; Main Routine
;;----------------------------------------------------------------------

year = 2009

;;----------------------------------------------------------------------
;; Define elevations of bands/sites
;;----------------------------------------------------------------------
elev_min = 1400.
elev_max = 7700.
elev_int =  100.
nelev = FLOOR( ( elev_max - elev_min ) / elev_int ) + 1
elev = ( FINDGEN( nelev ) * elev_int ) + elev_min + (elev_int*0.5)

;PRINT, elev
;STOP

;;----------------------------------------------------------------------
;; Dummy structure holding station info
;; To be replaced by reader from file
;;----------------------------------------------------------------------

stations = [ CREATE_STRUCT( 'NAME', 'Naltar',    'ELEVATION', 2810.0 ), $
             CREATE_STRUCT( 'NAME', 'Ziarat',    'ELEVATION', 3669.0 ), $
             CREATE_STRUCT( 'NAME', 'Khunjerab', 'ELEVATION', 4730.0 ) ]
;; Make sure stations are ordered by elevation
stations = stations[ SORT( stations.elevation ) ]
nstat = N_ELEMENTS( stations )

;;----------------------------------------------------------------------
;; Find the closest station to each elevation band
;;----------------------------------------------------------------------
statid = MAKE_ARRAY( nelev, /INTEGER, VALUE=-1 )
FOR il=0, nelev-1 DO BEGIN
   id = VALUE_LOCATE( stations.elevation, elev[il] )
   CASE 1 OF
      id EQ -1: statid[il] = 0  ;; assign lowest station
      id EQ ( N_ELEMENTS(stations)-1 ): statid[il] = N_ELEMENTS(stations)-1
      ELSE: BEGIN
         IF ( ABS(stations[id].elevation - elev[il] ) GT $
              ABS(stations[id+1].elevation - elev[il]) ) THEN BEGIN
            statid[il] = id+1 
         ENDIF ELSE BEGIN
            statid[il] = id
         ENDELSE
      END
   ENDCASE
ENDFOR

;;----------------------------------------------------------------------
;; Get data for a given year
;;----------------------------------------------------------------------
;wapda_diri = '/projects/CHARIS/streamflow/Pakistan/' + $
;             'Data_from_DHashmi/from_danial/Data_for_Andrew_Barnett/climate/'
wapda_diri = '/projects/CHARIS/surface_met_data/WAPDA'
khun = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'khunjerab/khunjerab_'+STRTRIM(year,2)+'.qc1.fill.txt' )
nalt = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'naltar/naltar_'+STRTRIM(year,2)+'.qc1.fill.txt' )
ziar = READ_WAPDA_CLIMATE( wapda_diri + '/' + 'ziarat/ziarat_'+STRTRIM(year,2)+'.qc1.fill.txt' )
nday = N_ELEMENTS( khun )

;;----------------------------------------------------------------------
;; Some ziarat data are missing.  Interpolate from Naltar using standard lapse
;; rate
;;----------------------------------------------------------------------
tlapse = -0.0065  ;; lapse rate

ismissing = WHERE( ziar.tavg LT -9998., nummissing )
IF ( nummissing GT 0 ) THEN BEGIN
   delev = stations[1].elevation - stations[0].elevation
   ziar[ismissing].tavg = nalt[ismissing].tavg + ( tlapse * delev )
ENDIF

;; We only need average temperature
tavg_obs = MAKE_ARRAY( nstat, nday, /FLOAT, VALUE=-9999.99 )
tavg_obs[0,*] = nalt.tavg
tavg_obs[1,*] = ziar.tavg
tavg_obs[2,*] = khun.tavg


;;----------------------------------------------------------------------
;; Generate temperatures for elevation bands
;;----------------------------------------------------------------------
tavg_est = MAKE_ARRAY( nelev, nday, /FLOAT )
FOR id=0, nday-1 DO BEGIN
   
   tavg_est[*,id] = tavg_obs[statid,id] + ( tlapse * ( elev - stations[statid].elevation ) ) + 273.15
   ;PLOT, tavg_est[*,id], elev, PSYM=-2, TITLE=STRING(id+1,FORMAT='("DAY=",i03)')
   ;OPLOT, tavg_obs[*,id], stations.elevation, PSYM=4, COLOR=FSC_COLOR('red'), THICK=2
   ;STOP

ENDFOR

PRINT, MIN( tavg_est ), MAX( tavg_est )
;STOP

;;----------------------------------------------------------------------
;; Write results to file
;;----------------------------------------------------------------------

basin = 'hunza'
datei = DATEINT( year )

filo = STRLOWCASE(basin)+'_observation_tavg_profile_100m.'+STRTRIM(year,2)+'.dscale.v2.asc'
OPENW, U, filo, /GET_LUN
PRINTF, U, nelev
PRINTF, U, elev, FORMAT='('+STRTRIM(nelev,2)+'(1x,i4))'
FOR id=0, nday-1 DO BEGIN
   PRINTF, U, datei[*,id], tavg_est[*,id], FORMAT='(i4,2(1x,i2),1x,i3,'+STRTRIM(nelev,2)+'(1x,f6.2))'
ENDFOR
CLOSE, U
FREE_LUN, U

END
